<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Liquidacion;

/**
 * app\models\LiquidacionSearch represents the model behind the search form about `app\models\Liquidacion`.
 */
 class LiquidacionSearch extends Liquidacion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado', 'created_by', 'updated_by','proveedor_id'], 'integer'],
            [['valor_factura', 'total_gastos', 'impuestos', 'total_importacion', 'factor_importacion', 'factor_liquidacion'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Liquidacion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->join('join', PedidoLog::tableName(),'factura = liquidacion.id');
        $query->join('join', Pedido::tableName(),'pedido.id = pedido_id');
        $query->andFilterWhere([
            'id' => $this->id,
            'estado' => $this->estado,
            'valor_factura' => $this->valor_factura,
            'total_gastos' => $this->total_gastos,
            'impuestos' => $this->impuestos,
            'total_importacion' => $this->total_importacion,
            'factor_importacion' => $this->factor_importacion,
            'factor_liquidacion' => $this->factor_liquidacion,
            'proveedor_id' => $this->proveedor_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
