<?php

namespace app\models;

use \app\models\base\ProveedorProducto as BaseProveedorProducto;

/**
 * This is the model class for table "proveedor_producto".
 */
class ProveedorProducto extends BaseProveedorProducto
{
    public $codigo;
    public $estado;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['proveedor_id', 'producto_id'], 'required'],
            [['proveedor_id', 'producto_id',  'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['referencia_proveedor', 'referencia_proveedor2'], 'string', 'max' => 255]
        ]);
    }
    
    /**
     *@author Juan Velez <juanfv@gmail.com>
     * @return \yii\db\ActiveQuery
     */
    public static function getProductosRepetidos()
    {
        $productos = \Yii::$app->db->createCommand("
                SELECT producto_id,nombre
                    FROM proveedor_producto
                    JOIN proveedor ON proveedor_id = id
                    WHERE producto_id IN (
                        SELECT producto_id
                        FROM proveedor_producto
                        GROUP BY producto_id
                        HAVING COUNT(producto_id) > 1
                    )
            ")
            ->queryAll();
        $repetidos = [];
        foreach ($productos as $producto) {
            $repetidos[$producto['producto_id']] = isset($repetidos[$producto['producto_id']])?($repetidos[$producto['producto_id']].$producto['nombre']):$producto['nombre'].', '; 
        }
        
        return $repetidos;
    }
    
    /**
     *@author Juan Velez <juanfv@gmail.com>
     * @return \yii\db\ActiveQuery
     */
    public static function getProductosRelacionados()
    {
        
        $productos = ProductosRelacionados::find()->asArray()->all();
        $relaciones = [];
        foreach ($productos as $producto) {
            $relaciones[$producto['producto']] = isset($relaciones[$producto['producto']])?($relaciones[$producto['producto']].$producto['relacion'].', '):$producto['relacion'].', '; 
        }
        
        return $relaciones;
    }
	
}
