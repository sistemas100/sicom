<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Distribuidor;

/**
 * app\models\DistribuidorSearch represents the model behind the search form about `app\models\Distribuidor`.
 */
 class DistribuidorSearch extends Distribuidor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'descuento', 'created_by', 'updated_by'], 'integer'],
            [['nombre', 'fecha','columna_referencia', 'columna_codigo', 'columna_precio', 'columna_nombre', 'columna_marca', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Distribuidor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'descuento' => $this->descuento,
            'fecha' => $this->fecha,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'columna_referencia', $this->columna_referencia])
            ->andFilterWhere(['like', 'columna_codigo', $this->columna_codigo])
            ->andFilterWhere(['like', 'columna_precio', $this->columna_precio])
            ->andFilterWhere(['like', 'columna_nombre', $this->columna_nombre])
            ->andFilterWhere(['like', 'columna_marca', $this->columna_marca]);

        return $dataProvider;
    }
}
