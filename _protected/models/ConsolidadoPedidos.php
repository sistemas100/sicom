<?php

namespace app\models;

use \app\models\base\ConsolidadoPedidos as BaseConsolidadoPedidos;

/**
 * This is the model class for table "consolidado_pedidos".
 */
class ConsolidadoPedidos extends BaseConsolidadoPedidos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['producto_codigo', 'fecha', 'cantidad'], 'required'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['cantidad', 'created_by', 'updated_by'], 'integer'],
            [['producto_codigo'], 'string', 'max' => 20]
        ]);
    }
	
}
