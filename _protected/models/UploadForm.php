<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;
    public $fecha;
    public $tipo;

    public function rules()
    {
        return [
            //[['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
            [['file','fecha','tipo'], 'required'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->baseName . '.' . $this->file->extension);
            return true;
        } else {
            echo "<pre>";
            print_R($this);
            echo "</pre>";
            die;
            return false;
        }
    }
    public function save()
    {
        return true;
    }
    
     public function attributeLabels() {
        return [
            'file' => Yii::t('app', 'Archivo'),

        ];
    }
}

