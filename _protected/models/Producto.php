<?php

namespace app\models;

use app\models\base\Producto as BaseProducto;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "producto".
 */
class Producto extends BaseProducto
{
     public $meses;
     public $bajo_stock;
     public $bo;
     public $duracion;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['referencia', 'nombre'], 'required'],
            [['precio1', 'precio2', 'precio4', 'promedio_manual'], 'number'],
            [['existencia', 'ctoprm', 'upcompra', 'ufcompra', 'ufvta', 'estado', 'tiempo_produccion', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['codigo', 'nombre_proveedor','tipo'], 'string', 'max' => 45],
            [['referencia', 'nombre'], 'string', 'max' => 255],
            [['codigo'], 'unique']
        ]);
    }
    
    /**
     * @return ActiveQuery
     * @author Juan Velez <juanfv@gmail.com>
     */
    public static function getPreciosCompetencia($proveedor_id)
    {
        $result  = Yii::$app->db->createCommand("SELECT producto.id,precio1,precio2,CONCAT(distribuidor.nombre,' - ',producto_distribuidor.referencia,' - ',producto_distribuidor.marca ) nombre,producto_distribuidor.precio
                                            FROM producto 
                                            LEFT JOIN referencia ON producto.id = referencia.producto_id
                                            LEFT JOIN proddist_referencia ON proddist_referencia.referencia_id = referencia.id
                                            LEFT JOIN producto_distribuidor ON producto_distribuidor.id = proddist_referencia.proddist_id
                                            LEFT JOIN distribuidor ON distribuidor.id =  producto_distribuidor.distribuidor_id
                                            LEFT JOIN proveedor_producto ON proveedor_producto.producto_id = producto.id
                                            WHERE proveedor_producto.proveedor_id  = '$proveedor_id'
                                            GROUP BY producto.id,producto_distribuidor.distribuidor_id
                                            ORDER BY producto.id,producto_distribuidor.precio")->queryAll();
        $precios = [];
        $prod_id = '';
        $todos = '';
        $flag = true;
        foreach ($result as $row) {
            if($prod_id == $row['id']){
                $todos .= $row['precio']>0?$row['nombre'].': $'.number_format($row['precio'],0).'<br /> ':'';
            }else{
                $todos = $row['precio']>0?$row['nombre'].': $'.number_format($row['precio'],0).'<br /> ':'';
                $flag = true;
            }
            if($flag && $row['precio']>0){
                $precios[$row['id']]['menor'] = '$'.number_format($row['precio'],0);
                $flag = false;
            }
            if($flag){
                $precios[$row['id']]['menor'] = '';
            }
            $precio_reypar = $row['precio1']>0?$row['precio1']:$row['precio2'];
            $precios[$row['id']]['todos'] = 'REYPAR: $'.number_format($precio_reypar,0).'<br /> '.str_replace('"','',substr($todos, 0,-2));
            $prod_id = $row['id'];
        }
        return $precios;
    }
	
}
