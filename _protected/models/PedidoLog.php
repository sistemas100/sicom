<?php

namespace app\models;

use \app\models\base\PedidoLog as BasePedidoLog;

/**
 * This is the model class for table "pedido_log".
 */
class PedidoLog extends BasePedidoLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pedido_id', 'estado'], 'required'],
            [['pedido_id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
