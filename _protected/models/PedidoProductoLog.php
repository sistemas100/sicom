<?php

namespace app\models;

use \app\models\base\PedidoProductoLog as BasePedidoProductoLog;

/**
 * This is the model class for table "pedido_producto_log".
 */
class PedidoProductoLog extends BasePedidoProductoLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['pedido_log_id', 'producto_id', 'cantidad', 'precio'], 'required'],
            [['pedido_log_id', 'producto_id', 'cantidad', 'nueva_cantidad', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['precio'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['observacion'], 'string', 'max' => 255]
        ]);
    }
	
}
