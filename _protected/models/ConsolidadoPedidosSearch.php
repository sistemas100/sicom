<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ConsolidadoPedidos;

/**
 * app\models\ConsolidadoPedidosSearch represents the model behind the search form about `app\models\ConsolidadoPedidos`.
 */
 class ConsolidadoPedidosSearch extends ConsolidadoPedidos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_codigo', 'fecha', 'created_at', 'updated_at'], 'safe'],
            [['cantidad', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConsolidadoPedidos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fecha' => $this->fecha,
            'cantidad' => $this->cantidad,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'producto_codigo', $this->producto_codigo]);

        return $dataProvider;
    }
}
