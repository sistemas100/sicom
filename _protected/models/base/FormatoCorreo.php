<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "formato_correo".
 *
 * @property integer $id
 * @property string $asunto
 * @property string $cuerpo_texto
 * @property string $cuerpo_html
 * @property string $idioma
 * @property integer $estado
 * @property integer $estado_previo
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class FormatoCorreo extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asunto', 'cuerpo_texto', 'idioma', 'estado', 'estado_previo'], 'required'],
            [['estado', 'estado_previo', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['asunto'], 'string', 'max' => 255],
            [['cuerpo_texto', 'cuerpo_html'], 'string', 'max' => 1000],
            [['idioma'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'formato_correo';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'asunto' => Yii::t('app', 'Asunto'),
            'cuerpo_texto' => Yii::t('app', 'Cuerpo Texto'),
            'cuerpo_html' => Yii::t('app', 'Cuerpo Html'),
            'idioma' => Yii::t('app', 'Idioma'),
            'estado' => Yii::t('app', 'Estado'),
            'estado_previo' => Yii::t('app', 'Estado Previo'),
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
