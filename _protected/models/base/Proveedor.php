<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "proveedor".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $nit
 * @property string $contacto
 * @property string $telefono
 * @property string $direccion
 * @property string $ciudad
 * @property string $correo
 * @property string $idioma
 * @property string $tipo
 * @property integer $tiempo_produccion
 * @property integer $tiempo_transporte
 * @property integer $frecuencia_pedido
 * @property string $factor_pedido
 * @property string $bo_aceptados
 * @property string $columna_codigo
 * @property string $columna_cantidad
 * @property string $columna_precio
 * @property string $columna_pedido_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Pedido[] $pedidos
 * @property \app\models\Producto[] $productos
 * @property \app\models\User $createdBy
 * @property \app\models\ProveedorProducto[] $proveedorProductos
 */
class Proveedor extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo', 'factor_pedido', 'frecuencia_pedido'], 'required'],
            [['tiempo_produccion', 'bo_aceptados', 'tiempo_transporte', 'frecuencia_pedido', 'created_by', 'updated_by'], 'integer'],
            [['factor_pedido','tipo'], 'number'],
            [['correo'], 'email'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'contacto', 'direccion', 'correo'], 'string', 'max' => 255],
            [['nit', 'telefono', 'ciudad', 'columna_codigo', 'columna_cantidad', 'columna_precio', 'columna_pedido_id','idioma'], 'string', 'max' => 45]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'nit' => Yii::t('app', 'Nit'),
            'contacto' => Yii::t('app', 'Contacto'),
            'telefono' => Yii::t('app', 'Teléfono'),
            'direccion' => Yii::t('app', 'Dirección'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'correo' => Yii::t('app', 'Correo'),
            'idioma' => Yii::t('app', 'Idioma'),
            'tipo' => Yii::t('app', 'Tipo'),
            'tiempo_produccion' => Yii::t('app', 'T Producción'),
            'tiempo_transporte' => Yii::t('app', 'T Transporte'),
            'frecuencia_pedido' => Yii::t('app', 'Frecuencia Pedido'),
            'factor_pedido' => Yii::t('app', 'Factor Reposición'),
            'bo_aceptados' => Yii::t('app', 'BO Aceptados'),
            'columna_codigo' => Yii::t('app', 'Col Codigo'),
            'columna_cantidad' => Yii::t('app', 'Col Cantidad'),
            'columna_precio' => Yii::t('app', 'Col Precio'),
            'columna_pedido_id' => Yii::t('app', 'Col Pedido ID'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(\app\models\Pedido::className(), ['proveedor_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(\app\models\Producto::className(), ['id' => 'producto_id'])->viaTable('proveedor_producto', ['proveedor_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorProductos()
    {
        return $this->hasMany(\app\models\ProveedorProducto::className(), ['proveedor_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
