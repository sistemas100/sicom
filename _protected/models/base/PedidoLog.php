<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "pedido_log".
 *
 * @property integer $id
 * @property integer $pedido_id
 * @property integer $estado
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Pedido $pedido
 * @property \app\models\PedidoProductoLog[] $pedidoProductoLogs
 * @property \app\models\Producto[] $productos
 */
class PedidoLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pedido_id', 'estado'], 'required'],
            [['pedido_id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido_log';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pedido_id' => Yii::t('app', 'Pedido ID'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(\app\models\Pedido::className(), ['id' => 'pedido_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoProductoLogs()
    {
        return $this->hasMany(\app\models\PedidoProductoLog::className(), ['pedido_log_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(\app\models\Producto::className(), ['id' => 'producto_id'])->viaTable('pedido_producto_log', ['pedido_log_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
