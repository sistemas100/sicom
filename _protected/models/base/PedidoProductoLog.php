<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "pedido_producto_log".
 *
 * @property integer $pedido_log_id
 * @property string $producto_id
 * @property integer $cantidad
 * @property string $precio
 * @property string $observacion
 * @property integer $nueva_cantidad
 * @property integer $estado
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\PedidoLog $pedidoLog
 * @property \app\models\Producto $producto
 */
class PedidoProductoLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pedido_log_id', 'producto_id', 'cantidad', 'precio'], 'required'],
            [['pedido_log_id', 'producto_id', 'cantidad', 'nueva_cantidad', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['precio'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['observacion'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido_producto_log';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pedido_log_id' => Yii::t('app', 'Pedido Log ID'),
            'producto_id' => Yii::t('app', 'Producto ID'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'precio' => Yii::t('app', 'Precio'),
            'observacion' => Yii::t('app', 'Observacion'),
            'nueva_cantidad' => Yii::t('app', 'Nueva Cantidad'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoLog()
    {
        return $this->hasOne(\app\models\PedidoLog::className(), ['id' => 'pedido_log_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(\app\models\Producto::className(), ['id' => 'producto_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
