<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "liquidacion_item".
 *
 * @property integer $id
 * @property integer $liquidacion_id
 * @property integer $item
 * @property string $moneda
 * @property string $usdcop
 * @property string $eurusd
 * @property string $valor
 * @property string $valor_cop
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \app\models\Liquidacion $liquidacion
 * @property \app\models\ListItem $item0
 */
class LiquidacionItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['liquidacion_id'], 'required'],
            [['liquidacion_id', 'item', 'created_by', 'updated_by'], 'integer'],
            [['moneda'], 'string'],
            [['usdcop', 'eurusd', 'valor', 'valor_cop'], 'number'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liquidacion_item';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'liquidacion_id' => Yii::t('app', 'Liquidacion ID'),
            'item' => Yii::t('app', 'Item'),
            'moneda' => Yii::t('app', 'Moneda'),
            'usdcop' => Yii::t('app', 'Usdcop'),
            'eurusd' => Yii::t('app', 'Eurusd'),
            'valor' => Yii::t('app', 'Valor'),
            'valor_cop' => Yii::t('app', 'Valor Cop'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacion()
    {
        return $this->hasOne(\app\models\Liquidacion::className(), ['id' => 'liquidacion_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem0()
    {
        return $this->hasOne(\app\models\ListItem::className(), ['id' => 'item']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
