<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "proveedor_producto".
 *
 * @property integer $proveedor_id
 * @property string $producto_id
 * @property string $referencia_proveedor
 * @property string $referencia_proveedor2
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Producto $producto
 * @property \app\models\Proveedor $proveedor
 */
class ProveedorProducto extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proveedor_id', 'producto_id'], 'required'],
            [['proveedor_id', 'producto_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['referencia_proveedor', 'referencia_proveedor2'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor_producto';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'proveedor_id' => Yii::t('app', 'Proveedor ID'),
            'producto_id' => Yii::t('app', 'Producto ID'),
//            'estado' => Yii::t('app', 'Estado'),
            'referencia_proveedor' => Yii::t('app', 'Referencia Proveedor'),
            'referencia_proveedor2' => Yii::t('app', 'Referencia Proveedor2'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(\app\models\Producto::className(), ['id' => 'producto_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(\app\models\Proveedor::className(), ['id' => 'proveedor_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
