<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "pedido".
 *
 * @property integer $id
 * @property string $codigo_pedido
 * @property string $codigo
 * @property integer $proveedor_id
 * @property string $fecha
 * @property integer $estado
 * @property string $observacion
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Proveedor $proveedor
 * @property \app\models\ListItem $nombreEstado
 * @property \app\models\User $createdBy
 * @property \app\models\PedidoLog[] $pedidoLogs
 * @property \app\models\PedidoProducto[] $pedidoProductos
 * @property \app\models\Producto[] $productos
 */
class Pedido extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proveedor_id', 'estado'], 'required'],
            [['proveedor_id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['codigo_pedido', 'codigo'], 'string', 'max' => 45],
            [['observacion'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'codigo_pedido' => Yii::t('app', 'Codigo Pedido'),
            'codigo' => Yii::t('app', 'Codigo Proveedor'),
            'proveedor_id' => Yii::t('app', 'Proveedor ID'),
            'fecha' => Yii::t('app', 'Fecha'),
            'estado' => Yii::t('app', 'Estado'),
            'observacion' => Yii::t('app', 'Observacion'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(\app\models\Proveedor::className(), ['id' => 'proveedor_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNombreEstado()
    {
        return $this->hasOne(\app\models\ListItem::className(), ['code' => 'estado']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoLogs()
    {
        return $this->hasMany(\app\models\PedidoLog::className(), ['pedido_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoProductos()
    {
        return $this->hasMany(\app\models\PedidoProducto::className(), ['pedido_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(\app\models\Producto::className(), ['id' => 'producto_id'])->viaTable('pedido_producto', ['pedido_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
