<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "consolidado_pedidos".
 *
 * @property string $producto_codigo
 * @property string $fecha
 * @property integer $cantidad
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class ConsolidadoPedidos extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_codigo', 'fecha', 'cantidad'], 'required'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['cantidad', 'created_by', 'updated_by'], 'integer'],
            [['producto_codigo'], 'string', 'max' => 20]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consolidado_pedidos';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'producto_codigo' => Yii::t('app', 'Producto Codigo'),
            'fecha' => Yii::t('app', 'Fecha'),
            'cantidad' => Yii::t('app', 'Cantidad'),
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
