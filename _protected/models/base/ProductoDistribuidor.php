<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "producto_distribuidor".
 *
 * @property string $id
 * @property string $producto_id
 * @property string $referencia
 * @property string $nombre
 * @property string $precio
 * @property string $marca
 * @property integer $distribuidor_id
 * @property string $fecha 
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Distribuidor $distribuidor
 * @property \app\models\Producto $producto 
 * @property \app\models\User $createdBy
 */
class ProductoDistribuidor extends \yii\db\ActiveRecord {

    use \mootensai\relation\RelationTrait;

    public $dist;
    public $descuento;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['referencia', 'nombre', 'distribuidor_id'], 'required'],
            [['precio'], 'number'],
            [['distribuidor_id', 'producto_id', 'created_by', 'updated_by'], 'integer'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['marca'], 'string', 'max' => 45],
            [['referencia', 'nombre'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'producto_distribuidor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'producto_id' => Yii::t('app', 'Producto id'),
            'referencia' => Yii::t('app', 'Referencia'),
            'nombre' => Yii::t('app', 'Nombre'),
            'precio' => Yii::t('app', 'Precio'),
            'marca' => Yii::t('app', 'Marca'),
            'distribuidor_id' => Yii::t('app', 'Distribuidor ID'),
            'fecha' => Yii::t('app', 'Fecha'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuidor() {
        return $this->hasOne(\app\models\Distribuidor::className(), ['id' => 'distribuidor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto() {
        return $this->hasOne(\app\models\Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @author Juan Velez <juanfv@gmail.com>
     */
    public static function getMenores() {
        $menores = array();
        $result = Yii::$app->db->createCommand("SELECT * 
FROM ((SELECT DISTINCT t1.id, t1.id codigo,t1.precio1 precio, 'prod' 
	FROM producto t1 
	JOIN `referencia` t2 ON t1.id = t2.producto_id 
	JOIN `proddist_referencia` t3 ON t3.`referencia_id` = t2.id 
	WHERE t1.precio1 > 0) 
UNION (SELECT DISTINCT t1.id, t1.id codigo,t1.precio2 precio, 'prod' 
	FROM producto t1 
	JOIN `referencia` t2 ON t1.id = t2.producto_id 
	JOIN `proddist_referencia` t3 ON t3.`referencia_id` = t2.id
	WHERE t1.precio1 = 0 AND t1.precio2 > 0) 
UNION (SELECT DISTINCT t4.id, t1.id codigo,t4.precio precio, 'dist' 
	FROM producto t1 
	JOIN `referencia` t2 ON t1.id = t2.producto_id 
	JOIN `proddist_referencia` t3 ON t3.`referencia_id` = t2.id 
	JOIN `producto_distribuidor` t4 ON t4.`id` = t3.`proddist_id` 
	WHERE precio > 0) ORDER BY precio) t5 GROUP BY codigo")->queryAll();
        foreach ($result as $row) {
            if ($row['prod'] == 'prod') {
                $menores['prod'][] = $row['id'];
            } else {
                $menores['dist'][] = $row['id'];
            }
        }
        return $menores;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @author Juan Velez <juanfv@gmail.com>
     */
    public static function updateProductoDistribuidor($id = null) {
        if ($id) {
            $params = [':p1_id' => $id];
            $sqlA = "DELETE proddist_referencia 
                FROM  proddist_referencia
                JOIN producto_distribuidor  ON proddist_id = producto_distribuidor.id
                JOIN referencia  ON referencia_id = referencia.id
                WHERE `distribuidor_id` = '$id' AND (tipo = 2 OR nuevo = 1)";
            
            
            $updatedRows = Yii::$app->db->createCommand($sqlA)->execute();

            //inserta las referencias que cruzan
            Yii::$app->db->createCommand("SET SQL_BIG_SELECTS = 1")->execute();
            $rows = Yii::$app->db->createCommand("INSERT INTO proddist_referencia(`proddist_id`,`referencia_id`)
                SELECT p1.id proddist_id, p2.id referencia_id
                FROM `producto_distribuidor` p1
                JOIN `referencia` p2 
                ON p2.`referencia` = p1.referencia OR p1.`referencia` = p2.referencia
                WHERE p1.`distribuidor_id` = :p1_id 
                    AND p1.referencia <> '' 
                    AND p2.referencia <> '' 
                    AND CHAR_LENGTH(p1.referencia)>5
                    AND CHAR_LENGTH(p2.referencia)>5
                    AND (p2.tipo = 2 OR  p1.nuevo = 1)
                UNION    
                SELECT p1.id proddist_id, p2.id referencia_id
                FROM `producto_distribuidor` p1
                JOIN `referencia` p2 ON p2.`referencia` = p1.referencia
                WHERE p1.`distribuidor_id` = :p1_id 
                    AND p1.referencia <> '' 
                    AND p2.referencia <> ''
                    AND CHAR_LENGTH(p1.referencia)<=5
                    AND CHAR_LENGTH(p2.referencia)<=5
                    AND (p2.tipo = 2 OR  p1.nuevo = 1)")->bindValues($params)->execute(); //2: nueva referencia distribuidor
        } else {
            $updatedRows = Yii::$app->db->createCommand("DELETE proddist_referencia
                FROM `proddist_referencia`
                JOIN referencia ON referencia_id = id
                WHERE tipo = 2")->execute();

            Yii::$app->db->createCommand("SET SQL_BIG_SELECTS = 1")->execute();
            Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")->execute();
            $rows = Yii::$app->db->createCommand("INSERT INTO proddist_referencia(`proddist_id`,`referencia_id`)
                SELECT p1.id proddist_id, p2.id referencia_id
                FROM `producto_distribuidor` p1
                JOIN `referencia` p2 
                ON p2.`referencia` = p1.referencia OR p1.`referencia`  = p2.referencia
                WHERE p1.referencia <> '' 
                    AND p2.referencia <> '' 
                    AND CHAR_LENGTH(p1.referencia)>5 
                    AND CHAR_LENGTH(p2.referencia)>5 
                    AND p2.tipo = 2
                UNION
                SELECT p1.id proddist_id, p2.id referencia_id
                FROM `producto_distribuidor` p1
                JOIN `referencia` p2 ON p2.`referencia` = p1.referencia
                WHERE p1.referencia <> '' 
                    AND p2.referencia <> '' 
                    AND CHAR_LENGTH(p1.referencia)<=5 
                    AND CHAR_LENGTH(p2.referencia)<=5 
                    AND p2.tipo = 2")->execute();
            Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")->execute();
        }
        return $updatedRows;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

}
