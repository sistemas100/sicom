<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "referencia".
 *
 * @property string $id
 * @property string $referencia
 * @property string $producto_id
 * @property integer $tipo
 *
 * @property \app\models\ProddistReferencia[] $proddistReferencias
 * @property \app\models\ProductoDistribuidor[] $proddists
 * @property \app\models\Producto $producto
 */
class Referencia extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['referencia', 'producto_id'], 'required'],
            [['producto_id', 'tipo'], 'integer'],
            [['referencia'], 'string', 'max' => 255],
//            [['producto_id', 'referencia'], 'unique', 'targetAttribute' => ['producto_id', 'referencia'], 'message' => 'The combination of Referencia and Producto ID has already been taken.']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referencia';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'referencia' => Yii::t('app', 'Referencia'),
            'producto_id' => Yii::t('app', 'Producto ID'),
            'tipo' => Yii::t('app', 'Tipo'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProddistReferencias()
    {
        return $this->hasMany(\app\models\ProddistReferencia::className(), ['referencia_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProddists()
    {
        return $this->hasMany(\app\models\ProductoDistribuidor::className(), ['id' => 'proddist_id'])->viaTable('proddist_referencia', ['referencia_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(\app\models\Producto::className(), ['id' => 'producto_id']);
    }
}
