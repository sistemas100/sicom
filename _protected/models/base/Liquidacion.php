<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "liquidacion".
 *
 * @property integer $id
 * @property integer $estado
 * @property string $valor_factura
 * @property string $total_gastos
 * @property string $impuestos
 * @property string $total_importacion
 * @property string $factor_importacion
 * @property string $factor_liquidacion
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property \app\models\LiquidacionItem[] $liquidacionItems
 */
class Liquidacion extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['valor_factura', 'total_gastos', 'impuestos', 'total_importacion', 'factor_importacion', 'factor_liquidacion'], 'number'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'liquidacion';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'estado' => Yii::t('app', 'Estado'),
            'valor_factura' => Yii::t('app', 'Valor Factura'),
            'total_gastos' => Yii::t('app', 'Total Gastos'),
            'impuestos' => Yii::t('app', '4 X Mil'),
            'total_importacion' => Yii::t('app', 'Total Importacion'),
            'factor_importacion' => Yii::t('app', 'Factor Importacion'),
            'factor_liquidacion' => Yii::t('app', 'Factor Liquidacion'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionItems()
    {
        return $this->hasMany(\app\models\LiquidacionItem::className(), ['liquidacion_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
