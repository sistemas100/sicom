<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "producto".
 *
 * @property string $id
 * @property string $codigo
 * @property string $referencia
 * @property string $nombre
 * @property string $precio1
 * @property string $precio2
 * @property string $precio4
 * @property integer $existencia
 * @property integer $ctoprm
 * @property integer $upcompra
 * @property integer $ufcompra
 * @property integer $ufvta
 * @property string $nombre_proveedor
 * @property string $promedio_manual 
 * @property integer $estado
 * @property integer $tiempo_produccion 
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\PedidoProducto[] $pedidoProductos
 * @property \app\models\Pedido[] $pedidos
 * @property \app\models\PedidoProductoLog[] $pedidoProductoLogs 
 * @property \app\models\PedidoLog[] $pedidoLogs 
 * @property \app\models\Proveedor $proveedor
 * @property \app\models\User $createdBy
 * @property \app\models\ProveedorProducto[] $proveedorProductos
 * @property \app\models\Proveedor[] $proveedors
 * @property \app\models\Referencia[] $referencias
 */
class Producto extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $join;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['referencia', 'nombre'], 'required'],
            [['precio1', 'precio2', 'precio4', 'promedio_manual'], 'number'],
            [['existencia', 'ctoprm', 'upcompra', 'ufcompra', 'ufvta', 'estado', 'tiempo_produccion', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['codigo', 'nombre_proveedor','tipo'], 'string', 'max' => 45],
            [['referencia', 'nombre'], 'string', 'max' => 255],
            [['codigo'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'codigo' => Yii::t('app', 'Codigo'),
            'referencia' => Yii::t('app', 'Referencia'),
            'nombre' => Yii::t('app', 'Nombre'),
            'precio1' => Yii::t('app', 'Precio1'),
            'precio2' => Yii::t('app', 'Precio2'),
            'precio4' => Yii::t('app', 'Precio4'),
            'existencia' => Yii::t('app', 'Existencia'),
            'ctoprm' => Yii::t('app', 'Ctoprm'),
            'upcompra' => Yii::t('app', 'Upcompra'),
            'ufcompra' => Yii::t('app', 'Ufcompra'),
            'ufvta' => Yii::t('app', 'Ufvta'),
            'nombre_proveedor' => Yii::t('app', 'Marca'),
            'tipo' => Yii::t('app', 'Tipo'),
            'promedio_manual' => Yii::t('app', 'Promedio'),
            'estado' => Yii::t('app', 'Estado'),
            'tiempo_produccion' => Yii::t('app', 'Tiempo Produccion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoProductos()
    {
        return $this->hasMany(\app\models\PedidoProducto::className(), ['producto_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(\app\models\Pedido::className(), ['id' => 'pedido_id'])->viaTable('pedido_producto', ['producto_id' => 'id']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
   public function getPedidoProductoLogs() 
   { 
       return $this->hasMany(\app\models\PedidoProductoLog::className(), ['producto_id' => 'id']); 
   } 

   /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPedidoLogs() 
   { 
       return $this->hasMany(\app\models\PedidoLog::className(), ['id' => 'pedido_log_id'])->viaTable('pedido_producto_log', ['producto_id' => 'id']); 
   } 
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorProductos()
    {
        return $this->hasMany(\app\models\ProveedorProducto::className(), ['producto_id' => 'id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedors()
    {
        return $this->hasMany(\app\models\Proveedor::className(), ['id' => 'proveedor_id'])->viaTable('proveedor_producto', ['producto_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferencias()
    {
        return $this->hasMany(\app\models\Referencia::className(), ['producto_id' => 'id']);
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getReferenciasNuevas() {
        $arrReferencias = array();
        $productos = Producto::find()->select("producto.`id`, producto.`referencia`")
                        ->join('LEFT JOIN', 'referencia', '`producto_id` = producto.id')
                        ->where('(producto.referencia <> referencia.referencia AND referencia.tipo = 2) OR referencia.id IS NULL')
                        ->asArray()->all();
        foreach ($productos as $row) {
                $arrReferencias[] = [$row['id'],$row['referencia'],2];//2:pendiente por actualizar 1:Actualizado
        }
        
        return $arrReferencias;
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public static function batchUpdate($table, $columns, $rows, $duplicates = []) {

        $db = Yii::$app->db;

        if (($tableSchema = $db->getTableSchema($table)) !== null) {

            $columnSchemas = $tableSchema->columns;
        } else {

            $columnSchemas = [];
        }

        $sql = $db->getQueryBuilder()->batchInsert($table, $columns, $rows);

        if (!empty($duplicates)) {

            $columnDuplicates = [];

            foreach ($duplicates as $i => $column) {

                if (isset($columnSchemas[$duplicates[$i]])) {

                    $column = $db->quoteColumnName($column);

                    $columnDuplicates[] = $column . ' = VALUES(' . $column . ')';
                }
            }

            if (!empty($columnDuplicates)) {

                $sql .= ' ON DUPLICATE KEY UPDATE ' . implode(',', $columnDuplicates);
            }
        }

        return $db->createCommand()->setSql($sql);
    }

}
