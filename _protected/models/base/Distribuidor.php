<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "distribuidor".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $columna_referencia
 * @property string $columna_codigo
 * @property string $columna_precio
 * @property string $columna_nombre
 * @property string $columna_marca
 * @property integer $descuento
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\User $createdBy
  * @property \app\models\ProductoDistribuidor[] $productosDistribuidor
 */
class Distribuidor extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'columna_referencia', 'columna_precio', 'columna_nombre'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['descuento','created_by', 'updated_by'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['columna_referencia', 'columna_codigo', 'columna_precio', 'columna_nombre', 'columna_marca'], 'string', 'max' => 45]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribuidor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'columna_referencia' => Yii::t('app', 'Columna Referencia'),
            'columna_codigo' => Yii::t('app', 'Columna Codigo'),
            'columna_precio' => Yii::t('app', 'Columna Precio'),
            'columna_nombre' => Yii::t('app', 'Columna Nombre'),
            'columna_marca' => Yii::t('app', 'Columna Marca'),
            'descuento' => Yii::t('app', 'Descuento'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductosDistribuidor()
    {
        return $this->hasMany(\app\models\ProductoDistribuidor::className(), ['distribuidor_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
