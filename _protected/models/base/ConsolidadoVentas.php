<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "consolidado_ventas".
 *
 * @property string $producto_codigo
 * @property string $fecha
 * @property integer $ventas
 * @property integer $pedidos
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class ConsolidadoVentas extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_codigo', 'fecha'], 'required'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['ventas', 'pedidos', 'created_by', 'updated_by'], 'integer'],
            [['producto_codigo'], 'string', 'max' => 20]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consolidado_ventas';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'producto_codigo' => Yii::t('app', 'Producto Codigo'),
            'fecha' => Yii::t('app', 'Fecha'),
            'ventas' => Yii::t('app', 'Ventas'),
            'pedidos' => Yii::t('app', 'Pedidos'),
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
