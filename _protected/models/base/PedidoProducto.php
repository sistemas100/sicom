<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "pedido_producto".
 *
 * @property string $producto_id
 * @property integer $pedido_id
 * @property integer $cantidad
 * @property integer $cantidad_disponible
 * @property string $precio_und
 * @property string $observacion
 * @property integer $estado
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Pedido $pedido
 * @property \app\models\Producto $producto
 */
class PedidoProducto extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id', 'pedido_id', 'cantidad'], 'required'],
            [['producto_id', 'pedido_id', 'cantidad', 'cantidad_disponible', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['precio_und'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['observacion'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pedido_producto';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'producto_id' => Yii::t('app', 'Producto ID'),
            'pedido_id' => Yii::t('app', 'Pedido ID'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'cantidad_disponible' => Yii::t('app', 'Cantidad Disponible'),
            'precio_und' => Yii::t('app', 'Precio Und'),
            'observacion' => Yii::t('app', 'Observacion'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedido()
    {
        return $this->hasOne(\app\models\Pedido::className(), ['id' => 'pedido_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(\app\models\Producto::className(), ['id' => 'producto_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
}
