<?php

namespace app\models;

use \app\models\base\ConsolidadoVentas as BaseConsolidadoVentas;

/**
 * This is the model class for table "consolidado_ventas".
 */
class ConsolidadoVentas extends BaseConsolidadoVentas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['producto_codigo', 'fecha'], 'required'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['ventas', 'pedidos', 'created_by', 'updated_by'], 'integer'],
            [['producto_codigo'], 'string', 'max' => 20]
        ]);
    }
	
}
