<?php

namespace app\models;

use \app\models\base\PedidoProducto as BasePedidoProducto;

/**
 * This is the model class for table "pedido_producto".
 */
class PedidoProducto extends BasePedidoProducto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['producto_id', 'pedido_id', 'cantidad'], 'required'],
            [['producto_id', 'pedido_id', 'cantidad', 'cantidad_disponible', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['precio_und'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['observacion'], 'string', 'max' => 255]
        ]);
    }
	
}
