<?php

namespace app\models;

use \app\models\base\LiquidacionItem as BaseLiquidacionItem;

/**
 * This is the model class for table "liquidacion_item".
 */
class LiquidacionItem extends BaseLiquidacionItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['liquidacion_id'], 'required'],
            [['liquidacion_id', 'item', 'created_by', 'updated_by'], 'integer'],
            [['moneda'], 'string'],
            [['usdcop', 'eurusd', 'valor', 'valor_cop'], 'number'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
