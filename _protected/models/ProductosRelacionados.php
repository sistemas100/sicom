<?php

namespace app\models;

use Yii;
use \app\models\base\ProductosRelacionados as BaseProductosRelacionados;

/**
 * This is the model class for table "productos_relacionados".
 */
class ProductosRelacionados extends BaseProductosRelacionados
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['producto', 'relacion'], 'required'],
            [['producto', 'relacion'], 'string', 'max' => 20]
        ]);
    }
	
}
