<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductoDistribuidor;

/**
 * app\models\ProductoDistribuidorSearch represents the model behind the search form about `app\models\ProductoDistribuidor`.
 */
class ProductoDistribuidorSearch extends ProductoDistribuidor {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'distribuidor_id', 'created_by', 'updated_by'], 'integer'],
            [['precio', 'referencia', 'nombre', 'marca', 'fecha', 'created_at', 'updated_at'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductoDistribuidor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);



        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'distribuidor_id' => $this->distribuidor_id,
            'fecha' => $this->fecha,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterCompare('precio', $this->precio);
//        echo $this->referencia;
//        die;



        $referenciaPre = explode(' ', preg_replace('/\s+/', ' ', trim($this->referencia)));
        $referencia = $referenciaPre;

//        echo "<pre>";
//        print_R($referenciaPre);
//        echo "</pre>";
//            die;

        foreach ($referenciaPre as $co => $pre) {

            $sql = "select 
                                                                referencia.referencia as rR,
                                                                producto.referencia as pR,
                                                                referencias2.referencia as rRR
                                                        from 
                                                                referencia
                                                        inner join producto on producto.id = referencia.producto_id
                                                        inner join referencia as referencias2 on producto.id = referencias2.producto_id 
                                                        where referencia.referencia = '$pre'";


            $referenciasTabla = Yii::$app->db->createCommand($sql)->queryAll();
            foreach ($referenciasTabla as $r1) {
                if ($r1["pR"] == 0) {
                    
                } else {

                    $referencia[] = $r1["rR"];
                    $referencia[] = $r1["pR"];
                    $referencia[] = $r1["rRR"];
                }
            }
        }


//        die;

        $referencia = array_unique($referencia);
//        echo "<pre>";
//        print_R($referencia);
//        echo "</pre>";
//        print_R($referencia);
//        die;
        $query->andFilterWhere(['or like', 'referencia', $referencia])
                ->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'marca', $this->marca]);
        return $dataProvider;
    }

}
