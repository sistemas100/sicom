<?php

namespace app\models;

use \app\models\base\ListItem as BaseListItem;

/**
 * This is the model class for table "list_item".
 */
class ListItem extends BaseListItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'list'], 'required'],
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['list', 'value'], 'string', 'max' => 45],
            [['id'], 'unique']
        ]);
    }
	
}
