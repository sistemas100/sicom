<?php

namespace app\models;

use \app\models\base\Distribuidor as BaseDistribuidor;

/**
 * This is the model class for table "distribuidor".
 */
class Distribuidor extends BaseDistribuidor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nombre', 'columna_referencia', 'columna_precio', 'columna_nombre'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['descuento', 'created_by', 'updated_by'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['columna_referencia', 'columna_codigo', 'columna_precio', 'columna_nombre', 'columna_marca'], 'string', 'max' => 45]
        ]);
    }
	
}
