<?php

namespace app\models;

use Yii;
use \app\models\base\FormatoCorreo as BaseFormatoCorreo;

/**
 * This is the model class for table "formato_correo".
 */
class FormatoCorreo extends BaseFormatoCorreo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['asunto', 'cuerpo_texto', 'idioma', 'estado', 'estado_previo'], 'required'],
            [['estado', 'estado_previo', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['asunto'], 'string', 'max' => 255],
            [['cuerpo_texto', 'cuerpo_html'], 'string', 'max' => 1000],
            [['idioma'], 'string', 'max' => 20]
        ]);
    }
	
}
