<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProveedorProducto;

/**
 * app\models\ProveedorProductoSearch represents the model behind the search form about `app\models\ProveedorProducto`.
 */
 class ProveedorProductoSearch extends ProveedorProducto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['producto_id', 'proveedor_id', 'estado_', 'created_by', 'updated_by'], 'integer'],
            [['codigo','estado','referencia_proveedor', 'referencia_proveedor2', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProveedorProducto::find()->join('join', 'producto','producto.id=proveedor_producto.producto_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'producto_id' => $this->producto_id,
            'proveedor_id' => $this->proveedor_id,
            'codigo' => $this->codigo,
            'estado' => $this->estado,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'referencia_proveedor', $this->referencia_proveedor])
            ->andFilterWhere(['like', 'referencia_proveedor2', $this->referencia_proveedor2]);

        return $dataProvider;
    }
}
