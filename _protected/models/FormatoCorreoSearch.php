<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormatoCorreo;

/**
 * app\models\FormatoCorreoSearch represents the model behind the search form about `app\models\FormatoCorreo`.
 */
 class FormatoCorreoSearch extends FormatoCorreo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado', 'estado_previo', 'created_by', 'updated_by'], 'integer'],
            [['asunto', 'cuerpo_texto', 'cuerpo_html', 'idioma', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormatoCorreo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'estado' => $this->estado,
            'estado_previo' => $this->estado_previo,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'asunto', $this->asunto])
            ->andFilterWhere(['like', 'cuerpo_texto', $this->cuerpo_texto])
            ->andFilterWhere(['like', 'cuerpo_html', $this->cuerpo_html])
            ->andFilterWhere(['like', 'idioma', $this->idioma]);

        return $dataProvider;
    }
}
