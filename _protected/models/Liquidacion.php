<?php

namespace app\models;

use \app\models\base\Liquidacion as BaseLiquidacion;

/**
 * This is the model class for table "liquidacion".
 */
class Liquidacion extends BaseLiquidacion
{
    public $proveedor_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'required'],
            [['id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['valor_factura', 'total_gastos', 'impuestos', 'total_importacion', 'factor_importacion', 'factor_liquidacion'], 'number'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
    
    /**
     * @return ActiveQuery
     */
    public static function getStrPedidos($id)
    {
        $pedidos = ''; 
        $pedidoLogs = PedidoLog::findAll(['factura'=>$id]);
        foreach ($pedidoLogs as $pedidoLog) {
            $pedidos .= $pedidoLog->pedido->codigo_pedido.', ' ;
        }
        $pedidos = substr($pedidos, 0,-2);
        return $pedidos;
    }
	
}
