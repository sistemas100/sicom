<?php

namespace app\models;

use app\models\base\Proveedor as BaseProveedor;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "proveedor".
 */
class Proveedor extends BaseProveedor {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_replace_recursive(parent::rules(), [
            [['nombre', 'tipo', 'factor_pedido', 'frecuencia_pedido'], 'required'],
            [['tiempo_produccion', 'bo_aceptados', 'tiempo_transporte', 'frecuencia_pedido', 'created_by', 'updated_by'], 'integer'],
            [['factor_pedido', 'tipo'], 'number'],
            [['correo'], 'email'],
            [['created_at', 'updated_at'], 'safe'],
            [['nombre', 'contacto', 'direccion', 'correo'], 'string', 'max' => 255],
            [['nit', 'telefono', 'ciudad', 'columna_codigo', 'columna_cantidad', 'columna_precio', 'columna_pedido_id', 'idioma'], 'string', 'max' => 45]
        ]);
    }

    /**
     * @author Juan Velez <juanfv@gmail.com>
     * @return array
     */
    public static function getInfoProductosQ($proveedor_id, $estado = null, $pedido_id = null, $factura_id = null) {
        ini_set("memory_limit", "512M");
        ini_set('max_execution_time', 0);

        $params = [':proveedor_id' => $proveedor_id];
        $sql = "SELECT DISTINCT producto.id producto_id,
                        'PRODUCTO' as tipo,
                        codigo,
                        referencia,
                        referencia_proveedor,
                        referencia_proveedor2,
                        producto.nombre,
                        producto.nombre_proveedor,
                        ROUND((precio1*100)/85) precio1,
                        ROUND((precio2*100)/85) precio2,
                        ROUND(((precio4*100)/85),2) precio4,
                        existencia,
                        ctoprm,
                        upcompra,
                        ufcompra,
                        ufvta,
                        producto.estado estado_producto,
                        CONCAT(YEAR(fecha),'-', MONTH(fecha)) fecha,
                        pedidos,
                        ventas,
                        IF(producto.tiempo_produccion > 0, producto.tiempo_produccion, proveedor.tiempo_produccion) tiempo_produccion,
                        promedio_manual,
                        tiempo_transporte,
                        frecuencia_pedido,
                        factor_pedido,
                        bo_aceptados,
                        pedido_producto.estado estado," .
                        ($estado === 0 ? "pedido_producto.cantidad cantidad" : "
                        pedido_producto_log.cantidad cantidad,
                        pedido_producto_log.observacion observacion,
                        pedido_producto_log.precio costo,
                        pedido_producto_log.pedido_log_id pedido_log_id") . "
                FROM producto
                LEFT JOIN (	SELECT * FROM (	SELECT * FROM `consolidado_ventas` WHERE YEAR(consolidado_ventas.created_at) IN (YEAR(CURDATE()), YEAR(CURDATE())-1) ORDER BY fecha DESC) t GROUP BY `producto_codigo` ,MONTH(`fecha`)) t2 ON `producto_codigo` = `codigo`                LEFT JOIN proveedor_producto ON proveedor_producto.`producto_id` = producto.id
                LEFT JOIN proveedor ON proveedor_producto.proveedor_id = proveedor.id
                LEFT JOIN pedido_producto ON pedido_producto.producto_id = producto.id" . ($pedido_id ? " AND pedido_producto.pedido_id = $pedido_id" : "") .
                ($estado === 0 ? "" : " LEFT JOIN `pedido_producto_log` ON `pedido_producto_log`.`producto_id` = producto.id
                                    LEFT JOIN `pedido_log` ON `pedido_log`.id = `pedido_producto_log`.`pedido_log_id`") . " 
                WHERE 1 = 1" .
                ($factura_id ? " AND pedido_log.factura = $factura_id" : "") .
                ($pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id AND (producto.estado = 1 OR pedido_producto.estado = 1)" : "") .
                (!$pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id AND producto.estado = 1" : "") .
                ($estado === null && !$factura_id ? " AND proveedor_producto.proveedor_id = :proveedor_id AND producto.estado = 0" : "") .
                ($pedido_id && $estado !== 0 ? " AND (pedido_producto.pedido_id = $pedido_id OR (pedido_producto.pedido_id IS NULL AND pedido_log.pedido_id = $pedido_id))" : "") .
                " ORDER BY `codigo`, fecha DESC";
//        echo "<pre>";
//        print_R($sql);
//        echo "</pre>";
//        die;
        $infoProductos = Yii::$app->db->createCommand($sql)->bindValues($params)->queryAll();
        return $infoProductos;

//        $infoProductos = Yii::$app->db->createCommand($sql)
//                ->bindValues($params)
//                ->queryAll();
        return $infoProductos;
    }

    /**
     * @author Juan Velez <juanfv@gmail.com>
     * @return array
     */
    public static function getInfoProductosQRleacionados($proveedor_id, $estado = null, $pedido_id = null, $factura_id = null) {
        ini_set("memory_limit", "512M");
        ini_set('max_execution_time', 0);

        $params = [':proveedor_id' => $proveedor_id];
        $sql = "SELECT 
                        DISTINCT producto.id producto_id,
                        'PRODUCTO' as tipo,
                        codigo,
                        referencia,
                        referencia_proveedor,
                        referencia_proveedor2,
                        producto.nombre,
                        producto.nombre_proveedor,
                        ROUND((precio1*100)/85) precio1,
                        ROUND((precio2*100)/85) precio2,
                        ROUND(((precio4*100)/85),2) precio4,
                        existencia,
                        ctoprm,
                        upcompra,
                        ufcompra,
                        ufvta,
                        producto.estado estado_producto,
                        CONCAT(YEAR(fecha),'-', MONTH(fecha)) fecha,
                        pedidos,
                        ventas,
                        IF(producto.tiempo_produccion > 0, producto.tiempo_produccion, proveedor.tiempo_produccion) tiempo_produccion,
                        promedio_manual,
                        tiempo_transporte,
                        frecuencia_pedido,
                        factor_pedido,
                        bo_aceptados,
                        pedido_producto.estado estado," .
                        ($estado === 0 ? "pedido_producto.cantidad cantidad" : "
                        pedido_producto_log.cantidad cantidad,
                        pedido_producto_log.observacion observacion,
                        pedido_producto_log.precio costo,
                        pedido_producto_log.pedido_log_id pedido_log_id") . "
                FROM producto
                    LEFT JOIN (	SELECT * FROM (	SELECT * FROM `consolidado_ventas` WHERE YEAR(consolidado_ventas.created_at) IN (YEAR(CURDATE()), YEAR(CURDATE())-1) ORDER BY fecha DESC) t GROUP BY `producto_codigo` ,MONTH(`fecha`)) t2 ON `producto_codigo` = `codigo`
                    LEFT JOIN proveedor_producto ON proveedor_producto.`producto_id` = producto.id
                    LEFT JOIN proveedor ON proveedor_producto.proveedor_id = proveedor.id
                    LEFT JOIN pedido_producto ON pedido_producto.producto_id = producto.id" . ($pedido_id ? " AND pedido_producto.pedido_id = $pedido_id" : "") .
                    ($estado === 0 ? "" : " LEFT JOIN `pedido_producto_log` ON `pedido_producto_log`.`producto_id` = producto.id
                    LEFT JOIN `pedido_log` ON `pedido_log`.id = `pedido_producto_log`.`pedido_log_id`") . " 
                WHERE 1 = 1" .
                ($factura_id ? " AND pedido_log.factura = $factura_id" : "") .
                ($pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id AND (producto.estado = 1 OR pedido_producto.estado = 1)" : "") .
                (!$pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id AND producto.estado = 1" : "") .
                ($estado === null && !$factura_id ? " AND proveedor_producto.proveedor_id = :proveedor_id AND producto.estado = 0" : "") .
                ($pedido_id && $estado !== 0 ? " AND (pedido_producto.pedido_id = $pedido_id OR (pedido_producto.pedido_id IS NULL AND pedido_log.pedido_id = $pedido_id))" : "");

        $sql2 = "SELECT  
                    DISTINCT CONCAT(producto.id,'-',productos_relacionados.id) producto_id,
                    concat('PRODUCTO RELACIONADO de: ',p2.codigo) as tipo,
                    producto.codigo as codigo,
                    producto.referencia,
                    referencia_proveedor,
                    referencia_proveedor2,
                    producto.nombre,
                    producto.nombre_proveedor,
                    ROUND((producto.precio1*100)/85) precio1,
                    ROUND((producto.precio2*100)/85) precio2,
                    ROUND(((producto.precio4*100)/85),2) precio4,
                    producto.existencia,
                    producto.ctoprm,
                    producto.upcompra,
                    producto.ufcompra,
                    producto.ufvta,
                    producto.estado estado_producto,
                    CONCAT(YEAR(fecha),'-', MONTH(fecha)) fecha,
                    pedidos,
                    ventas,
                    IF(producto.tiempo_produccion > 0, producto.tiempo_produccion, proveedor.tiempo_produccion) tiempo_produccion,
                    producto.promedio_manual,
                    tiempo_transporte,
                    frecuencia_pedido,
                    factor_pedido,
                    bo_aceptados,
                    pedido_producto.estado estado," .
                    ($estado === 0 ? "pedido_producto.cantidad cantidad" : "
                    pedido_producto_log.cantidad cantidad,
                    pedido_producto_log.observacion observacion,
                    pedido_producto_log.precio costo, 
                    pedido_producto_log.pedido_log_id pedido_log_id
                    ") . "
                FROM productos_relacionados
                LEFT JOIN producto on productos_relacionados.producto = producto.codigo 
                LEFT JOIN producto p2 on productos_relacionados.relacion = p2.codigo
                LEFT JOIN (SELECT * FROM (SELECT * FROM `consolidado_ventas` ORDER BY fecha DESC) t GROUP BY `producto_codigo` ,MONTH(`fecha`)) t2 ON t2.`producto_codigo` = producto.codigo                
                -- LEFT JOIN consolidado_ventas  ON `producto_codigo` = producto.codigo
                LEFT JOIN proveedor_producto ON proveedor_producto.`producto_id` = p2.id
                LEFT JOIN proveedor ON proveedor_producto.proveedor_id = proveedor.id
                LEFT JOIN pedido_producto ON pedido_producto.producto_id = p2.id" . ($pedido_id ? " AND pedido_producto.pedido_id = $pedido_id" : "") .
                ($estado === 0 ? "" : " LEFT JOIN `pedido_producto_log` ON `pedido_producto_log`.`producto_id` = p2.id
                LEFT JOIN `pedido_log` ON `pedido_log`.id = `pedido_producto_log`.`pedido_log_id`") . " 
                WHERE 1 = 1 " .
                ($factura_id ? " AND pedido_log.factura = $factura_id" : "") .
                ($pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id " : "") .
                (!$pedido_id && $estado === 0 ? " AND proveedor_producto.proveedor_id = :proveedor_id AND p2.estado = 1" : "") .
                ($estado === null && !$factura_id ? " AND proveedor_producto.proveedor_id = :proveedor_id AND p2.estado = 0" : "") .
                ($pedido_id && $estado !== 0 ? " AND (pedido_producto.pedido_id = $pedido_id OR (pedido_producto.pedido_id IS NULL AND pedido_log.pedido_id = $pedido_id))" : "") .
                " ";

        $sqlFinal = $sql . " UNION ALL " . $sql2;
//        $sqlFinal = $sql;

        echo "<pre style='display:none' id='CampaID'>";
//        print_R($sql);
        print_R($sqlFinal);
        echo "</pre>";
//        die;

        $infoProductos = Yii::$app->db->createCommand($sqlFinal)->bindValues($params)->queryAll();
        return $infoProductos;
    }

    /**
     * @author Juan Velez <juanfv@gmail.com>
     * @return array
     */
    public static function getInfoProductos($model_pedido) {
        if (strtolower(Yii::$app->controller->id) == "pedido" && Yii::$app->controller->action->id == "view") {
            $infoProductos = Proveedor::getInfoProductosQRleacionados($model_pedido->proveedor_id, $model_pedido->estado, $model_pedido->id);
//            echo "ore";
        } else {
//            echo "aca";
            $infoProductos = Proveedor::getInfoProductosQ($model_pedido->proveedor_id, $model_pedido->estado, $model_pedido->id);
        }
//            $infoProductos = Proveedor::getInfoProductosQ($model_pedido->proveedor_id, $model_pedido->estado, $model_pedido->id);
//        die;
        //filtramos los productos repetidos y guardamos las fechas y cantidades pedidas y vendidas
        $productos = [];
        $produto_id = '';
        $keyAnt = '';
        $fechas = [];
        for ($index = 12; $index >= 0; $index--) {
            $fecha = date("Y-n", strtotime("-$index months", strtotime(date("Y-m-01", strtotime($model_pedido->created_at)))));
            $fechas[] = $fecha;
        }
        $bo_aceptados = $model_pedido->proveedor->bo_aceptados ? $model_pedido->proveedor->bo_aceptados : 0;
        $bos = Proveedor::getBos($model_pedido->proveedor_id, $bo_aceptados);
        $arrPedidos = [];
        $repetidos = ProveedorProducto::getProductosRepetidos();
        $relacionados = ProveedorProducto::getProductosRelacionados();
        foreach ($infoProductos as $key => $producto) {
            if ($produto_id !== $producto['producto_id']) {
                $producto['repetido'] = isset($repetidos[$producto['producto_id']]) ? $repetidos[$producto['producto_id']] : '';
                $producto['relacionado'] = isset($relacionados[$producto['codigo']]) ? substr($relacionados[$producto['codigo']], 0, -2) : '';
                $productos[$key] = $producto;
                $keyAnt = $key;
                foreach ($fechas as $fecha) {
                    $productos[$key][$fecha] = $producto['fecha'] == $fecha ? ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']] : ['pedidos' => 0, 'ventas' => 0];
                }
                $cantidad_bos = 0;
                foreach ($bos as $bo) {
                    if ($producto['producto_id'] === $bo['producto_id'] && $cantidad_bos <= $bo_aceptados && $bo_aceptados > 0) {
                        $productos[$key][$bo['codigo_pedido']] = (string) ($bo['cantidadp'] - $bo['cantidadd']) . ($bo['cantidada'] != 0 ? '(' . (string) $bo['cantidada'] . ')' : '');
                        $arrPedidos[] = $bo['codigo_pedido'];
                        $productos[$key]['cantidad'] = $bo['cantidadp'] + $bo['cantidadd'];
                        $cantidad_bos++;
                    }
                }
                $productos[$key]['estado'] = $producto['estado'];
                $productos[$key]['cantidadP'] = $producto['cantidad'];
            } else {
                foreach ($fechas as $fecha) {
                    if ($producto['fecha'] == $fecha) {
                        $productos[$keyAnt][$fecha] = ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']];
                    }
                }
            }
            $produto_id = $producto['producto_id'];
        }
        $bos = array_unique($arrPedidos);
        $precios = Producto::getPreciosCompetencia($model_pedido->proveedor_id);
        return [
            'model' => $model_pedido,
            'productos' => $productos,
            'fechas' => $fechas,
            'bos' => $bos,
            'precios' => $precios,
        ];
    }

    /**
     * @author Juan Velez <juanfv@gmail.com>
     * @return ActiveQuery
     */
    public static function getBos($proveedor_id, $limit) {

        $proveedoresBO = Yii::$app->db->createCommand("SELECT DISTINCT(proveedor_id) 
        FROM `proveedor_producto`
        WHERE producto_id IN(
	SELECT `producto_id` 
	FROM `proveedor_producto`
	WHERE  proveedor_id =( :proveedor_id)
	 )", [':proveedor_id' => $proveedor_id])->queryColumn();
        //$proveedoresBO = "'".implode("','", array_values($proveedoresBO))."'";
        $pedidosBO = '';
        foreach ($proveedoresBO as $value) {


            $pedidosBOResult = Yii::$app->db->createCommand("
            SELECT DISTINCT codigo_pedido
        FROM(
        SELECT `codigo_pedido`,`producto_id`,SUM(`cantidadp`) cantidadp,SUM(`cantidadd`) cantidadd,SUM(`cantidada`) cantidada
            FROM( SELECT `pedido_id`,`producto_id`, `pedido_producto_log`.`cantidad` cantidadp, 0 cantidadd, 0 cantidada 
                    FROM (SELECT * FROM `pedido_log` WHERE `pedido_log`.estado IN (1,2) ORDER BY `pedido_log`.id DESC ) pedido_log 
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    GROUP BY pedido_id,`producto_id` 
                    UNION 
                    SELECT `pedido_id`,`producto_id`,0 cantidadp, SUM(`pedido_producto_log`.`cantidad`) cantidadd, 0 cantidada 
                    FROM `pedido_log` 
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    JOIN `liquidacion` ON `liquidacion`.`id` = pedido_log.factura 
                    WHERE `pedido_log`.estado IN('4') AND `liquidacion`.estado = 1 
                    GROUP BY pedido_id ,`producto_id` 
                    UNION 
                    SELECT `pedido_id`,`producto_id`,0 cantidadp, 0 cantidadd, SUM(`pedido_producto_log`.`cantidad`) cantidada 
                    FROM pedido
                    JOIN `pedido_log` ON pedido.id = pedido_id
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    WHERE `pedido_log`.estado = 2 AND pedido.estado = 3
                    GROUP BY pedido_id ,`producto_id` )t 
            JOIN `pedido` ON `pedido`.id = t.`pedido_id` 
            WHERE `pedido`.estado > 1 AND pedido.proveedor_id = :proveedor_id
            GROUP BY `pedido_id` ,`producto_id` 
            HAVING cantidadp <> cantidadd) t2
            ORDER BY codigo_pedido DESC
            LIMIT :limit", [':proveedor_id' => $value, ':limit' => $limit])->queryColumn();
            $pedidosBO .= "'" . implode("','", array_values($pedidosBOResult)) . "',";
        }
        $pedidosBO = substr($pedidosBO, 0, -1);

        $rowsBo = Yii::$app->db->createCommand("
            SELECT `pedido_id`,`codigo_pedido`,`producto_id`,SUM(`cantidadp`) cantidadp,SUM(`cantidadd`) cantidadd,SUM(`cantidada`) cantidada 
            FROM( SELECT `pedido_id`,`producto_id`, `pedido_producto_log`.`cantidad` cantidadp, 0 cantidadd, 0 cantidada 
                    FROM (SELECT * FROM `pedido_log` WHERE `pedido_log`.estado  IN (1,2) ORDER BY `pedido_log`.id DESC ) pedido_log 
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    GROUP BY pedido_id,`producto_id` 
                    UNION 
                    SELECT `pedido_id`,`producto_id`,0 cantidadp, SUM(`pedido_producto_log`.`cantidad`) cantidadd, 0 cantidada 
                    FROM `pedido_log` 
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    JOIN `liquidacion` ON `liquidacion`.`id` = pedido_log.factura 
                    WHERE `pedido_log`.estado IN('4') AND `liquidacion`.estado = 1 
                    GROUP BY pedido_id ,`producto_id` 
                    UNION 
                    SELECT `pedido_id`,`producto_id`,0 cantidadp, 0 cantidadd, SUM(`pedido_producto_log`.`cantidad`) cantidada 
                    FROM pedido
                    JOIN `pedido_log` ON pedido.id = pedido_id
                    JOIN `pedido_producto_log` ON `pedido_producto_log`.`pedido_log_id` = `pedido_log`.id 
                    WHERE `pedido_log`.estado = 2 AND pedido.estado = 3
                    GROUP BY pedido_id ,`producto_id` )t 
            JOIN `pedido` ON `pedido`.id = t.`pedido_id` 
            WHERE `pedido`.estado > 1 AND codigo_pedido IN (" . $pedidosBO . ")
            GROUP BY `pedido_id` ,`producto_id` 
            HAVING cantidadp <> cantidadd")->queryAll();
        return $rowsBo;
    }

    /**
     * @author Juan Velez <juanfv@gmail.com>
     * @return ActiveQuery
     */
    public static function getCodigoPedido($proveedor_id) {
        $row = Yii::$app->db->createCommand("
            SELECT CONCAT(SUBSTRING(nombre, 1, 3), DATE_FORMAT(NOW(),'%m%y'),'-',(COUNT(pedido.id)+1)) codigo
            FROM pedido
            JOIN proveedor ON proveedor_id = proveedor.id
            WHERE MONTH(pedido.`created_at`) = MONTH(NOW()) AND proveedor_id = $proveedor_id   
            ")->queryOne();
        $codigo_pedido = $row['codigo'];
        return $codigo_pedido;
    }

}
