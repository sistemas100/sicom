<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proveedor;

/**
 * app\models\ProveedorSearch represents the model behind the search form about `app\models\Proveedor`.
 */
 class ProveedorSearch extends Proveedor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tiempo_produccion','bo_aceptados', 'tiempo_transporte', 'created_by', 'updated_by','frecuencia_pedido'], 'integer'],
            [['nombre', 'nit', 'contacto', 'telefono', 'direccion', 'ciudad','correo', 'columna_codigo', 'columna_cantidad', 'columna_precio', 'columna_pedido_id', 'created_at', 'updated_at'], 'safe'],
            [['factor_pedido'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proveedor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tiempo_produccion' => $this->tiempo_produccion,
            'tiempo_transporte' => $this->tiempo_transporte,
            'factor_pedido' => $this->factor_pedido,
            'bo_aceptados' => $this->bo_aceptados,
            'frecuencia_pedido' => $this->frecuencia_pedido,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'nit', $this->nit])
            ->andFilterWhere(['like', 'contacto', $this->contacto])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'ciudad', $this->ciudad])
            ->andFilterWhere(['like', 'correo', $this->ciudad])
            ->andFilterWhere(['like', 'columna_codigo', $this->columna_codigo])
            ->andFilterWhere(['like', 'columna_cantidad', $this->columna_cantidad])
            ->andFilterWhere(['like', 'columna_precio', $this->columna_precio])
            ->andFilterWhere(['like', 'columna_pedido_id', $this->columna_pedido_id]);

        return $dataProvider;
    }
}
