<?php

namespace app\models;

use \app\models\base\Referencia as BaseReferencia;

/**
 * This is the model class for table "referencia".
 */
class Referencia extends BaseReferencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['referencia', 'producto_id'], 'required'],
            [['producto_id', 'tipo'], 'integer'],
            [['referencia'], 'string', 'max' => 255],
//            [['producto_id', 'referencia'], 'unique', 'targetAttribute' => ['producto_id', 'referencia'], 'message' => 'The combination of Referencia and Producto ID has already been taken.']
        ]);
    }
	
}
