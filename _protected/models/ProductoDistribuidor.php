<?php

namespace app\models;

use \app\models\base\ProductoDistribuidor as BaseProductoDistribuidor;

/**
 * This is the model class for table "producto_distribuidor".
 */
class ProductoDistribuidor extends BaseProductoDistribuidor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['referencia', 'nombre', 'distribuidor_id'], 'required'],
            [['precio'], 'number'],
            [['distribuidor_id', 'created_by', 'updated_by'], 'integer'],
            [['fecha','created_at', 'updated_at'], 'safe'],
            [['codigo', 'marca'], 'string', 'max' => 45],
            [['referencia', 'nombre'], 'string', 'max' => 255]
        ]);
    }
	
}
