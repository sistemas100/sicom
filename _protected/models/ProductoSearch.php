<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * app\models\ProductoSearch represents the model behind the search form about `app\models\Producto`.
 */
class ProductoSearch extends Producto {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'estado', 'created_by', 'updated_by', 'bajo_stock'], 'integer'],
            [['existencia', 'precio1', 'precio2', 'precio4', 'ctoprm', 'upcompra', 'promedio_manual', 'codigo', 'referencia', 'nombre', 'nombre_proveedor', 'created_at', 'updated_at', 'meses', 'tipo', 'ufcompra', 'ufvta', 'bo', 'duracion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchOld($params) {
        $query = Producto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->select("p1.*,
            IF(promedio_manual > 0, ROUND(((existencia + IF(bo IS NULL, (0), (bo)))/promedio_manual),(1)), IF(promedio > 0, ROUND(((existencia + IF(bo IS NULL, (0), (bo)))/promedio),(1)),(0))) meses,
            IF(p1.tiempo_produccion > 0, p1.tiempo_produccion, proveedor.tiempo_produccion) + proveedor.tiempo_transporte + proveedor.frecuencia_pedido >= IF(promedio_manual > 0, ((existencia + IF(bo IS NULL, (0), (bo)))/promedio_manual), ((existencia + IF(bo IS NULL, (0), (bo)))/promedio))*30 bajo_stock,
            IF(bo IS NULL,(0),(bo)) bo,
            ((p1.ufvta)-(p1.ufcompra)) duracion");
        $query->from('producto p1');
        if ($this->join) {
            $query->join('JOIN', 'referencia p2', 'p1.`id` = p2.producto_id');
            $query->join('JOIN', 'proddist_referencia p3', 'p2.`id` = p3.referencia_id');
        }
        $query->join('LEFT JOIN', "(SELECT pedido_id,codigo_pedido,producto_id, SUM(cantidadp) cantidadp, SUM(cantidadd) cantidadd, (SUM(cantidadp)-SUM(cantidadd)) bo
FROM( SELECT pedido_id,producto_id, pedido_producto_log.cantidad cantidadp, 0 cantidadd 
    FROM (SELECT * FROM pedido_log WHERE pedido_log.estado = 1 ORDER BY pedido_log.id DESC ) pedido_log 
    JOIN pedido_producto_log ON pedido_producto_log.pedido_log_id = pedido_log.id 
    GROUP BY pedido_id,producto_id 
    UNION 
    SELECT pedido_id,producto_id,0 cantidadp, SUM(pedido_producto_log.cantidad) cantidadd
    FROM pedido_log 
    JOIN pedido_producto_log ON pedido_producto_log.pedido_log_id = pedido_log.id 
    JOIN liquidacion ON liquidacion.id = pedido_log.factura 
    WHERE pedido_log.estado IN('4') AND liquidacion.estado = 1 
    GROUP BY pedido_id, producto_id)t 
JOIN (SELECT proveedor_id, Y.id,codigo_pedido,estado
	FROM (
		SELECT
			id,
			codigo_pedido,
			estado,
			@r := CASE WHEN @c=proveedor_id THEN @r+1 ELSE 1 END rownum,
			@c := proveedor_id proveedor_id 
		FROM (SELECT @r :=0 , @c := '') X, pedido
		ORDER BY proveedor_id, id DESC
	) Y
	JOIN proveedor ON proveedor_id = proveedor.id
	WHERE rownum <= bo_aceptados) pedido 
ON pedido.id = t.pedido_id 
WHERE pedido.estado > 1
GROUP BY producto_id 
HAVING cantidadp <> cantidadd) bo", 'p1.id = bo.producto_id');
        $query->join('LEFT JOIN', "( SELECT producto_codigo, (SUM(ventas)/12) promedio
			FROM consolidado_ventas
			WHERE DATE_FORMAT(fecha,'%Y-%m') < DATE_FORMAT(NOW(),'%Y-%m') AND
				DATE_FORMAT(fecha,'%Y-%m') >= DATE_FORMAT(NOW() - INTERVAL 12 MONTH,'%Y-%m')
		GROUP BY producto_codigo) ventas", 'producto_codigo = codigo');

        $query->join('LEFT JOIN', 'proveedor_producto', 'proveedor_producto.producto_id = p1.id');
        $query->join('LEFT JOIN', 'proveedor', 'proveedor_producto.proveedor_id = proveedor.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'ufcompra' => $this->ufcompra ? 25569 + round(strtotime($this->ufcompra) / 86400) : '',
            'ufvta' => $this->ufvta ? 25569 + round(strtotime($this->ufvta) / 86400) : '',
            'estado' => $this->estado,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        $referencia = explode(' ', preg_replace('/\s+/', ' ', trim($this->referencia)));
        $codigo = $this->codigo ? explode(' ', $this->codigo) : $this->codigo;
        $query->andFilterCompare('p1.tipo', $this->tipo);
        $query->andFilterCompare('p1.precio1', $this->precio1);
        $query->andFilterCompare('p1.precio2', $this->precio2);
        $query->andFilterCompare('p1.precio4', $this->precio4);
        $query->andFilterCompare('p1.existencia', $this->existencia);
        $query->andFilterCompare('p1.promedio_manual', $this->promedio_manual);
        $query->andFilterCompare('p1.tipo', $this->ctoprm);
        $query->andFilterCompare('p1.tipo', $this->upcompra);
        $query->andFilterWhere(['in', 'p1.codigo', $codigo])
                ->andFilterWhere(['or like', 'p1.referencia', $referencia])
                ->andFilterWhere(['like', 'p1.nombre', $this->nombre])
                ->andFilterWhere(['like', 'p1.nombre_proveedor', $this->nombre_proveedor]);

        $query->andFilterHaving($this->getOperator($this->bo, 'bo'));
        $query->andFilterHaving($this->getOperator($this->meses, 'meses'));
        $query->andFilterHaving($this->getOperator($this->duracion, 'duracion'));
        $query->andFilterHaving(['bajo_stock' => $this->bajo_stock]);
        $query->with('referencias', 'referencias.proddists');

        return $dataProvider;
    }

    public function searchCm($params) {
        $sql = "Select 
                    d.codigo as codigo, 
                    d.referencia as referencia, 
                    d.nombre as nombre, 
                    d.nombre_proveedor as Marca, 
                    d.precio1 as precio,  
                    a.referencia as RefR, 
                    c.nombre as NombreR, 
                    b.precio as PrecioR, 
                    b.marca as MarcaR,
                    c.fecha as Fecha
                from referencia a 
                inner join producto_distribuidor b on b.referencia = a.referencia
                inner join distribuidor c on c.id = b.distribuidor_id
                inner join producto d on d.id = a.producto_id
                where d.codigo IN ($params)
                order by c.nombre";
        
//        echo "<pre>";
//        print_R($sql);
//        echo "</pre>";

        $query = Yii::$app->db->createCommand($sql)->queryAll();
        return $query;
    }

    public function search($params) {
        $query = Producto::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->select("p1.*,
            IF(promedio_manual > 0, ROUND(((existencia + IF(bo IS NULL, (0), (bo)))/promedio_manual),(1)), IF(promedio > 0, ROUND(((existencia + IF(bo IS NULL, (0), (bo)))/promedio),(1)),(0))) meses,
            IF(p1.tiempo_produccion > 0, p1.tiempo_produccion, proveedor.tiempo_produccion) + proveedor.tiempo_transporte + proveedor.frecuencia_pedido >= IF(promedio_manual > 0, ((existencia + IF(bo IS NULL, (0), (bo)))/promedio_manual), ((existencia + IF(bo IS NULL, (0), (bo)))/promedio))*30 bajo_stock,
            IF(bo IS NULL,(0),(bo)) bo,
            ((p1.ufvta)-(p1.ufcompra)) duracion");
        $query->from('producto p1');
        if ($this->join) {
            $query->join('JOIN', 'referencia p2', 'p1.`id` = p2.producto_id');
//            $query->join('JOIN','proddist_referencia p3',  'p2.`id` = p3.referencia_id');
        }
        $query->join('LEFT JOIN', "(SELECT pedido_id,codigo_pedido,producto_id, SUM(cantidadp) cantidadp, SUM(cantidadd) cantidadd, (SUM(cantidadp)-SUM(cantidadd)) bo
                    FROM( 
                        SELECT pedido_id,producto_id, pedido_producto_log.cantidad cantidadp, 0 cantidadd 
                        FROM (
                            SELECT * FROM pedido_log WHERE pedido_log.estado = 1 ORDER BY pedido_log.id DESC ) pedido_log 
                            JOIN pedido_producto_log ON pedido_producto_log.pedido_log_id = pedido_log.id AND YEAR(pedido_producto_log.created_at) IN (YEAR(CURDATE()))
                        GROUP BY pedido_id,producto_id 
                        UNION 
                        SELECT pedido_id,producto_id,0 cantidadp, SUM(pedido_producto_log.cantidad) cantidadd
                        FROM pedido_log 
                        JOIN pedido_producto_log ON pedido_producto_log.pedido_log_id = pedido_log.id  AND YEAR(pedido_producto_log.created_at) IN (YEAR(CURDATE()))
                        JOIN liquidacion ON liquidacion.id = pedido_log.factura 
                        WHERE pedido_log.estado IN('4') AND liquidacion.estado = 1 
                        GROUP BY pedido_id, producto_id)t 
                    JOIN (SELECT proveedor_id, Y.id,codigo_pedido,estado
                            FROM (
                                    SELECT
                                            id,
                                            codigo_pedido,
                                            estado,
                                            @r := CASE WHEN @c=proveedor_id THEN @r+1 ELSE 1 END rownum,
                                            @c := proveedor_id proveedor_id 
                                    FROM (SELECT @r :=0 , @c := '') X, pedido
                                    ORDER BY proveedor_id, id DESC
                            ) Y
                            JOIN proveedor ON proveedor_id = proveedor.id
                            WHERE rownum <= bo_aceptados) pedido 
                    ON pedido.id = t.pedido_id 
                    WHERE pedido.estado > 1
                    GROUP BY producto_id 
                    HAVING cantidadp <> cantidadd) bo", 'p1.id = bo.producto_id');
        $query->join('LEFT JOIN', "( SELECT producto_codigo, (SUM(ventas)/12) promedio
			FROM consolidado_ventas
			WHERE DATE_FORMAT(fecha,'%Y-%m') < DATE_FORMAT(NOW(),'%Y-%m') AND
				DATE_FORMAT(fecha,'%Y-%m') >= DATE_FORMAT(NOW() - INTERVAL 12 MONTH,'%Y-%m')
		GROUP BY producto_codigo) ventas", 'producto_codigo = codigo');

        $query->join('LEFT JOIN', 'proveedor_producto', 'proveedor_producto.producto_id = p1.id');
        $query->join('LEFT JOIN', 'proveedor', 'proveedor_producto.proveedor_id = proveedor.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'ufcompra' => $this->ufcompra ? 25569 + round(strtotime($this->ufcompra) / 86400) : '',
            'ufvta' => $this->ufvta ? 25569 + round(strtotime($this->ufvta) / 86400) : '',
            'estado' => $this->estado,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        $referencia = explode(' ', preg_replace('/\s+/', ' ', trim($this->referencia)));
        $codigo = $this->codigo ? explode(' ', $this->codigo) : $this->codigo;
        $query->andFilterCompare('p1.tipo', $this->tipo);
        $query->andFilterCompare('p1.precio1', $this->precio1);
        $query->andFilterCompare('p1.precio2', $this->precio2);
        $query->andFilterCompare('p1.precio4', $this->precio4);
        $query->andFilterCompare('p1.existencia', $this->existencia);
        $query->andFilterCompare('p1.promedio_manual', $this->promedio_manual);
        $query->andFilterCompare('p1.tipo', $this->ctoprm);
        $query->andFilterCompare('p1.tipo', $this->upcompra);
        $query->andFilterWhere(['in', 'p1.codigo', $codigo])
                ->andFilterWhere(['or like', 'p1.referencia', $referencia])
                ->andFilterWhere(['like', 'p1.nombre', $this->nombre])
                ->andFilterWhere(['like', 'p1.nombre_proveedor', $this->nombre_proveedor]);

        $query->andFilterHaving($this->getOperator($this->bo, 'bo'));
        $query->andFilterHaving($this->getOperator($this->meses, 'meses'));
        $query->andFilterHaving($this->getOperator($this->duracion, 'duracion'));
        $query->andFilterHaving(['bajo_stock' => $this->bajo_stock]);
        $query->with('referencias', 'referencias.proddists');

        return $dataProvider;
    }

    public function getOperator($value, $name) {
        if (preg_match('/^(<>|>=|>|<=|<|=)/', $value, $matches)) {
            $operator = $matches[1];
            $value = substr($value, strlen($operator));
        } else {
            $operator = '=';
        }
        return [$operator, $name, $value];
    }

}
