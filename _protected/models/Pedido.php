<?php

namespace app\models;

use \app\models\base\Pedido as BasePedido;

/**
 * This is the model class for table "pedido".
 */
class Pedido extends BasePedido
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['proveedor_id', 'estado'], 'required'],
            [['proveedor_id', 'estado', 'created_by', 'updated_by'], 'integer'],
            [['fecha', 'created_at', 'updated_at'], 'safe'],
            [['codigo_pedido', 'codigo'], 'string', 'max' => 45],
            [['observacion'], 'string', 'max' => 255]
        ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastLog($estado)
    {
        return PedidoLog::find()->where(['pedido_id' => $this->id,'estado' => $estado])
                ->with('pedidoProductoLogs','pedidoProductoLogs.producto')
                ->orderBy('id DESC')->one();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getPenultimoLog($pedido_id,$estado)
    {
        return PedidoLog::find()->where('pedido_id = :pedido_id AND estado = :estado', [':pedido_id' => $pedido_id ,':estado' => $estado])
                ->with('pedidoProductoLogs','pedidoProductoLogs.producto')
                ->orderBy('id DESC')
                ->limit(1)->offset(1)->one();
    }
	
}
