<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Referencia */

$this->title = $model->referencia;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referencia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="referencia-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Referencia').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'referencia',
        [
                'attribute' => 'producto.nombre',
                'label' => Yii::t('app', 'Producto')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
