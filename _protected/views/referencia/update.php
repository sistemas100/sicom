<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Referencia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Referencia',
]) . ' ' . $model->referencia;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referencia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->referencia, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="referencia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
