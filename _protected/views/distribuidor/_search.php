<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DistribuidorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-distribuidor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'columna_referencia')->textInput(['maxlength' => true, 'placeholder' => 'Columna Referencia']) ?>

    <?= $form->field($model, 'columna_precio')->textInput(['maxlength' => true, 'placeholder' => 'Columna Precio']) ?>

    <?php /* echo $form->field($model, 'columna_nombre')->textInput(['maxlength' => true, 'placeholder' => 'Columna Nombre']) */ ?>

    <?php /* echo $form->field($model, 'columna_marca')->textInput(['maxlength' => true, 'placeholder' => 'Columna Marca']) */ ?>

    <?php /* echo $form->field($model, 'descuento')->textInput(['placeholder' => 'Descuento']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
