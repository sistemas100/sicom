<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Distribuidor').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'columna_referencia',
        'columna_precio',
        'columna_nombre',
        'columna_marca',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerProducto->totalCount){
    $gridColumnProducto = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'referencia',
        'nombre',
        'precio1',
        'precio2',
        'precio4',
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
        [
                'attribute' => 'proveedor.nombre',
                'label' => Yii::t('app', 'Proveedor')
            ],
        'nombre_proveedor',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerProducto,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Producto')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnProducto
    ]);
}
?>
    </div>
</div>
