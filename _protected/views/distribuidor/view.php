<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Distribuidor').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            <?= Html::a('<i class="fa glyphicon glyphicon-upload"></i> ' .Yii::t('app', 'Import'),
                ['distribuidor/import', 'id' => $model->id],
                ['class' => 'btn btn-primary',
                    'title' => Yii::t('app', 'Import')]) ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'columna_referencia',
        'columna_precio',
        'columna_nombre',
        'columna_marca',
        'descuento',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerProducto->totalCount){
    $gridColumnProducto = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'referencia',
            'nombre',
            [
                'attribute' => 'precio',
                'value' => function($model) {
                    return '$' . number_format($model->precio, 0);
                },
            ],
            'marca',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProducto,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-producto']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Producto')),
        ],
        'columns' => $gridColumnProducto
    ]);
}
?>
    </div>
</div>
