<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Distribuidor',
]) . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="distribuidor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
