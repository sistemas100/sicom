<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistribuidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Distribuidor');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="distribuidor-index">

    <h1><?= Html::encode($this->title) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <span class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Distribuidor'), ['create'], ['class' => 'btn btn-success']) ?>
    </span>
    </h1>
    <?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'columna_referencia',
        'columna_precio',
        'columna_nombre',
        'columna_marca',
        'descuento',
        'fecha',
        [ 
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {import}',
            'buttons' => ['view' => function ($url, $model) {
                            $url = Url::to(['distribuidor/view', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                        },
                        'update' => function ($url, $model) {
                            $url = Url::to(['distribuidor/update', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['distribuidor/delete', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                                                                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                                                                    'data-method' => 'post',
                            ]);
                        },
                        'import' => function ($url, $model) {
                            $url = Url::to(['distribuidor/import', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, ['title' => Yii::t('yii', 'Import')]);
                        }
                        ],
                        
                                
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-distribuidor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-th-large"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
