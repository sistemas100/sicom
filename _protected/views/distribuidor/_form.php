<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Producto', 
        'relID' => 'producto', 
        'value' => \yii\helpers\Json::encode($model->productosDistribuidor),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="distribuidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'columna_referencia')->textInput(['maxlength' => true, 'placeholder' => 'Columna Referencia']) ?>

    <?= $form->field($model, 'columna_precio')->textInput(['maxlength' => true, 'placeholder' => 'Columna Precio']) ?>
    
    <?= $form->field($model, 'columna_nombre')->textInput(['maxlength' => true, 'placeholder' => 'Columna Nombre']) ?>

    <?= $form->field($model, 'columna_marca')->textInput(['maxlength' => true, 'placeholder' => 'Columna Marca']) ?>
    
    <?= $form->field($model, 'descuento')->textInput(['placeholder' => 'Descuento']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
