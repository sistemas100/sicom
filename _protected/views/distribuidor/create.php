<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = Yii::t('app', 'Create Distribuidor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
