<?php

use app\models\FormatoCorreo;
use app\models\ListItem;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model FormatoCorreo */
/* @var $form ActiveForm */

?>

<div class="formato-correo-form">

    <?php $form = ActiveForm::begin(); 
        $estados = ArrayHelper::map(ListItem::find()->where(['list'=>'estado_pedido'])->asArray()->all(), 'code', 'value');
    ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'asunto')->textInput(['maxlength' => true, 'placeholder' => 'Asunto']) ?>

    <?= $form->field($model, 'cuerpo_texto')->textarea(['maxlength' => true, 'placeholder' => 'Cuerpo Texto']) ?>

    <?= $form->field($model, 'cuerpo_html')->textarea(['maxlength' => true, 'placeholder' => 'Cuerpo Html']) ?>

    <?= $form->field($model, 'idioma')->widget(Select2::classname(), [
        'data' => ['es'=>'ES','en'=>'EN'],
        'options' => ['placeholder' => Yii::t('app', 'Seleccione idioma')],
    ]); ?>
    <?= $form->field($model, 'estado')->widget(Select2::classname(), [
        'data' => $estados,
        'options' => ['placeholder' => Yii::t('app', 'Seleccione estado')],
    ]); ?>
    <?= $form->field($model, 'estado_previo')->widget(Select2::classname(), [
        'data' => $estados,
        'options' => ['placeholder' => Yii::t('app', 'Seleccione estado previo')],
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
