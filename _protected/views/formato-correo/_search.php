<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FormatoCorreoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-formato-correo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'asunto')->textInput(['maxlength' => true, 'placeholder' => 'Asunto']) ?>

    <?= $form->field($model, 'cuerpo_texto')->textInput(['maxlength' => true, 'placeholder' => 'Cuerpo Texto']) ?>

    <?= $form->field($model, 'cuerpo_html')->textInput(['maxlength' => true, 'placeholder' => 'Cuerpo Html']) ?>

    <?= $form->field($model, 'idioma')->textInput(['maxlength' => true, 'placeholder' => 'Idioma']) ?>

    <?php /* echo $form->field($model, 'estado')->textInput(['placeholder' => 'Estado']) */ ?>

    <?php /* echo $form->field($model, 'estado_previo')->textInput(['placeholder' => 'Estado Previo']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
