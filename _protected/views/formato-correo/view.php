<?php

use app\models\FormatoCorreo;
use app\models\ListItem;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model FormatoCorreo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Formato Correo'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="formato-correo-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Formato Correo').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $estados = ArrayHelper::map(ListItem::find()->where(['list'=>'estado_pedido'])->asArray()->all(), 'code', 'value');
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'asunto',
        'cuerpo_texto',
        'cuerpo_html',
        'idioma',
        [
            'attribute' => 'estado', 
            'value' => $estados[$model->estado]
        ],
        [
            'attribute' => 'estado_previo', 
            'value' => $estados[$model->estado_previo]
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
