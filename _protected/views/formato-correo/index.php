<?php

/* @var $this View */
/* @var $searchModel FormatoCorreoSearch */
/* @var $dataProvider ActiveDataProvider */

use app\models\FormatoCorreoSearch;
use app\models\ListItem;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::t('app', 'Formato Correo');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="formato-correo-index">
     <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
        <?= Html::a(Yii::t('app', 'Crear formato correo'), ['create'], ['class' => 'btn btn-success']) ?>
    </span>
    </h1>
    <?php 
    $estados = ArrayHelper::map(ListItem::find()->where(['list'=>'estado_pedido'])->asArray()->all(), 'code', 'value');
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'asunto',
        'cuerpo_texto',
        'cuerpo_html',
        'idioma',
        [
                'attribute' => 'estado',
                'value' => function($model)use($estados){ 
                    return $estados[$model->estado];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $estados,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado', 'id' => 'grid-pedido-search-estado']
            ],
        [
                'attribute' => 'estado_previo',
                'value' => function($model)use($estados){
                    return $estados[$model->estado_previo];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ListItem::find()->where(['list'=>'estado_pedido'])->asArray()->all(), 'code', 'value'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado previo', 'id' => 'grid-pedido-search-estado']
            ],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
     //   'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-formato-correo']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-envelope"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
