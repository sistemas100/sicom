<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoDistribuidorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-producto-distribuidor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'referencia')->textInput(['maxlength' => true, 'placeholder' => 'Referencia']) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true, 'placeholder' => 'Precio']) ?>

    <?php /* echo $form->field($model, 'marca')->textInput(['maxlength' => true, 'placeholder' => 'Marca']) */ ?>

    <?php /* echo $form->field($model, 'distribuidor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Distribuidor')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>
    <?php /* echo $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [ 
		       'type' => \kartik\datecontrol\DateControl::FORMAT_DATE, 
		       'saveFormat' => 'php:Y-m-d', 
		       'ajaxConversion' => true, 
		       'options' => [ 
		           'pluginOptions' => [ 
		               'placeholder' => Yii::t('app', 'Choose Fecha'), 
		               'autoclose' => true 
		           ],
                           'size' => 'sm'
		       ], 
		   ]); */ ?> 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
