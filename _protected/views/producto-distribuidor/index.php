<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoDistribuidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Producto Distribuidor');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="producto-distribuidor-index">

    <h1><?= Html::encode($this->title) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <span class="pull-right">
        <?php // Html::a(Yii::t('app', 'Create Producto Distribuidor'), ['create'], ['class' => 'btn btn-success']) ?>
          <?=
            Html::a('<i class="fa glyphicon glyphicon-export"></i> ' . Yii::t('app', 'Exportar todo'), ['producto-distribuidor/export'], ['class' => 'btn btn-primary',
                'title' => Yii::t('app', 'Exportar todo')])
            ?>
        <?php //echo Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </span>
    </h1>
    
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php ini_set('max_execution_time', 0);
    ini_set('memory_limit', '1000M');
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'referencia',
        'nombre',
        [
        'attribute' => 'precio', 
        'value' => function($model) {
                return '$' . number_format($model->precio,0);   
            },
        'hAlign'=>'right'
        ],
        'marca',
        [
            'attribute' => 'distribuidor_id',
            'label' => Yii::t('app', 'Distribuidor'),
            'value' => function($model){
                return $model->distribuidor->nombre;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->asArray()->all(), 'id', 'nombre'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Distribuidor', 'id' => 'grid-producto-distribuidor-search-distribuidor_id']
        ],
        'updated_at',
        'distribuidor.fecha',
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-producto-distribuidor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'showConfirmAlert' => false,
                'fontAwesome' => true,
                'stream' => false, // this will automatically save the file to a folder on web server
                'streamAfterSave' => true, // this will stream the file to browser after its saved on the web folder 
                'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                'batchSize' => 100,
  
                'dropdownOptions' => [
                    'label' => 'Todo',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Exportar todo lo filtrado</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
