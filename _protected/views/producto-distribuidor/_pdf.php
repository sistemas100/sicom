<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoDistribuidor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producto Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-distribuidor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Producto Distribuidor').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'referencia',
        'nombre',
        'precio',
        'marca',
        [
                'attribute' => 'distribuidor.nombre',
                'label' => Yii::t('app', 'Distribuidor')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
