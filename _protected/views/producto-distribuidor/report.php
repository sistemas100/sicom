<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoDistribuidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Producto Distribuidor');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
?>
<div class="producto-distribuidor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

  
    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        'codigo',
        'referencia',
        'nombre',
        'marca',
        [   
        //    'label' => ' Precio'.($->distribuidor->descuento?' - '.$model->distribuidor->descuento.'%':''),
            'attribute' => 'precio',
            'format' => 'raw',
            'value' => function($model) use($menores){
                $precio = $model->precio == 0?  number_format($model->precio2,0).'*':number_format($model->precio,0);
                $bold = in_array($model->id, $menores)?'<b>':'';
                return $bold .'$' . $precio;
            },
        ],
    ];
    $arrDistribuidores = array();
    foreach ($distribuidores as $distribuidor) {
        $arrDistribuidores[] = [
            'label' => $distribuidor->nombre.' '.$distribuidor->fecha,
            'format' => 'raw',
            'value' => function($model) use ($distribuidor,$menores) {
                $str = '';
                $str = '<table><tbody>';
                $arrDistInfo = array();
                foreach ($model->productosDistribuidor as $producto) {
                    if ($distribuidor->distribuidor_id == $producto->distribuidor_id 
                            && (empty($arrDistInfo) || (array_search($producto->referencia, $arrDistInfo) === FALSE ))){
                        $bold = in_array($producto->id, $menores)?'<b>':'';
                        $str .= '<tr><td>' . $bold . $producto->referencia . '</td></tr>';
                        $arrDistInfo[] = $producto->referencia;
                    }
                }
                $str .= '</tbody></table>';
                return $str;
            }
        ];
        $arrDistribuidores[] = [
            'label' => ' Precio'.($distribuidor->descuento?' - '.$distribuidor->descuento.'%':''),
            'format' => 'raw',
            'value' => function($model) use ($distribuidor,$menores) {
                $str = '';
                $str = '<table><tbody>';
                $arrDistInfo = array();
                foreach ($model->productosDistribuidor as $producto) {
                    if ($distribuidor->distribuidor_id == $producto->distribuidor_id 
                            && (empty($arrDistInfo) || (array_search($producto->referencia, $arrDistInfo) === FALSE ))){
                        $bold = in_array($producto->id, $menores)?'<b>':'';
                        $str .= '<tr><td>$' . $bold . number_format($producto->precio, 0) . '</td></tr>';
                        $arrDistInfo[] = $producto->referencia;
                    }
                }
                $str .= '</tbody></table>';
                return $str;
            }
        ];
        $arrDistribuidores[] = [
            'label' => ' marca',
            'format' => 'raw',
            'value' => function($model) use ($distribuidor,$menores) {
                $str = '';
                $str = '<table><tbody>';
                $arrDistInfo = array();
                foreach ($model->productosDistribuidor as $producto) {
                    if ($distribuidor->distribuidor_id == $producto->distribuidor_id 
                            && (empty($arrDistInfo) || (array_search($producto->referencia, $arrDistInfo) === FALSE ))){
                        $bold = in_array($producto->id, $menores)?'<b>':'';
                        $str .= '<tr><td>' . $bold . $producto->marca . '</td></tr>';
                        $arrDistInfo[] = $producto->referencia;
                    }
                }
                $str .= '</tbody></table>';
                return $str;
            }
        ];
    }
    $gridColumn = array_merge($gridColumn, $arrDistribuidores);


//        [
//                'attribute' => 'distribuidor_id',
//                'label' => Yii::t('app', 'Distribuidor'),
//                'value' => function($model){
//                    return $model->distribuidor->nombre;
//                },
//                'filterType' => GridView::FILTER_SELECT2,
//                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->asArray()->all(), 'id', 'nombre'),
//                'filterWidgetOptions' => [
//                    'pluginOptions' => ['allowClear' => true],
//                ],
//                'filterInputOptions' => ['placeholder' => 'Distribuidor', 'id' => 'grid-producto-distribuidor-search-distribuidor_id']
//            ],
    $gridColumn[] = ['class' => 'yii\grid\ActionColumn'];
//    ]; 
    ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'autoXlFormat' => true,
//        'floatHeader'=>true,
//        'floatHeaderOptions'=>['scrollingTop'=>'50'],
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false,
            'target' => GridView::TARGET_BLANK
        ],
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-producto-distribuidor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
            // your toolbar can include the additional full export menu
//        'toolbar' => [
//            '{export}',
//            ExportMenu::widget([
//                'dataProvider' => $dataProvider,
//                'columns' => $gridColumn,
//                'target' => ExportMenu::TARGET_BLANK,
//                'fontAwesome' => true,
//                'dropdownOptions' => [
//                    'label' => 'Full',
//                    'class' => 'btn btn-default',
//                    'itemsBefore' => [
//                        '<li class="dropdown-header">Export All Data</li>',
//                    ],
//                ],
//            ]),
//        ],
    ]);
    ?>
  <p>
        * Producto agotado
    </p>
</div>
