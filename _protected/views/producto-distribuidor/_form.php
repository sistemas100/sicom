<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoDistribuidor */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="producto-distribuidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'referencia')->textInput(['maxlength' => true, 'placeholder' => 'Referencia']) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true, 'placeholder' => 'Precio']) ?>
    
    <?= $form->field($model, 'marca')->textInput(['maxlength' => true, 'placeholder' => 'Marca']) ?>

    <?= $form->field($model, 'distribuidor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Distribuidor')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

     <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [ 
		       'type' => \kartik\datecontrol\DateControl::FORMAT_DATE, 
		       'saveFormat' => 'php:Y-m-d', 
		       'ajaxConversion' => true, 
		       'options' => [ 
		           'pluginOptions' => [ 
		               'placeholder' => Yii::t('app', 'Choose Fecha'), 
		               'autoclose' => true 
		           ],
                            'size' => 'sm'
		       ], 
		   ]); ?> 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
