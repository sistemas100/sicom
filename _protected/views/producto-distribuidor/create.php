<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductoDistribuidor */

$this->title = Yii::t('app', 'Create Producto Distribuidor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producto Distribuidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-distribuidor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
