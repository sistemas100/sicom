<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PedidoLog', 
        'relID' => 'pedido-log', 
        'value' => \yii\helpers\Json::encode($model->pedidoLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PedidoProducto', 
        'relID' => 'pedido-producto', 
        'value' => \yii\helpers\Json::encode($model->pedidoProductos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true, 'placeholder' => 'Codigo']) ?>

    <?= $form->field($model, 'proveedor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Proveedor')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'widgetOptions' => [ 
                            'pluginOptions' => [ 
                                'placeholder' => Yii::t('app', 'Choose Fecha'), 
                                'autoclose' => true,
                                'todayHighlight' => true,
		           ],
                           'size' => 'sm'
		       ],
    ]); ?>

    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true, 'placeholder' => 'Observacion']) ?>

    <?php
    $forms = [
//        [
//            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'PedidoLog')),
//            'content' => $this->render('_formPedidoLog', [
//                'row' => \yii\helpers\ArrayHelper::toArray($model->pedidoLogs),
//            ]),
//        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'PedidoProducto')),
            'content' => $this->render('_formPedidoProducto', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->pedidoProductos),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name'=>'save', 'value' => 'save']) ?>
        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name'=>'send', 'value' => 'send']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
