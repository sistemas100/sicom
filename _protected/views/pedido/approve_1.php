<div class="form-group" id="approve-pedido-producto">
<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;

$this->registerJs(
    "$('#accept').on('click', function() {
        var flag = true;
        $('.nueva-info').each(function() {
            if($(this).val()!== ''){
                alert('Existen campos con datos ingresados, corrija la información para continuar');
                flag = false;
                return false;
            }
        });
        if(flag){
            return confirm('Estás seguro que deseas aprobar la informacion del proveedor?');
        }else{
            return false;
        }
    });
    

    var table = $('#report').DataTable( {
        scrollY:        400,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   true,
        info: false
        } );",
    View::POS_READY
);
    
$this->registerJsFile(
    'https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js',
    ['depends' => [JqueryAsset::className()]]
);
$this->registerJsFile(
    'https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js',
    ['depends' => [JqueryAsset::className()]]
);


$proveedor_id = '';
$table = '';
$repetidos = [];
foreach ($models as $model){
    $arrPedidos = [];
//    foreach ($model->getLastLog($model->estado)->pedidoProductoLogs as $pedidoProducto) {
//         foreach ($bos as $bo) {
//            if($pedidoProducto->producto_id === $bo['producto_id']){
//                if ($bo['cantidadp']-$bo['cantidadd'] !== 0){
//                    $producto[$pedidoProducto->producto_id][$bo['pedido_id']] = $bo['cantidadp']-$bo['cantidadd'];
//                    $arrPedidos[] = $bo['pedido_id'];
//                }
//            }
//        }
//    }
//    $bos = array_unique($arrPedidos);
    
    $proveedor_id = $model->proveedor_id;
    $table .= "<h4>Pedido: $model->codigo_pedido </h4>".'<table id="report" class="bg-white table-striped table-condensed table-hover table-bordered stripe row-border order-column" >
      <thead>
        <tr>
          <th rowspan="2">Codigo</th>
          <th rowspan="2">Referencia</th>
          <th rowspan="2">Nombre</th>
          <th rowspan="2">Precio1</th>
          <th rowspan="2">Precio2</th>
          <th rowspan="2">Precio4</th>
          <th rowspan="2">Saldo</th>
          <th rowspan="2">Cant P</th>';
//    foreach ($bos as $bo) {
//        $table .= '<th rowspan="2">' . $bo . '</th>';
//    }

    $count = 0;
    foreach ($model->pedidoLogs as $pedidoLog) {
        switch ($pedidoLog->estado) {
            case 1:
                $table .= '<th colspan="4"><nobr>&nbsp;Cotizaci&oacute;n</nobr></th>';
                $count++;
                break;
            case 2:
                $table .= '<th colspan="4"><nobr>&nbsp;Desapacho</nobr></th>';
                $count++;
                break;
            case 3:
                $table .= '<th colspan="4"><nobr>&nbsp;Factura</nobr></th>';
                $count++;
                break;
            default:
                break;
        }
    }
    $table .= '</tr><tr>';
    for ($index = 0;$index < $count;$index++) {
        $table.='<th>cant</th><th>precio</th><th>cant</th><th>observacion</th>';
    }
    $table .= '</tr>
    </thead>
      <tbody>';
    foreach ($model->getLastLog($model->estado)->pedidoProductoLogs as $pedidoProducto) {
        $repetidos[$pedidoProducto->producto->codigo][]= $pedidoProducto->pedidoLog->pedido_id;
        $table .= '<tr>
          <th scope="row">'.$pedidoProducto->producto->codigo.'</th>
          <td>'.$pedidoProducto->producto->referencia.'</td>
          <td>'.$pedidoProducto->producto->nombre.'</td>
          <td>'.$pedidoProducto->producto->precio1.'</td>
          <td>'.$pedidoProducto->producto->precio2.'</td>
          <td>'.$pedidoProducto->producto->precio4.'</td>
          <td>'.$pedidoProducto->producto->existencia.'</td>
          <td>'.$pedidoProducto->cantidad.'</td>';
          //backorders
//        foreach ($bos as $bo) {
//            $table .= '<td>' . (isset($producto[$pedidoProducto->producto_id][$bo]) ? $producto[$pedidoProducto->producto_id][$bo] : 0) . '</td>';
//        }
        $countLogs = count($model->pedidoLogs);
        foreach ($model->pedidoLogs as $pedidoLog) {
            $flag = false;
            $countProd = count($pedidoLog->pedidoProductoLogs);
            foreach ($pedidoLog->pedidoProductoLogs as $pedidoProductoLog) {
                if($pedidoProductoLog->producto_id == $pedidoProducto->producto_id){
                    $table .= '<td>'.$pedidoProductoLog->cantidad.'</td>
                        <td>'.$pedidoProductoLog->precio.'</td>
                        <td>'.(($countLogs === 1 && $countProd >0)?
                            Html::input('text', "$model->id[PedidoProductoLog][$pedidoProducto->producto_id][nueva_cantidad]", $pedidoProductoLog->nueva_cantidad,['class'=>'nueva-info']):
                            $pedidoProductoLog->nueva_cantidad).'</td>
                        <td>'.(($countLogs === 1 && $countProd >0)?
                            Html::input('text', "$model->id[PedidoProductoLog][$pedidoProducto->producto_id][observacion]", $pedidoProductoLog->observacion,['class'=>'nueva-info']):
                            $pedidoProductoLog->observacion).
                            Html::hiddenInput("$model->id[PedidoProductoLog][$pedidoProducto->producto_id][pedido_log_id]", $pedidoProductoLog->pedido_log_id ).
                            Html::hiddenInput("$model->id[PedidoProductoLog][$pedidoProducto->producto_id][precio]", $pedidoProductoLog->precio ).
                            Html::hiddenInput("$model->id[PedidoProductoLog][$pedidoProducto->producto_id][cantidad]", $pedidoProductoLog->cantidad ).'</td>';
                    $flag = true;
                }
            }
            if(!$flag){
                $table .= '<td>0</td>
                        <td>0</td>
                        <td></td>
                        <td></td>';
            }
            $countLogs--;
        }
//        if($model->pedidoLogs){
//            $table .= '<td>'.Html::checkbox("$model->id[PedidoProductoLog][$pedidoProducto->producto_id][estado]",$pedidoProducto->estado).'</td>';
//        }
    }
    $table .='</tr>
      </tbody>
    </table>';
}

$flag = false;
$temp = '<h4>Productos repetidos</h4>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Codigo</th>
          <th>Pedidos</th>
        </tr>
        <tr>
      </thead>
        <tbody>';
foreach ($repetidos as $key=>$repetido) {
    if(count($repetido) > 1){
        $flag = true;
        $strPedidos = '';
        foreach ($repetido as $value){
             $strPedidos .= $value.',';       
        }
        $strPedidos = substr($strPedidos, 0, -1);
        $temp .= '<tr><th scope="row">'.$key.'</th>
                    <td>'.$strPedidos.'</td></tr>';
    }
}
$temp .='</tbody>
    </table>';

$table .= $flag?$temp:'';


echo  Html::beginForm();
Html::hiddenInput('proveedor_id', $proveedor_id);
echo '<div class="table-responsive" style="height: 85vh;">' .$table . '</div>';?>
    <div class="form-group pull-right">
<?php 
echo Html::submitButton(Yii::t('app', 'Accept'), ['class' => 'btn btn-primary','name'=>'accept', 'value' => 'accept', 'id' => 'accept']);
echo ' '.Html::submitButton(Yii::t('app', 'Reject'), ['class' => 'btn btn-primary','name'=>'reject', 'value' => 'reject','data' => ['confirm' => Yii::t('app', 'Estás seguro que deseas enviar los cambios ingresados al proveedor?')]]);
echo ' '.Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']);
?>
    </div>
<?php echo Html::endForm();?>