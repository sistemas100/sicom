<?php
//echo "entró acá";
//die;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;

$this->registerJs(
        "$('#report').tableHeadFixer({'left' : 1});
        $('.precios').tooltip({html:true});
        
        var btnDLtoExcel = $('#btn_exportar');
           btnDLtoExcel.on('click', function () {
               $('#report').excelexportjs({
                   containerid: 'report'
                   , datatype: 'table'
               });
                return false;
           });", View::POS_READY
);

$this->registerJsFile(
        '@web/js/tableHeadFixer.js', ['depends' => [JqueryAsset::className()]]
);
$this->registerJsFile(
        '@web/js/excelexportjs.js', ['depends' => [JqueryAsset::className()]]
);

$thead = '<table id="report" class="bg-white table-striped table-condensed table-hover table-bordered"  >
  <thead>
    <tr>
        <th style= "text-align:center" rowspan="2">Codigo</th>
        <th style= "text-align:center" rowspan="2">Referencia</th>
        <th style= "text-align:center" rowspan="2">Ref proveedor</th>
        <th style= "text-align:center" rowspan="2">Nombre</th>';
//$tr = '<tr>';
foreach ($fechas as $fecha) {
    $thead .= '<th style= "text-align:center" rowspan="2"><nobr>' . $fecha . '</nobr></th>';
 //    $thead .= '<th style= "text-align:center" colspan="2"><nobr>' . $fecha . '</nobr></th>';
//    $tr .= '<th style= "text-align:center" >P</th><th style= "text-align:center" >V</th>';
}
$thead .= '<th style= "text-align:center" rowspan="2">Pr pedido</th>
      <th style= "text-align:center" rowspan="2">Pr ventas</th>
      <th style= "text-align:center" rowspan="2">Pr visual</th>
      <th style= "text-align:center" rowspan="2">Saldo</th>';

$thead .= '<th style= "text-align:center" rowspan="2">UFCOMPRA</th>
        <th style= "text-align:center" rowspan="2">UFVTA</th>
        <th style= "text-align:center" rowspan="2">UPCOMPRA</th>
        ';


foreach ($bos as $bo) {
    $thead .= '<th style= "text-align:center" rowspan="2">' . $bo . '</th>';
}
$thead .= '
      
      <th style= "text-align:center" rowspan="2">FAC</th>
      <th style= "text-align:center" rowspan="2">Precio1</th>
      <th style= "text-align:center" rowspan="2">Precio2</th>
      <th style= "text-align:center" rowspan="2">Precio4</th>
      <th style= "text-align:center" rowspan="2">Competencia</th>
      <th style= "text-align:center" rowspan="2">Estado</th>
      ' .
        ($model->isNewRecord || $model->estado == 0 ?
        '<th style= "text-align:center" rowspan="2">Repetido</th>
      <th style= "text-align:center" rowspan="2">Relacionado</th>
      <th style= "text-align:center" rowspan="2">Meses</th>
      <th style= "text-align:center" rowspan="2">Cant P</th>
      <th style= "text-align:center" rowspan="2"></th>' :
        '<th style= "text-align:center" rowspan="2">Cant P</th>');
$count = 0;
$countPLogs = count($model->pedidoLogs);
foreach ($model->pedidoLogs as $pedidoLog) {
    switch ($pedidoLog->estado) {
        case 0:
            //  $count++;
            break;
        case 1:
            $count++;
            $thead .= '<th style= "text-align:center" colspan="4"><nobr>' . ($count === ($countPLogs) ?
                    Html::a('<i class="glyphicon glyphicon-import"></i>', ['import', 'id' => $pedidoLog->id]) : '') . '&nbsp;Cotizaci&oacute;n</nobr></th>';
            break;
        case 2:
            $thead .= '<th style= "text-align:center" colspan="4">Despacho</th>';
            $count++;
            break;
        case 3:
            $thead .= '<th style= "text-align:center" colspan="4">Factura</th>';
            $count++;
            break;
        default:
            break;
    }
}

//$thead .= $tr;
for ($index = 0; $index < $count; $index++) {
    $thead .= '<th>cant</th><th>precio</th><th>cant</th><th>obs</th>';
}
$thead .= '</tr></thead>';
$table = '<tbody>';
$bottom = '';
$total_precio4 = 0;
$totales_precio = [];
foreach ($productos as $producto) {
    $producto_id = $producto['producto_id'];
    $countLogs = $countPLogs;
    $total_precio4 += $producto['precio4'];
    $temp = '<tr>
      <th scope="row">' . Html::a($producto['codigo'], ['producto/update', 'id' => $producto['producto_id']], ['target' => '_blank']) . ($model->isNewRecord || $model->estado == 0 ? Html::hiddenInput("PedidoProducto[" . $producto_id . "][producto_id]", $producto_id) : '') . '</th>
      <td style= "text-align:left">' . $producto['referencia'] . '</td>
      <td style= "text-align:left">' . $producto['referencia_proveedor'] . '</td>
      <td>' . $producto['nombre'] . '</td>';
    $sum_pedidos = 0;
    $sum_ventas = 0;
    $fac = ($producto['upcompra'] != 0) ? $producto['precio1'] != 0 ? (round(($producto['precio1'] / $producto['upcompra']), 2)) : (round(($producto['precio2'] / $producto['upcompra']), 2)) : '0';
    //ventas y pedidos por mes
    foreach ($fechas as $i => $fecha) {
        $pedidos = $producto[$fecha]['pedidos'] ? $producto[$fecha]['pedidos'] : 0;
        $ventas = $producto[$fecha]['ventas'] ? $producto[$fecha]['ventas'] : 0;
    //    $temp .= '<td style= "text-align:center">' . $pedidos . '</td>';
        $temp .= '<td style= "text-align:center">' . $ventas . '</td>';
        if ($i < 12) {
            $sum_ventas += $ventas;
            $sum_pedidos += $pedidos;
        }
    }


    $temp .= '<td style= "text-align:right">' . round(($sum_pedidos / 12), 1) . '</td>';
    $temp .= '<td style= "text-align:right">' . round(($sum_ventas / 12), 1) . '</td>
      <td>' . ($model->estado > 0 ? $producto['promedio_manual'] . '</td>' : Html::input('text', "PedidoProducto[" . $producto_id . "][promedio_manual]", $producto["promedio_manual"], ['id' => $producto_id, 'class' => 'promedio', 'style' => 'width:50px'])) . '</td>
    <td style= "text-align:right" id = existencia-' . $producto_id . '>' . $producto['existencia'] . '</td>
      <td>' . $producto['ufcompra'] . '</td>
      <td>' . $producto['ufvta'] . '</td>
      <td style= "text-align:right">' . $producto['upcompra'] . '</td>';


    //backorders
    $totalBo = 0;
    foreach ($bos as $bo) {
        $vbo = isset($producto[$bo]) ? $producto[$bo] : 0;
        $totalBo += $vbo;
        $temp .= '<td style= "text-align:right">' . $vbo . '</td>';
    }
    $cantidad = 0;
    $estado = 0;
    if ($model->isNewRecord || $model->estado === 0) {
        //obtenemos cantidad sugerida
        echo Html::hiddenInput("", $totalBo, ['id' => 'totalbo-' . $producto_id]) . '</td>';
        $promedio_ventas = $producto["promedio_manual"] ? $producto["promedio_manual"] : round(($sum_ventas / 12));
        $dias_inventario = $promedio_ventas != 0 ? (($producto['existencia'] + $totalBo) / $promedio_ventas) * 30 : 0;
        $dias_entrega = $producto['tiempo_produccion'] + $producto['tiempo_transporte'];
        echo Html::hiddenInput("", $dias_entrega, ['id' => 'dias-entrega-' . $producto_id]) . '</td>';
        $frecuencia_pedido = $producto['frecuencia_pedido'] ? $producto['frecuencia_pedido'] : 0;
        echo Html::hiddenInput("", $frecuencia_pedido, ['id' => 'frecuencia-pedido-' . $producto_id]) . '</td>';
        $factor_pedido = $producto['factor_pedido'] ? $producto['factor_pedido'] : 1;
        echo Html::hiddenInput("", $factor_pedido, ['id' => 'factor-pedido-' . $producto_id]) . '</td>';
    }
    if ($model->isNewRecord) {
        if ($dias_entrega >= $dias_inventario) {
            $cantidad = $factor_pedido * $promedio_ventas;
            $estado = $cantidad;
        } elseif ($dias_entrega + $frecuencia_pedido >= $dias_inventario) {
            $cantidad = (($factor_pedido * $promedio_ventas) / $frecuencia_pedido) * ($dias_entrega + $frecuencia_pedido - $dias_inventario);
            $estado = $cantidad;
        }
        $cantidad = round($cantidad);
        $cantidad = $cantidad ? $cantidad + 5 - ($cantidad % 5) : 0;
    } else {
        $cantidad = $producto['cantidadP'];
        $estado = $producto['estado'];
    }
    $temp .= '<td style= "text-align:right"' . ($fac > 1.5 ? '>' : ' class = "bg-pink">') . $fac . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio1'], 0) . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio2'], 0) . '</td>
      <td style= "text-align:right">' . $producto['precio4'] . '</td>
      <td style= "text-align:right" class = "precios" title="' . $precios[$producto['producto_id']]['todos'] . '">' . $precios[$producto['producto_id']]['menor'] . '</td>
      <td>' . ($model->estado > 0 ? ($producto['estado_producto'] ? 'Activo' : 'Inactivo') : (Html::dropDownList("PedidoProducto[" . $producto_id . "][estado_producto]", $producto['estado_producto'], [0 => 'Inactivo', 1 => 'Activo']))) . '</td>    
      ';
    if ($model->isNewRecord || $model->estado == 0) {
        $temp .= '<td>' . $producto['repetido'] . '</td>
      <td>' . $producto['relacionado'] . '</td>
      <td style= "text-align:right" id = meses-' . $producto_id . '>' . round(($dias_inventario / 30), 1) . '</td>
      <td>' . Html::input('text', "PedidoProducto[" . $producto_id . "][cantidad]", round($cantidad), ['id' => 'cantidad-' . $producto_id, 'style' => 'width:50px']) . '</td>
      <td>' . Html::checkbox("PedidoProducto[" . $producto_id . "][estado]", $estado) . '</td>';
    } else {
        $temp .= '<td' . ( $producto['cantidadP'] ? '>' : ' class = "bg-yellow">') . $producto['cantidadP'] . '</td>';
    }

    foreach ($model->pedidoLogs as $pedidoLog) {
        if ($pedidoLog->estado > 0 && $pedidoLog->estado < 4) {
            $flag = false;
            $totales_precio[$pedidoLog->id] = 0;
            $countProd = count($pedidoLog->pedidoProductoLogs);
            foreach ($pedidoLog->pedidoProductoLogs as $pedidoProductoLog) {
                if ($pedidoProductoLog->producto_id == $producto_id && $pedidoLog->estado) {
                    $totales_precio[$pedidoLog->id] += $pedidoProductoLog->precio;
                    $temp .= '<td' . ($pedidoProductoLog->cantidad && $pedidoProductoLog->cantidad == $producto['cantidadP'] ? '>' : ' class = "bg-red">') . $pedidoProductoLog->cantidad . '</td>
                        <td' . ($producto['precio4'] != $pedidoProductoLog->precio && $countProd > 0 && $pedidoLog->estado == 1 ? ' class = "bg-blue">' : '>') . $pedidoProductoLog->precio . '</td>';
                    if (in_array($model->estado, [2, 3])) {
                        if ($countLogs === 1 && $countProd > 0 && Yii::$app->controller->action->id == 'approve') {
                            $temp .= ' <td>' . Html::input('text', "$model->id[PedidoProductoLog][$producto_id][nueva_cantidad]", $pedidoProductoLog->nueva_cantidad, ['class' => 'nueva-info']) . '</td> '
                                    . '<td>' . Html::input('text', "$model->id[PedidoProductoLog][$producto_id][observacion]", $pedidoProductoLog->observacion, ['class' => 'nueva-info']) .
                                    Html::hiddenInput("$model->id[PedidoProductoLog][$producto_id][pedido_log_id]", $pedidoProductoLog->pedido_log_id) .
                                    Html::hiddenInput("$model->id[PedidoProductoLog][$producto_id][precio]", $pedidoProductoLog->precio) .
                                    Html::hiddenInput("$model->id[PedidoProductoLog][$producto_id][cantidad]", $pedidoProductoLog->cantidad) . '</td>';
                        } else {
                            $temp .= '<td>' . $pedidoProductoLog->nueva_cantidad . '</td> <td>' . $pedidoProductoLog->observacion . '</td>';
                        }
                    } else {

                        $temp .= '<td>' . (($countLogs === 1 && $countProd > 0 ) ?
                                Html::input('text', 'PedidoProductoLog[' . $producto_id . '][nueva_cantidad]', $pedidoProductoLog->nueva_cantidad, ['class' => 'nueva-info']) :
                                $pedidoProductoLog->nueva_cantidad) . '</td>
                        <td>' . (($countLogs === 1 && $countProd > 0 ) ?
                                Html::input('text', "PedidoProductoLog[" . $producto_id . "][observacion]", $pedidoProductoLog->observacion, ['class' => 'nueva-info']) :
                                $pedidoProductoLog->observacion) .
                                Html::hiddenInput("PedidoProductoLog[" . $producto_id . "][pedido_log_id]", $pedidoProductoLog->pedido_log_id) . '</td>';
                    }
                    $flag = true;
                }
            }
            if (!$flag) {
                $temp .= '<td class = "bg-red">0</td>
                        <td>0</td>
                        <td></td>
                        <td></td>';
            }
            $countLogs--;
        }
    }
    $temp .= '</tr>';
    $table .= $temp;
}
$table .= $bottom . '</tbody>
</table>';
$footer = '<tfoot>
    <tr>
      <th colspan="41">Totales</th>
      <th>' . $total_precio4 . '</th>
      <th colspan="3"></th>';
foreach ($totales_precio as $value) {
    $footer .= '<th>' . $value . '</th>
                <th colspan="4"></th>';
}
$footer .= '</tr>
        </tfoot>';
echo '<div class="report">' . $thead . $table . '</div>';
?>