<div class="form-group" id="approve-pedido-producto">
<?php

use app\models\Proveedor;
use yii\helpers\Html;
use yii\web\View;

$this->registerJs(
    "$('#accept').on('click', function() {
        var flag = true;
        $('.nueva-info').each(function() {
            if($(this).val()!== ''){
                alert('Existen campos con datos ingresados, corrija la información para continuar');
                flag = false;
                return false;
            }
        });
        if(flag){
            return confirm('Estás seguro que deseas aprobar la informacion del proveedor?');
        }else{
            return false;
        }
    });",
    View::POS_READY
);
    
echo  Html::beginForm();

$proveedor_id = '';
$repetidos = [];
foreach ($models as $model){
    $arrPedidos = [];
    $proveedor_id = $model->proveedor_id;
    echo "<h4>Pedido: $model->codigo_pedido </h4>";
    $datos = Proveedor::getInfoProductos($model);
//    echo "<pre>";
//    print_R($datos);
//    echo "</pre>";
//    die;
    echo $this->render('_formPedidoProducto',$datos );
}
//echo 4; die;


$flag = false;
$temp = '<h4>Productos repetidos</h4>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Codigo</th>
          <th>Pedidos</th>
        </tr>
        <tr>
      </thead>
        <tbody>';
foreach ($repetidos as $key=>$repetido) {
    if(count($repetido) > 1){
        $flag = true;
        $strPedidos = '';
        foreach ($repetido as $value){
             $strPedidos .= $value.',';       
        }
        $strPedidos = substr($strPedidos, 0, -1);
        $temp .= '<tr><th scope="row">'.$key.'</th>
                    <td>'.$strPedidos.'</td></tr>';
    }
}
$temp .='</tbody>
    </table>';

Html::hiddenInput('proveedor_id', $proveedor_id);
$table = '';
$table .= $flag?$temp:'';

echo $table; ?>
    <br>
    <div>
<?php 
echo Html::a(Yii::t('app', 'Atras'), ['proveedor/purchases', 'id' => $model->proveedor_id], ['class' => 'btn btn-danger']);
echo ' '.Html::submitButton(Yii::t('app', 'Accept'), ['class' => 'btn btn-primary','name'=>'accept', 'value' => 'accept', 'id' => 'accept']);
echo ' '.Html::submitButton(Yii::t('app', 'Reject'), ['class' => 'btn btn-primary','name'=>'reject', 'value' => 'reject','data' => ['confirm' => Yii::t('app', 'Estás seguro que deseas enviar los cambios ingresados al proveedor?')]]);
?>
    </div>
<?php echo Html::endForm();?>