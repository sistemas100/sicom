<?php

$thead = '<table id="report" class="bg-white table-striped table-condensed table-hover table-bordered" >
  <thead>
    <tr>
        <th style= "text-align:center">Codigo</th>
        <th style= "text-align:center">Marca</th>
        <th style= "text-align:center">Referencia</th>
        <th style= "text-align:center">Ref proveedor</th>
        <th style= "text-align:center">Nombre</th>';
//$tr = '<tr>';
foreach ($fechas as $fecha) {
    $thead .= '<th style= "text-align:center"><nobr>' . $fecha . '</nobr></th>';
//    $thead .= '<th style= "text-align:center" colspan="2"><nobr>' . $fecha . '</nobr></th>';
//    $tr .= '<th style= "text-align:center" >P</th><th style= "text-align:center" >V</th>';
}
$thead .= '<th style= "text-align:center">Pr pedido</th>
      <th style= "text-align:center">Pr ventas</th>
      <th style= "text-align:center">Pr visual</th>
      <th style= "text-align:center">Meses</th>
      <th style= "text-align:center">Saldo</th>';

$thead .= '<th style= "text-align:center">UFCOMPRA</th>
        <th style= "text-align:center">UFVTA</th>
        <th style= "text-align:center">UPCOMPRA</th>
        ';


foreach ($bos as $bo) {
    $thead .= '<th style= "text-align:center">' . $bo . '</th>';
}
$thead .= '
      
      <th style= "text-align:center">FAC</th>
      <th style= "text-align:center">Precio1</th>
      <th style= "text-align:center">Precio2</th>
      <th style= "text-align:center">Precio4</th>
      <th style= "text-align:center">Competencia</th>
      <th style= "text-align:center">Estado</th>
      <th style= "text-align:center">Repetido</th>
      <th style= "text-align:center">Relacionado</th>
      <th style= "text-align:center">Cant P</th>';
$count = 0;
$countPLogs = count($model->pedidoLogs);
foreach ($model->pedidoLogs as $pedidoLog) {
    switch ($pedidoLog->estado) {
        case 0:
            //  $count++;
            break;
        case 1:
            $count++;
            $thead .= '<th style= "text-align:center" colspan="4">Cotizaci&oacute;n</th>';
            break;
        case 2:
            $thead .= '<th style= "text-align:center" colspan="4">Despacho</th>';
            $count++;
            break;
        case 3:
            $thead .= '<th style= "text-align:center" colspan="4">Factura</th>';
            $count++;
            break;
        default:
            break;
    }
}

//$thead .= $tr;
for ($index = 0; $index < $count; $index++) {
    $thead .= '<th>cant</th><th>precio</th><th>cant</th><th>obs</th>';
}
$thead .= '</tr></thead>';
$table = '<tbody>';
$bottom = '';
$total_precio4 = 0;
$totales_precio = [];
$prdRela = [];
foreach ($productos as $producto) {
    $producto_id = $producto['producto_id'];
    $countLogs = $countPLogs;
    $total_precio4 += $producto['precio4'];
    $temp = '<tr>
      <th scope="row">' . Html::a($producto['codigo'], ['producto/update', 'id' => $producto['producto_id']], ['target' => '_blank']) . '</th>
      <td style= "text-align:left">' . $producto['nombre_proveedor'] . '</td>
      <td style= "text-align:left">' . $producto['referencia'] . '</td>
      <td style= "text-align:left">' . $producto['referencia_proveedor'] . '</td>
      <td>' . $producto['nombre'] . '</td>';
    $sum_pedidos = 0;
    $sum_ventas = 0;
    $fac = ($producto['upcompra'] != 0) ? $producto['precio1'] != 0 ? (round(($producto['precio1'] / $producto['upcompra']), 2)) : (round(($producto['precio2'] / $producto['upcompra']), 2)) : '0';
    //ventas y pedidos por mes
    foreach ($fechas as $i => $fecha) {
        $pedidos = $producto[$fecha]['pedidos'] ? $producto[$fecha]['pedidos'] : 0;
        $ventas = $producto[$fecha]['ventas'] ? $producto[$fecha]['ventas'] : 0;
//        $temp .= '<td style= "text-align:center">' . $pedidos . '</td>';
        $temp .= '<td style= "text-align:center">' . $ventas . '</td>';
        if ($i < 12) {
            $sum_ventas += $ventas;
            $sum_pedidos += $pedidos;
        }
    }
    //backorders
    $totalBo = 0;
    $tempBo = '';
    foreach ($bos as $bo) {
        $vbo = isset($producto[$bo]) ? $producto[$bo] : 0;
        $totalBo += $vbo;
        $tempBo .= '<td style= "text-align:right">' . $vbo . '</td>';
    }
    $promedio_ventas = $producto["promedio_manual"] ? $producto["promedio_manual"] : round(($sum_ventas / 12));
    $dias_inventario = $promedio_ventas != 0 ? (($producto['existencia'] + $totalBo) / $promedio_ventas) * 30 : 0;

    $pRl = explode(",", $producto['relacionado']);
    $prdRela[@$pRl[0]] = @$pRl[0];
    $prRR = "'" . @$pRl[0] . "',";

    foreach ($pRl as $prRRWER) {
        $prRR .= "'" . $prRRWER . "',";
        $prdRela[$prRRWER] = $prRRWER;
    }
    $prRR = substr($prRR, 0, -1);

    $sql = "SELECT DISTINCT producto.id producto_id,
                    codigo,
                    producto.estado estado_producto
                    FROM producto
                    WHERE 1 = 1 AND codigo IS NOT NULL AND codigo IN ($prRR) ORDER BY `codigo` DESC";

    $infoProductos3 = Yii::$app->db->createCommand($sql)->queryAll();

    $relacionadOMostrar = $producto['relacionado'];


    $temp .= '<td style= "text-align:right">' . round(($sum_pedidos / 12), 1) . '</td>';
    $temp .= '<td style= "text-align:right">' . round(($sum_ventas / 12), 1) . '</td>
      <td style= "text-align:right">' . $producto['promedio_manual'] . '</td>
      <td style= "text-align:right" id = meses-' . $producto_id . '>' . round(($dias_inventario / 30), 1) . '</td>
      <td style= "text-align:right" id = existencia-' . $producto_id . '>' . $producto['existencia'] . '</td>
      <td>' . gmdate("Y-m-d", ($producto['ufcompra'] - 25569) * 86400) . '</td>
      <td>' . gmdate("Y-m-d", ($producto['ufvta'] - 25569) * 86400) . '</td>
      <td style= "text-align:right">' . $producto['upcompra'] . '</td>'
            . $tempBo
            . '<td style= "text-align:right"' . ($fac > 1.5 ? '>' : ' class = "bg-pink">') . $fac . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio1'], 0) . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio2'], 0) . '</td>
      <td style= "text-align:right">' . $producto['precio4'] . '</td>
      <td style= "text-align:right" class = "precios" title="' . $precios[$producto['producto_id']]['todos'] . '">' . $precios[$producto['producto_id']]['menor'] . '</td>
      <td>' . ( $producto['estado_producto'] ? 'Activo' : 'Inactivo' ) . '</td>
      <td>' . $producto['repetido'] . '</td>
      <td>' . $relacionadOMostrar . '</td>
      <td style= "text-align:right"' . ( $producto['cantidadP'] ? '>' : ' class = "bg-yellow">') . $producto['cantidadP'] . '</td>';
    foreach ($model->pedidoLogs as $pedidoLog) {
        if ($pedidoLog->estado > 1 && $pedidoLog->estado <= 4) {
            $flag = false;
            $totales_precio[$pedidoLog->id] = 0;
            $countProd = count($pedidoLog->pedidoProductoLogs);
            foreach ($pedidoLog->pedidoProductoLogs as $pedidoProductoLog) {
                if ($pedidoProductoLog->producto_id == $producto_id && $pedidoLog->estado) {
                    $totales_precio[$pedidoLog->id] += $pedidoProductoLog->precio;
                    $temp .= '<td style= "text-align:right"' . ($pedidoProductoLog->cantidad && $pedidoProductoLog->cantidad == $producto['cantidadP'] ? '>' : ' class = "bg-red">');
                    $temp .= $pedidoProductoLog->cantidad . '</td>';
                    $temp .= '<td style= "text-align:right"' . ($producto['precio4'] != $pedidoProductoLog->precio && $countProd > 0 && $pedidoLog->estado == 1 ? ' class = "bg-blue">' : '>');
                    $temp .= $pedidoProductoLog->precio . '</td>';
                    $temp .= '<td style= "text-align:right">' . $pedidoProductoLog->nueva_cantidad . '</td>';
                    $temp .= '<td>' . $pedidoProductoLog->observacion . '</td>';
                }
            }
            $countLogs--;
        }
    }
    $temp .= '</tr>';
    $table .= $temp;
}
$table .= $bottom . '</tbody>
</table>';
$footer = '<tfoot>
    <tr>
      <th colspan="41">Totales</th>
      <th>' . $total_precio4 . '</th>
      <th colspan="3"></th>';
foreach ($totales_precio as $value) {
    $footer .= '<th>' . $value . '</th>
                <th colspan="4"></th>';
}
$footer .= '</tr>
        </tfoot>';
echo '<div class="report">' . $thead . $table . '</div>';
?>