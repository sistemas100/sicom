<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Pedido').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('app', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('app', 'Will open the generated PDF file in a new window')
                ]
            )?>
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo_pedido',
        'codigo',
        [
            'attribute' => 'proveedor.id',
            'label' => Yii::t('app', 'Proveedor'),
        ],
        'fecha',
        'estado',
        'observacion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerPedidoLog->totalCount){
    $gridColumnPedidoLog = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'estado',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPedidoLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Pedido Log')),
        ],
        'columns' => $gridColumnPedidoLog
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPedidoProducto->totalCount){
    $gridColumnPedidoProducto = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'producto.id',
                'label' => Yii::t('app', 'Producto')
            ],
                        'cantidad',
            'cantidad_disponible',
            'precio_und',
            'observacion',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPedidoProducto,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido-producto']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Pedido Producto')),
        ],
        'columns' => $gridColumnPedidoProducto
    ]);
}
?>
    </div>
</div>
