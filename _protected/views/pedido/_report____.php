<div class="form-group" id="add-pedido-producto">
    <?php

    use yii\helpers\Html;

$thead = '<table id="report" class="bg-white table-striped table-condensed table-hover table-bordered stripe row-border order-column" >
  <thead>
    <tr>
      <th rowspan="2">Codigo</th>
      <th rowspan="2">Referencia</th>
      <th rowspan="2">Nombre</th>
      <th rowspan="2">Ref proveedor</th>
      <th rowspan="2">CTOPRM</th>
      <th rowspan="2">UPCOMPRA</th>
      <th rowspan="2">UFCOMPRA</th>
      <th rowspan="2">UFVTA</th>';
    $tr = '<tr>';
    foreach ($fechas as $fecha) {
        $thead .= '<th colspan="2"><nobr>' . $fecha . '</nobr></th>';
        $tr .= '<th>P</th><th>V</th>';
    }
    foreach ($bos as $bo) {
        $thead .= '<th rowspan="2">' . $bo . '</th>';
    }
    $thead .= '<th rowspan="2">Pr pedido</th>
      <th rowspan="2">Pr ventas</th>
      <th rowspan="2">Pr visual</th>
      <th rowspan="2">FAC</th>
      <th rowspan="2">Precio1</th>
      <th rowspan="2">Precio2</th>
      <th rowspan="2">Precio4</th>
      <th rowspan="2">Saldo</th>'.
    ($model->isNewRecord || $model->estado == 0?
      '<th rowspan="2">Repetido</th>
      <th rowspan="2">Meses</th>
      <th rowspan="2">Cant P</th>
      <th rowspan="2"></th>':
    '<th rowspan="2">Cant P</th>');
    $count = 0;
    $countPLogs = count($model->pedidoLogs);
    foreach ($model->pedidoLogs as $pedidoLog) {
        switch ($pedidoLog->estado) {
            case 0:
                $count++;
                break;
            case 1:
                $count++;
                $thead .= '<th colspan="4"><nobr>' . ($count === ($countPLogs) ?
                        Html::a('<i class="glyphicon glyphicon-import"></i>', ['import', 'id' => $pedidoLog->id]) : '') . '&nbsp;Cotizaci&oacute;n</nobr></th>';
                break;
            case 2:
                $thead .= '<th colspan="4"><nobr>&nbsp;Despacho</nobr></th>';
                $count++;
                break;
            case 3:
                $thead .= '<th colspan="4"><nobr>&nbsp;Factura</nobr></th>';
                $count++;
                break;
            default:
                break;
        }
    }
    if ($model->pedidoLogs && $model->estado == 1) {
        $thead .= '<th rowspan="2"></th>';
    }
    $thead .= $tr;
    for ($index = 0; $index < $count; $index++) {
        $thead .= '<th>cant</th><th>precio</th><th>cant</th><th>obs</th>';
    }
    $thead .= '</tr></thead>';
    $table = '<tbody>';
    $bottom = '';
    $total_precio4 = 0;
    $totales_precio = [];
    foreach ($productos as $producto) {
        $total_precio4 += $producto['precio4'];
        $temp = '<tr>
      <th scope="row">' . $producto['codigo'] . Html::hiddenInput("PedidoProducto[" . $producto['producto_id'] . "][producto_id]", $producto['producto_id']) . '</th>
      <td>' . $producto['referencia'] . '</td>
      <td>' . $producto['nombre'] . '</td>
      <td>' . $producto['referencia_proveedor'] . '</td>
      <td>' . $producto['ctoprm'] . '</td>
      <td>' . $producto['upcompra'] . '</td>
      <td>' . $producto['ufcompra'] . '</td>
      <td>' . $producto['ufvta'] . '</td>';
        $sum_pedidos = 0;
        $sum_ventas = 0;
        $fac = ($producto['upcompra'] != 0) ? $producto['precio1'] != 0 ?(round(($producto['precio1'] / $producto['upcompra']), 2)) :(round(($producto['precio2'] / $producto['upcompra']), 2)): '0';
        //ventas y pedidos por mes
        foreach ($fechas as $i => $fecha) {
            $pedidos = $producto[$fecha]['pedidos'] ? $producto[$fecha]['pedidos'] : 0;
            $ventas = $producto[$fecha]['ventas'] ? $producto[$fecha]['ventas'] : 0;
            $temp .= '<td>' . $pedidos . '</td>';
            $temp .= '<td>' . $ventas . '</td>';
            if ($i < 12) {
                $sum_ventas += $ventas;
                $sum_pedidos += $pedidos;
            }
        }
        //backorders
        $totalBo = 0;
        foreach ($bos as $bo) {
            $vbo = isset($producto[$bo]) ? $producto[$bo] : 0;
            $totalBo += $vbo;
            $temp .= '<td>' . $vbo . '</td>';
        }
        //obtenemos cantidad sugerida
        $promedio_ventas = $producto["promedio_manual"] ? $producto["promedio_manual"] : round(($sum_ventas / 12));
        $dias_inventario = $promedio_ventas != 0 ? (($producto['existencia'] + $totalBo) / $promedio_ventas) * 30 : 0;
        $dias_entrega = $producto['tiempo_produccion'] + $producto['tiempo_transporte'];
        $cantidad = 0;
        $estado = 0;
        if ($model->isNewRecord) {
            if ($dias_entrega >= $dias_inventario) {
                $cantidad = $producto["factor_pedido"] ? round($promedio_ventas * $producto["factor_pedido"]) : $promedio_ventas;
                $estado = $cantidad;
            }
        } else {
            $cantidad = $producto['cantidad'];
            $estado = $producto['estado'];
        }
        $temp .= '<td>' . round(($sum_pedidos / 12), 2) . '</td>
      <td>' . round(($sum_ventas / 12), 2) . '</td>
      <td>' . $producto['promedio_manual'].
                /*Html::input('text', "Producto[" . $producto['producto_id'] . "][promedio_manual]", $producto["promedio_manual"], ['style' => 'width:50px']) */''. '</td>
      <td'.($fac > 1.5?'>':' class = "bg-pink">') . $fac . '</td>
      <td>' . '$' . number_format($producto['precio1'],0) . '</td>
      <td>' . '$' . number_format($producto['precio2'],0) . '</td>
      <td>' . $producto['precio4'] . '</td>
      <td>' . $producto['existencia'] . '</td>';
        if($model->isNewRecord || $model->estado == 0){
            $temp .= '<td>' . $producto['repetido'] . '</td>
                <td>' . round(($dias_inventario / 30), 1) . '</td>
                <td>' . Html::input('text', "PedidoProducto[" . $producto['producto_id'] . "][cantidad]", $cantidad, ['style' => 'width:50px']) . '</td>
                <td>' . Html::checkbox("PedidoProducto[" . $producto['producto_id'] . "][estado]", $estado) . '</td>';
        }else{
            $temp .= '<td' . ( $producto['cantidadP']?'>':' class = "bg-yellow">') .$producto['cantidadP']. '</td>';
        }
        
        foreach ($model->pedidoLogs as $pedidoLog) {
            if ($pedidoLog->estado > 0 && $pedidoLog->estado < 4) {
                $flag = false;
                $totales_precio[$pedidoLog->id] = 0;
                $countProd = count($pedidoLog->pedidoProductoLogs);
                foreach ($pedidoLog->pedidoProductoLogs as $pedidoProductoLog) {
                    if ($pedidoProductoLog->producto_id == $producto['producto_id'] && $pedidoLog->estado) {
                        $totales_precio[$pedidoLog->id] += $pedidoProductoLog->precio;
                        $temp .= '<td'.($pedidoProductoLog->cantidad?'>':' class = "bg-red">') . $pedidoProductoLog->cantidad . '</td>
                        <td'. ($producto['precio4'] != $pedidoProductoLog->precio && $countProd > 0 && $pedidoLog->estado == 1 ?' class = "bg-blue">':'>') . $pedidoProductoLog->precio . '</td>
                        <td>' . (($countPLogs === 1 && $countProd > 0 && !in_array($model->estado, [2, 3])) ?
                                Html::input('text', 'PedidoProductoLog[' . $producto['producto_id'] . '][nueva_cantidad]', $pedidoProductoLog->nueva_cantidad, ['class' => 'nueva-info']) :
                                $pedidoProductoLog->nueva_cantidad) . '</td>
                        <td>' . (($countPLogs === 1 && $countProd > 0 && !in_array($model->estado, [2, 3])) ?
                                Html::input('text', "PedidoProductoLog[" . $producto['producto_id'] . "][observacion]", $pedidoProductoLog->observacion, ['class' => 'nueva-info']) :
                                $pedidoProductoLog->observacion) .
                                Html::hiddenInput("PedidoProductoLog[" . $producto['producto_id'] . "][pedido_log_id]", $pedidoProductoLog->pedido_log_id) . '</td>';
                        $flag = true;
                    }
                }
                if (!$flag) {
                    $temp .= '<td class = "bg-red">0</td>
                        <td>0</td>
                        <td></td>
                        <td></td>';
                }
            }
        }
//        if ($model->pedidoLogs && !in_array($model->estado, [2, 3])) {
//            $table .= '<td>' . Html::checkbox("PedidoProducto[" . $producto['producto_id'] . "][estado]", $producto['estado']) . '</td>';
//        }
    $temp .='</tr>';
        $table .= $temp;
//        if ($cantidad) {
//            $table .= $temp;
//        } else {
//            $bottom .= $temp;
//        }
    }
    $table .= $bottom . '</tbody>
</table>';
    $footer = '<tfoot>
    <tr>
      <th colspan="41">Totales</th>
      <th>' . $total_precio4 . '</th>
      <th colspan="3"></th>';
    foreach ($totales_precio as $value) {
        $footer .= '<th>' . $value . '</th>
                <th colspan="4"></th>';
    }
    $footer .= '</tr>
        </tfoot>';
    echo Html::beginForm(['create'], 'post');
    echo '<div class="table-responsive" style="height: 85vh;">' . $thead . $table . '</div>';
    echo Html::endForm();
    ?>