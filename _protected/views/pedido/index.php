<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Pedidos');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="pedido-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo_pedido',
        'codigo',
        [
                'attribute' => 'proveedor_id',
                'label' => Yii::t('app', 'Proveedor'),
                'value' => function($model)use($estados){ 
                    return $estados[$model->estado]['value'];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->asArray()->all(), 'id', 'nombre'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Proveedor', 'id' => 'grid-pedido-search-proveedor_id']
            ],
        'fecha',
        [
                'attribute' => 'estado',
                'label' => Yii::t('app', 'Estado'),
                'value' => function($model){
                    return $model->nombreEstado->value;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\ListItem::find()->where(['list'=>'estado_pedido'])->asArray()->all(), 'code', 'value'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado', 'id' => 'grid-pedido-search-estado']
            ],
        'observacion',
        [ 
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view},{update} {accept} {delete}',
            'buttons' => [
                        'view' => function ($url, $model) {
                            $url = Url::to(['view', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                        },
                        'update' => function ($url, $model) {
                            $url = Url::to(['update', 'id' =>  $model->id]);
                            return Html::a($model->estado < 2?'<span class="glyphicon glyphicon-pencil"></span>':'<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => $model->estado < 2?Yii::t('yii', 'Update'):Yii::t('yii', 'View')]);
                        },
//                        'accept' => function ($url, $model) {
//                            $url = Url::to(['accept', 'proveedor_id' =>  $model->id]);
//                            return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Accept')]);
//                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['delete', 'id' =>  $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                                                                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                                                                    'data-method' => 'post',
                            ]);
                        },
                        
                        ],
                    'visibleButtons' => [
                        'delete' => function ($model, $key, $index) {
                            return $model->productos?false:true;
                         },
//                        'accept' => function ($model, $key, $index) {
//                            return $model->estado == 1?true:false;
//                         },
//                        'update' => function ($model, $key, $index) {
//                            return $model->estado == 0?true:false;
//                         },
                   ],             
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
