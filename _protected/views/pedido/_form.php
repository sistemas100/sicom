<?php

use mootensai\components\JsBlock;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

JsBlock::widget(['viewFile' => '_script', 'pos' => View::POS_END,
    'viewParams' => [
        'class' => 'PedidoProducto',
        'relID' => 'pedido-producto',
        'value' => Json::encode($model->pedidoProductos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

$this->registerJs(
    "$('#accept-cotizacion').on('click', function() {
        var flag = true;
        $('.nueva-info').each(function() {
            if($(this).val()!== ''){
                alert('Existen campos con datos ingresados, corrija la información para continuar');
                flag = false;
                return false;
            }
        });
        if(flag){
            return confirm('Está seguro que desea confirmar la cotización del proveedor?');
        }else{
            return false;
        }
    });
    
    $('.promedio').on('change', function() {
        row = $(this).attr('id');
        promedio_ventas = $(this).val();
        existencia = parseInt($('#existencia-'+row).text());
        totalbo = parseInt($('#totalbo-'+row).val());
        dias_entrega = parseInt($('#dias-entrega-'+row).val());
        frecuencia_pedido = parseInt($('#frecuencia-pedido-'+row).val());
        factor_pedido = parseInt($('#factor-pedido-'+row).val());
        cantidad = 0;
        dias_inventario = promedio_ventas != 0 ? ((existencia + totalbo) / promedio_ventas) * 30 : 0;
        if (dias_entrega >= dias_inventario) {
            cantidad = factor_pedido*promedio_ventas;
        }else if(dias_entrega + frecuencia_pedido >= dias_inventario) {
            cantidad = ((factor_pedido*promedio_ventas)/frecuencia_pedido)*(dias_entrega + frecuencia_pedido - dias_inventario) ;
        }
        meses  = dias_inventario/30;
        $('#meses-'+row).text(meses.toFixed(2));
        $('#cantidad-'+row).val(cantidad.toFixed(0));
    });
    
", View::POS_READY
);
?>

<div class="pedido-form">

<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model); ?>
<?php
    echo $this->render('_formPedidoProducto', [
        'model' => $model,
        'productos' => $productos,
        'fechas' => isset($fechas) ? $fechas : null,
        'bos' => $bos,
        'precios' => $precios,
    ]);
?>
    <div>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?php // $form->field($model, 'estado')->textInput(['maxlength' => true, 'placeholder' => 'Estado','disabled' => $model->estado>0]) ?>
    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true, 'placeholder' => 'Pedido Proveedor', 'disabled' => $model->estado < 1]) ?>
    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true, 'placeholder' => 'Observacion', 'disabled' => $model->estado < 0]) ?>
     
    <?php
    echo Html::a(Yii::t('app', 'Atras'), ['proveedor/purchases', 'id' => $model->proveedor_id], ['class' => 'btn btn-danger']);
//    echo ' ' . Html::a('<span class="glyphicon glyphicon-export"></span> Exportar','', ['id'=>'btn_exportar','class' => 'btn btn-primary','title' => Yii::t('yii', 'Exportar')]);
    if ($model->isNewRecord) {
        echo ' ' . Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'name' => 'save', 'value' => 'save']);
    } elseif ($model->estado == 0) {
        echo ' ' . Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'name' => 'save', 'value' => 'save']);
        echo ' ' . Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary', 'name' => 'send', 'value' => 'send']);
    } elseif ($model->estado == 1 && count($model->getLastLog(1)->pedidoProductoLogs) > 0) {
        echo ' ' . Html::submitButton(Yii::t('app', 'Accept'), ['class' => 'btn btn-primary',
            'name' => 'accept',
            'value' => 'accept',
            'id' => 'accept-cotizacion',
        ]);
        echo ' ' . Html::submitButton(Yii::t('app', 'Resend'), ['class' => 'btn btn-primary', 'name' => 'resend', 'value' => 'resend',
            'data' => ['confirm' => Yii::t('app', 'Estás seguro que deseas enviar los cambios ingresados al proveedor?')]
        ]);
    }
    ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
