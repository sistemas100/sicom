<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Pedido').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        [
                'attribute' => 'proveedor.id',
                'label' => Yii::t('app', 'Proveedor')
            ],
        'fecha',
        'estado',
        'observacion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerPedidoLog->totalCount){
    $gridColumnPedidoLog = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'fecha',
        'estado',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPedidoLog,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Pedido Log')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPedidoLog
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPedidoProducto->totalCount){
    $gridColumnPedidoProducto = [
        ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'producto.id',
                'label' => Yii::t('app', 'Producto')
            ],
                'cantidad_pedida',
        'cantidad_disponible',
        'precio_und',
        'observacion',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPedidoProducto,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Pedido Producto')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPedidoProducto
    ]);
}
?>
    </div>
</div>
