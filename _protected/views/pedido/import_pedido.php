<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

?>

<div class="form">

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <button>Importar</button>
    <br>
    <br>

    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
<?php ActiveForm::end() ?>
</div>


<h4>Ejemplo de formmato del archivo:</h4>
<table border="1" class="table">
    <thead>
        <tr>
            <th>Código producto</th>
            <th>Cantidad</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>C1252</td>
            <td>55</td>
        </tr>
        <tr>
            <td>C14008</td>
            <td>100</td>
        </tr>
    </tbody>
</table>

<ul type="disc">
    <li>Extensión: .xlsx</li>
    <li>Los productos deben estar creados previamente</li>
    <li>Los productos deben estar activos en el módulo de productos</li>
    <li>No beben haber productos repetidos</li>
</ul>