<?php

use app\models\ProductoSearch;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = Yii::t('app', Yii::$app->name);

$searchModel = new ProductoSearch();
$dataProvider = $searchModel->search(['ProductoSearch' => ['bajo_stock' => 1,'tipo'=>'<>()','estado'=>'1']]);
$cant_bajo_stock = $dataProvider->getTotalCount();

$searchModel = new ProductoSearch();
$dataProvider = $searchModel->search(['ProductoSearch' => ['meses' => '>=5','tipo'=>'<>()','estado'=>'1']]);
$cant_alto_stock = $dataProvider->getTotalCount();

$searchModel = new ProductoSearch();
$dataProvider = $searchModel->search(['ProductoSearch' => ['precio1' => 0,'tipo'=>'<>()','bo'=>'0','estado'=>'1']]);
$cant_agotados_sin_pedido = $dataProvider->getTotalCount();

$searchModel = new ProductoSearch();
$dataProvider = $searchModel->search(['ProductoSearch' => ['precio1' => 0,'tipo'=>'<>()','bo'=>'>0','estado'=>'1']]);
$cant_agotados_pedidos = $dataProvider->getTotalCount();
?>

<div class="container-fluid">
    <h1> Bienvenido <?= isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : 'Invitado' ?>!</h1>
    <br>
    
    
    <?php if ($cant_bajo_stock) { ?>
    <h5><?= Html::a('<b><i class="fa glyphicon glyphicon-warning-sign"></i></b>&nbsp Hay <b>' . $cant_bajo_stock . '</b> productos con bajo stock!', ['producto/index?ProductoSearch[bajo_stock]=1&ProductoSearch[tipo]=<>()&ProductoSearch[estado]=1']) ?></h5>
    <?php } ?>
        
    <?php if ($cant_alto_stock) { ?>
        <h5><?= Html::a('<b><i class="fa glyphicon glyphicon-warning-sign"></i></b>&nbsp Hay <b>' . $cant_alto_stock . '</b> productos con alto inventario!', ['producto/index?ProductoSearch[meses]=>%3D5&ProductoSearch[tipo]=<>()&ProductoSearch[estado]=1']) ?></h5>
    <?php } ?>
    
    <?php if ($cant_agotados_sin_pedido) { ?>
        <h5><?= Html::a('<b><i class="fa glyphicon glyphicon-warning-sign"></i></b>&nbsp Hay <b>' . $cant_agotados_sin_pedido . '</b> productos agotados sin pedido!', ['producto/index?ProductoSearch[precio1]=0&ProductoSearch[tipo]=<>()&ProductoSearch[bo]=0&ProductoSearch[estado]=1']) ?></h5>
    <?php } ?>
    
    <?php if ($cant_agotados_pedidos) { ?>
        <h5><?= Html::a('<b><i class="fa glyphicon glyphicon-warning-sign"></i></b>&nbsp Hay <b>' . $cant_agotados_pedidos . '</b> productos agotados con pedido!', ['producto/index?ProductoSearch[precio1]=0&ProductoSearch[tipo]=<>()&ProductoSearch[bo]=>0&ProductoSearch[estado]=1']) ?></h5>
    <?php } ?>
</div>

