<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ConsolidadoVentas */

$this->title = Yii::t('app', 'Create Consolidado Ventas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consolidado Ventas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consolidado-ventas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
