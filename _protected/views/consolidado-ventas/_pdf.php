<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ConsolidadoVentas */

$this->title = $model->producto_codigo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consolidado Ventas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consolidado-ventas-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Consolidado Ventas').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'producto_codigo',
        'fecha',
        'cantidad',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
