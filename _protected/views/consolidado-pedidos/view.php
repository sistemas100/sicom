<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ConsolidadoPedidos */

$this->title = $model->producto_codigo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consolidado Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consolidado-pedidos-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Consolidado Pedidos').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'producto_codigo' => $model->producto_codigo, 'fecha' => $model->fecha], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'producto_codigo' => $model->producto_codigo, 'fecha' => $model->fecha], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'producto_codigo',
        'fecha',
        'cantidad',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
