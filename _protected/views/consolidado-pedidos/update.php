<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConsolidadoPedidos */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Consolidado Pedidos',
]) . ' ' . $model->producto_codigo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consolidado Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->producto_codigo, 'url' => ['view', 'producto_codigo' => $model->producto_codigo, 'fecha' => $model->fecha]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="consolidado-pedidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
