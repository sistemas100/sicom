<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="distribuidor-form">

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [ 
                        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE, 
                        'saveFormat' => 'php:Y-m-d', 
                        'ajaxConversion' => true, 
                        'widgetOptions' => [ 
                            'pluginOptions' => [ 
                                'placeholder' => Yii::t('app', 'Choose Fecha'), 
                                'autoclose' => true,
                                'todayHighlight' => true,
		           ],
                           'size' => 'sm'
		       ], 
		   ]); ?> 
    
<!--    'date' => ['type' => Form::INPUT_WIDGET, 
                        'widgetClass' => '\kartik\widgets\DatePicker',
                        'options' => ['pluginOptions' => ['format' => 'yyyy-mm-dd', 'autoclose'=>true, 'todayHighlight' => true]]],-->
    <?= $form->field($model, 'file')->fileInput() ?>

    <button>Importar</button>
    <br>
    <br>

    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
<?php ActiveForm::end() ?>
</div>
