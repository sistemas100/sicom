<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ConsolidadoPedidos */

$this->title = Yii::t('app', 'Create Consolidado Pedidos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consolidado Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consolidado-pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
