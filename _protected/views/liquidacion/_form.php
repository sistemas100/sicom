<?php

use app\models\Liquidacion;
use app\models\ListItem;
use kartik\money\MaskMoney;
use kartik\tabs\TabsX;
use mootensai\components\JsBlock;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Liquidacion */
/* @var $form ActiveForm */

JsBlock::widget(['viewFile' => '_script', 'pos' => View::POS_END,
    'viewParams' => [
        'class' => 'LiquidacionItem',
        'relID' => 'liquidacion-item',
        'value' => Json::encode($model->liquidacionItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="liquidacion-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'estado')->hiddenInput(['value' => 1])->label(false) ?>
    <?= $form->field($model, 'valor_factura')->widget(MaskMoney::classname(),['pluginOptions' => ['precision' => 2]])?>

    <?php
    if ($model->liquidacionItems) {
        $row = ArrayHelper::toArray($model->liquidacionItems);
    } else {
        $row = [];
        foreach (ListItem::findAll(['list' => 'item_liquidacion']) as $listItem) {
            $row[] = [
                'liquidacion_id' => $model->id,
                'item' => $listItem->id,
                'moneda' => 'usd',
                'usdcop' => '',
                'eurusd' => '',
                'valor' => '',
                'valor_cop' => ''
            ];
        }
    }

    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'LiquidacionItem')),
            'content' => $this->render('_formLiquidacionItem', [
                'row' => $row,
            ]),
        ],
    ];
    echo TabsX::widget([
        'items' => $forms,
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?= $form->field($model, 'total_gastos')->widget(MaskMoney::classname()) ?>
    <?= $form->field($model, 'impuestos')->widget(MaskMoney::classname())?>
    <?= $form->field($model, 'total_importacion')->widget(MaskMoney::classname())?>
    <?= $form->field($model, 'factor_importacion')->widget(MaskMoney::classname())?>
    <?= $form->field($model, 'factor_liquidacion')->textInput(['maxlength' => true, 'placeholder' => 'Factor Liquidacion']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
