<?php

use app\models\Liquidacion;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Liquidacion */

$this->title = Liquidacion::getStrPedidos($model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Liquidación').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
    //    'estado',
          [
            'attribute' => 'valor_factura', 
            'value' => number_format($model->valor_factura,2),
            'hAlign'=>'right'
        ],
          [
            'attribute' => 'total_gastos', 
            'value' => '$' . number_format($model->total_gastos,2),
            'hAlign'=>'right'
        ],
          [
            'attribute' => 'impuestos', 
            'value' => '$' . number_format($model->impuestos,2),
            'hAlign'=>'right'
        ],
          [
            'attribute' => 'total_importacion', 
            'value' => '$' . number_format($model->total_importacion,2),
            'hAlign'=>'right'
        ],
        'factor_importacion',
        'factor_liquidacion', 
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerLiquidacionItem->totalCount){
    $gridColumnLiquidacionItem = [
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'item0.id',
                'label' => Yii::t('app', 'Item')
            ],
            'moneda',
            [
                'attribute' => 'usdcop',
                'value' => function($model) {
                    return '$' . number_format($model->usdcop,2);   
                },
                'hAlign'=>'right'
            ],
            [
                'attribute' => 'eurusd',
                'value' => function($model) {
                    return number_format($model->eurusd,2);   
                },
                'hAlign'=>'right'
            ],
            [
                'attribute' => 'valor',
                'value' => function($model) {
                    return number_format($model->valor,2);   
                },
                'hAlign'=>'right'
            ],
            [
                'attribute' => 'valor_cop',
                'value' => function($model) {
                    return '$' . number_format($model->valor_cop,2);   
                },
                'hAlign'=>'right'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerLiquidacionItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-liquidacion-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Liquidacion Item')),
        ],
        'export' => false,
        'columns' => $gridColumnLiquidacionItem
    ]);
}
?>
    </div>
</div>
