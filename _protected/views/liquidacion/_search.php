<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LiquidacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-liquidacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'estado')->textInput(['placeholder' => 'Estado']) ?>

    <?= $form->field($model, 'valor_factura')->textInput(['maxlength' => true, 'placeholder' => 'Valor Factura']) ?>

    <?= $form->field($model, 'total_gastos')->textInput(['maxlength' => true, 'placeholder' => 'Total Gastos']) ?>

    <?= $form->field($model, 'impuestos')->textInput(['maxlength' => true, 'placeholder' => 'Impuestos']) ?>

    <?php /* echo $form->field($model, 'total_importacion')->textInput(['maxlength' => true, 'placeholder' => 'Total Importacion']) */ ?>

    <?php /* echo $form->field($model, 'factor_importacion')->textInput(['maxlength' => true, 'placeholder' => 'Factor Importacion']) */ ?>

    <?php /* echo $form->field($model, 'factor_liquidacion')->textInput(['maxlength' => true, 'placeholder' => 'Factor Liquidacion']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
