<?php
/* @var $this View */
/* @var $searchModel LiquidacionSearch */
/* @var $dataProvider ActiveDataProvider */

use app\models\Liquidacion;
use app\models\LiquidacionSearch;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Liquidaciones');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="liquidacion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'label' => 'Pedidos',
            'value' => function($model) {
                return Liquidacion::getStrPedidos($model->id);
            },
        ],
        [
            'attribute' => 'valor_factura',
            'value' => function($model) {
                return number_format($model->valor_factura, 2);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'total_gastos',
            'value' => function($model) {
                return '$' . number_format($model->total_gastos);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'impuestos',
            'value' => function($model) {
                return '$' . number_format($model->impuestos);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'total_importacion',
            'value' => function($model) {
                return '$' . number_format($model->total_importacion);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'factor_importacion',
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'factor_liquidacion',
            'hAlign' => 'right'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {report} {delete} ',
            'buttons' => ['view' => function ($url, $model) {
                    $url = Url::to(['view', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                },
                'update' => function ($url, $model) {
                    $url = Url::to(['update', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                },
                'report' => function ($url, $model) {
                    $url = Url::to(['report', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('yii', 'Report')]);
                },
                'delete' => function ($url, $model) {
                    $url = Url::to(['delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                    ]);
                },
            ],
            'visibleButtons' => [
                'delete' => false
            ],
        ],
    ];
    ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-liquidacion']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]),
        ],
    ]);
    ?>

</div>
