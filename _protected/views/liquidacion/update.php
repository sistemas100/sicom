<?php

use app\models\Liquidacion;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model Liquidacion */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Liquidacion',
]) . ' ' . Liquidacion::getStrPedidos($model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="liquidacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
