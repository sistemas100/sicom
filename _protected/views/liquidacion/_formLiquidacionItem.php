<div class="form-group" id="add-liquidacion-item">
    <?php

    use app\models\ListItem;
    use kartik\builder\TabularForm;
    use kartik\grid\GridView;
    use kartik\money\MaskMoney;
    use kartik\widgets\Select2;
    use yii\data\ArrayDataProvider;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use yii\web\View;

$this->registerJs(
    "$('.input-value').on('change', function() {
        valor_total = 0;
        $('.kv-tabform-row').each(function(){
            row = $(this).attr('data-key');
            eurusd = parseFloat($('#liquidacionitem-'+row+'-eurusd').val());
            usdcop = parseFloat($('#liquidacionitem-'+row+'-usdcop').val());
            moneda = $('#liquidacionitem-'+row+'-moneda').val();
            valor = parseFloat($('#liquidacionitem-'+row+'-valor').val());
            if(!isNaN(valor) || !isNaN(usdcop)){
                valor_cop = moneda === 'usd'?valor*usdcop:valor*usdcop*eurusd;
                $('#liquidacionitem-'+row+'-valor_cop').val(valor_cop);
                $('#liquidacionitem-'+row+'-valor_cop-disp').val(valor_cop);
                $('#liquidacionitem-'+row+'-valor_cop-disp').maskMoney('mask');
            }else{
                valor_cop = parseFloat($('#liquidacionitem-'+row+'-valor_cop').val());
            }
            valor_total += (!isNaN(valor_cop) ? valor_cop : 0);
        });
        $('#liquidacion-total_gastos').val(valor_total);
        $('#liquidacion-total_gastos-disp').val(valor_total);
        $('#liquidacion-total_gastos-disp').maskMoney('mask');
        impuestos =(valor_total*4)/1000;
        $('#liquidacion-impuestos').val(impuestos);
        $('#liquidacion-impuestos-disp').val(impuestos);
        $('#liquidacion-impuestos-disp').maskMoney('mask');
        
        $('#liquidacion-total_importacion').val(valor_total+impuestos);
        $('#liquidacion-total_importacion-disp').val(valor_total+impuestos);
        $('#liquidacion-total_importacion-disp').maskMoney('mask');
        
        tasa_cambio = $('#liquidacionitem-0-moneda').val()==='usd'?1:parseFloat($('#liquidacionitem-0-eurusd').val());
        valor_factura = $('#liquidacion-valor_factura').val()?parseFloat($('#liquidacion-valor_factura').val()):0;
        valor_factura = valor_factura*tasa_cambio;
        fi = valor_factura === 0?0:(valor_total+impuestos)/valor_factura;
        $('#liquidacion-factor_importacion').val(fi.toFixed(2));
        $('#liquidacion-factor_importacion-disp').val(fi.toFixed(2));
        $('#liquidacion-factor_importacion-disp').maskMoney('mask');
        
    });
    ", View::POS_READY
    );

    $dataProvider = new ArrayDataProvider([
        'allModels' => $row,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo TabularForm::widget([
        'dataProvider' => $dataProvider,
        'formName' => 'LiquidacionItem',
        'checkboxColumn' => false,
        'actionColumn' => false,
        'attributeDefaults' => [
            'type' => TabularForm::INPUT_TEXT,
        ],
        'attributes' => [
            "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
            'item' => [
                'label' => 'List item',
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => Select2::className(),
                'options' => [
                    'data' => ArrayHelper::map(ListItem::find()->where(['list' => 'item_liquidacion'])->orderBy('id')->asArray()->all(), 'id', 'value'),
                    'options' => ['placeholder' => Yii::t('app', 'Choose List item')],
                ],
                'columnOptions' => ['width' => '200px']
            ],
            'moneda' => ['type' => TabularForm::INPUT_DROPDOWN_LIST,
                'items' => ['usd' => 'USD', 'eur' => 'EUR',],
                'options' => [
                    'columnOptions' => ['width' => '185px'],
                    'options' => ['placeholder' => Yii::t('app', 'Choose Moneda')],
                    'class' => 'form-control input-value'
                ]
            ],
            'valor' => ['type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => MaskMoney::classname(),
                'options' => ['pluginOptions' => ['precision' => 2],
                    'options' => ['class' => 'form-control input-value']]
            ],
            'usdcop' => ['type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => MaskMoney::classname(),
                'options' => ['pluginOptions' => ['precision' => 0],
                    'options' => ['class' => 'form-control input-value']]
            ],
            'eurusd' => ['type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => MaskMoney::classname(),
                'options' => ['pluginOptions' => ['precision' => 4],
                    'options' => ['class' => 'form-control input-value']]
            ],
            'valor_cop' => ['type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => MaskMoney::classname(),
                'options' => ['options' => ['class' => 'form-control input-value']]
            ],
            'del' => [
                'type' => 'raw',
                'label' => '',
                'value' => function($model, $key) {
                    return
                            Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                            Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' => Yii::t('app', 'Delete'), 'onClick' => 'delRowLiquidacionItem(' . $key . '); return false;', 'id' => 'liquidacion-item-del-btn']);
                },
            ],
        ],
        'gridSettings' => [
            'panel' => [
                'heading' => false,
                'type' => GridView::TYPE_DEFAULT,
                'before' => false,
                'footer' => false,
                'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('app', 'Add Liquidacion Item'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowLiquidacionItem()']),
            ]
        ]
    ]);
    echo "    </div>\n\n";
    ?>

