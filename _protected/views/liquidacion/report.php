<?php

use app\models\Liquidacion;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;

/* @var $this View */
/* @var $model Liquidacion */

$this->title = 'Reporte liquidación (' . Liquidacion::getStrPedidos($model->id).')';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Reporte');
?>
<div class="form-group" id="liquidacion-reporte">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $this->registerJs(
        "$('#report').tableHeadFixer({'left' : 1});
        $('.precios').tooltip({html:true});
            
        var btnDLtoExcel = $('#btn_exportar');
           btnDLtoExcel.on('click', function () {
               $('#report').excelexportjs({
                   containerid: 'report'
                   , datatype: 'table'
               });
                return false;
           });",
            View::POS_READY
    );

    $this->registerJsFile(
            '@web/js/tableHeadFixer.js', ['depends' => [JqueryAsset::className()]]
    );
    $this->registerJsFile(
        '@web/js/excelexportjs.js', ['depends' => [JqueryAsset::className()]]
    );
    $temp = '';
    $count_th = 0;
    $table = ' <table id="report" class="bg-white table-striped table-condensed table-hover table-bordered" width="100%" cellspacing="0" >
  <thead>
    <tr>
        <th style= "text-align:center" rowspan="2">Codigo</th>
        <th style= "text-align:center" rowspan="2">Referencia</th>
        <th style= "text-align:center" rowspan="2">Ref proveedor</th>
        <th style= "text-align:center" rowspan="2">Ref proveedor2</th>
        <th style= "text-align:center" rowspan="2">Nombre</th>
        <th style= "text-align:center" rowspan="2">Marca</th>';
    foreach ($fechas as $fecha) {
        $temp .= '<th style= "text-align:center" ><nobr>' . substr($fecha, 2) . '</nobr></th>';
        $count_th++;
        
    }
    $table .= '
        <th style= "text-align:center" colspan="'.$count_th.'">VENTAS MENSUALES</th>
        <th style= "text-align:center" rowspan="2">Costo</th>
        <th style= "text-align:center" rowspan="2">USD Imp</th>
        <th style= "text-align:center" rowspan="2">Costo COP</th>
        <th style= "text-align:center" rowspan="2"> FL '. round($model->factor_liquidacion,1) . '</th>
        <th style= "text-align:center" rowspan="2">Precio final</th>
        <th style= "text-align:center" rowspan="2">FAC</th>
        <th style= "text-align:center" rowspan="2">Lista1</th>
        <th style= "text-align:center" rowspan="2">Lista2</th>
        <th style= "text-align:center" rowspan="2">Cant</th>
        <th style= "text-align:center" rowspan="2">Saldo</th>
        <th style= "text-align:center" rowspan="2">Competencia</th>
        </tr>
        <tr>'.
        $temp.
        '</tr>
      ';
    $table .= '</thead><tbody>';
    $moneda = '';
    $eurusd = 1;
    foreach ($model->liquidacionItems as  $item) {
        if($item->item == 5){
            $moneda = $item->moneda;
            $eurusd = $item->eurusd?$item->eurusd:1;
        }
    }

    foreach ($productos as $key => $producto) {
        $fac = ($producto['upcompra'] != 0) ? $producto['precio1'] != 0 ? (round(($producto['precio1'] / $producto['upcompra']), 2)) : (round(($producto['precio2'] / $producto['upcompra']), 2)) : '0';
        $table .= '<tr>
        <th scope="row">' . $producto['codigo'] . Html::hiddenInput("PedidoProducto[$key][producto_id]", $producto['producto_id']) .
                Html::hiddenInput("PedidoProducto[$key][pedido_log_id]", $producto['pedido_log_id']) . '</th>
        <td>' . $producto['referencia'] . '</td>
        <td>' . $producto['referencia_proveedor'] . '</td>
        <td>' . $producto['referencia_proveedor2'] . '</td>
        <td>' . $producto['nombre'] . '</td>
        <td>' . $producto['nombre_proveedor'] . '</td>';
        $sum_pedidos = 0;
        $sum_ventas = 0;
        //ventas y pedidos por mes
        foreach ($fechas as $i => $fecha) {
            $table .= '<td style= "text-align:center">' . $producto[$fecha]['ventas'] . '</td>';
            if ($i < 6) {
                $sum_ventas += $producto[$fecha]['ventas'];
                $sum_pedidos += $producto[$fecha]['pedidos'];
            }
        }
        
        $costo = $moneda == 'eur'?$producto['costo']*$eurusd:$producto['costo'];
        $table .= '
  
        <td style= "text-align:right">' . number_format($costo, 2) . '</td>
        <td style= "text-align:right">' . number_format($model->factor_importacion, 0) . '</td>
        <td style= "text-align:right">' . '$' . number_format($model->factor_importacion * $costo, 0) . '</td>
        <td style= "text-align:right">' . '$' . number_format($model->factor_liquidacion * $model->factor_importacion * $costo, 0) . '</td>
        <td>' . Html::input('text', "PedidoProducto[$key][observacion]", $producto['observacion'], ['style' => 'width:80px']) . '</td>
        <td style= "text-align:right"' . ($fac > 1.5 ? '>' : ' class = "bg-pink">') . $fac . '</td>
        <td style= "text-align:right">' . $producto['precio1'] . '</td>
        <td style= "text-align:right">' . $producto['precio2'] . '</td>
        <td style= "text-align:right">' . $producto['cantidad'] . '</td>
        <td style= "text-align:right">' . $producto['existencia'] . '</td>
        <td  style= "text-align:right" class = "precios" title="'.$precios[$producto['producto_id']]['todos'].'">' . $precios[$producto['producto_id']]['menor'] . '</td>';
        $table .= '</tr>';
    }
    $table .= '</tbody>
</table>';
    echo Html::beginForm();
    echo '<div class="report">' . $table . '</div>';
    ?>
    <br>
    <div>
        <?php
//        echo Html::a('<span class="glyphicon glyphicon-export"></span> Exportar','', ['id'=>'btn_exportar','class' => 'btn btn-primary','title' => Yii::t('yii', 'Exportar')]);
        echo ' ' . Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'name' => 'save', 'value' => 'save', 'id' => 'save']);
        echo ' ' . Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']);
        ?>
    </div>
    <?php echo Html::endForm(); ?>
</div>