<div class="form-group" id="add-pedido-producto">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;


$dataProvider = new ArrayDataProvider([
    'allModels' => \yii\helpers\ArrayHelper::toArray($productos),
    'pagination' => [
        'pageSize' => -1
    ]
]);
// The controller action that will render the list
$url = \yii\helpers\Url::to(['product-list']);

echo Html::beginForm();
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PedidoProducto',
    'actionColumn' => false,
    'serialColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id'=>[
            'label' => '',
            'type'=>TabularForm::INPUT_HIDDEN, 
        ],
        'codigo'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'referencia'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'nombre'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'precio1'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'precio2'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'precio4'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[6]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[5]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[4]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[3]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[2]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[1]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        $fechas[0]=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'existencia'=>[
            'type'=>TabularForm::INPUT_STATIC, 
        ],
        'cantidad'=>[
            'type'=>TabularForm::INPUT_TEXT, 
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::submitButton(Yii::t('app', 'Crear Pedido'), ['type' => 'button', 'class' => 'btn btn-success']),
        ]
    ]
]);
echo Html::endForm();
echo  "    </div>\n\n";
?>

