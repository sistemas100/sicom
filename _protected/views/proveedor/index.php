<?php
/* @var $this View */
/* @var $searchModel ProveedorSearch */
/* @var $dataProvider ActiveDataProvider */

use app\models\Liquidacion;
use app\models\Pedido;
use app\models\PedidoProductoLog;
use app\models\ProveedorSearch;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('app', 'Proveedores');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="proveedor-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Proveedor'), ['create'], ['class' => 'btn btn-success']) ?>
        <?php // Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </span>
    </h1>
    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
//        'nit',
//        'contacto',
//        'telefono',
//        'direccion',
//        'ciudad',
        'correo',
        'tiempo_produccion',
        'tiempo_transporte',
        'frecuencia_pedido',
        'factor_pedido',
        'columna_codigo',
        'columna_cantidad',
        'columna_precio',
        'columna_pedido_id',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {purchases} {report} {importD} {approveD} {importF} {approveF} {delete} {liquidate}',
            'buttons' => ['view' => function ($url, $model) {
                    $url = Url::to(['view', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                },
                'update' => function ($url, $model) {
                    $url = Url::to(['update', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                },
                'delete' => function ($url, $model) {
                    $url = Url::to(['delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                    ]);
                },
                'purchases' => function ($url, $model) {
                    $url = Url::to(['purchases', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-shopping-cart"></span>', $url, ['title' => Yii::t('yii', 'Compras')]);
                },
                        
                        
                        
//                'report' => function ($url, $model) {
//                    $url = Url::to(['pedido/create', 'proveedor_id' => $model->id]);
//                    return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('yii', 'Pedido Sugerido')]);
//                },
//                'importD' => function ($url, $model) {
//                    $url = Url::to(['pedido/import', 'proveedor_id' => $model->id, 'estado' => 2]);
//                    return Html::a('<span class="glyphicon glyphicon-import"></span>', $url, ['title' => Yii::t('yii', 'Importar despacho')]);
//                },
//                'importF' => function ($url, $model) {
//                    $url = Url::to(['pedido/import', 'proveedor_id' => $model->id, 'estado' => 3]);
//                    return Html::a('<span class="glyphicon glyphicon-import"></span>', $url, ['title' => Yii::t('yii', 'Importar factura')]);
//                },
//                'approveD' => function ($url, $model) {
//                    $url = Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 2]);
//                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Aprobar despacho')]);
//                },
//                'approveF' => function ($url, $model) {
//                    $url = Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 3]);
//                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Aprobar factura')]);
//                },
//                'liquidate' => function ($url, $model) {
//                    $url = Url::to(['liquidacion/index', 'proveedor_id' => $model->id]);
//                    return Html::a('<span class="glyphicon glyphicon-saved"></span>', $url, ['title' => Yii::t('yii', 'Liquidar factura')]);
//                },
            ],
            'visibleButtons' => [
                'delete' => function ($model, $key, $index) {
                    return $model->pedidos || (isset($model->clientDatas) && $model->clientDatas) ? false : true;
                },
//                'importD' => function ($model, $key, $index) {
//                    return (Pedido::find()->where(['proveedor_id' => $model->id, 'estado' => 2])->count() > 0) ? true : false;
//                },
//                'importF' => function ($model, $key, $index) {
//                    return (Pedido::find()->where(['proveedor_id' => $model->id, 'estado' => 3])->count() > 0) ? true : false;
//                },
//                'approveD' => function ($model, $key, $index) {
//                    return(PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 2, 'pedido.estado' => 2])->join('join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 0) ? true : false;
//                },
//                'approveF' => function ($model, $key, $index) {
//                    return(PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido.estado' => 3, 'pedido_log.estado' => 3])->join('join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 0) ? true : false;
//                },
//                'liquidate' => function ($model, $key, $index) {
//                    return(Liquidacion::find()->where(['pedido.proveedor_id' => $model->id, 'liquidacion.estado' => 0])->join('join', 'pedido_log', 'pedido_log.factura = liquidacion.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 0) ? true : false;
//                },
            ],
        ],
    ];
    ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proveedor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-globe"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
            'bordered'=>true,
    'striped'=>TRUE,
    'condensed'=>TRUE,
    'responsive'=>TRUE,
    'hover'=>TRUE,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]),
        ],
    ]);
    ?>

</div>
