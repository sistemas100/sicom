<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-proveedor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'nit')->textInput(['maxlength' => true, 'placeholder' => 'Nit']) ?>

    <?= $form->field($model, 'contacto')->textInput(['maxlength' => true, 'placeholder' => 'Contacto']) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>

    <?php /* echo $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) */ ?>

    <?php /* echo $form->field($model, 'ciudad')->textInput(['maxlength' => true, 'placeholder' => 'Ciudad']) */ ?>

    <?php /* echo $form->field($model, 'tiempo_produccion')->textInput(['placeholder' => 'Tiempo Produccion']) */ ?>

    <?php /* echo $form->field($model, 'tiempo_transporte')->textInput(['placeholder' => 'Tiempo Transporte']) */ ?>

    <?php /* echo $form->field($model, 'factor_pedido')->textInput(['maxlength' => true, 'placeholder' => 'Factor Pedido']) */ ?>

    <?php /* echo $form->field($model, 'columna_codigo')->textInput(['maxlength' => true, 'placeholder' => 'Columna Codigo']) */ ?>

    <?php /* echo $form->field($model, 'columna_cantidad')->textInput(['maxlength' => true, 'placeholder' => 'Columna Cantidad']) */ ?>

    <?php /* echo $form->field($model, 'columna_precio')->textInput(['maxlength' => true, 'placeholder' => 'Columna Precio']) */ ?>

    <?php /* echo $form->field($model, 'columna_pedido_id')->textInput(['maxlength' => true, 'placeholder' => 'Columna Pedido']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
