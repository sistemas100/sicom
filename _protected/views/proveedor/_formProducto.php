<div class="form-group" id="add-producto">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Producto',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'codigo' => ['type' => TabularForm::INPUT_TEXT],
        'referencia' => ['type' => TabularForm::INPUT_TEXT],
        'nombre' => ['type' => TabularForm::INPUT_TEXT],
        'precio1' => ['type' => TabularForm::INPUT_TEXT],
        'precio2' => ['type' => TabularForm::INPUT_TEXT],
        'precio4' => ['type' => TabularForm::INPUT_TEXT],
        'existencia' => ['type' => TabularForm::INPUT_TEXT],
        'ctoprm' => ['type' => TabularForm::INPUT_TEXT],
        'upcompra' => ['type' => TabularForm::INPUT_TEXT],
        'ufcompra' => ['type' => TabularForm::INPUT_TEXT],
        'ufvta' => ['type' => TabularForm::INPUT_TEXT],
        'nombre_proveedor' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('app', 'Delete'), 'onClick' => 'delRowProducto(' . $key . '); return false;', 'id' => 'producto-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('app', 'Add Producto'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProducto()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

