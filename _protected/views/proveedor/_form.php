<?php

use app\models\Proveedor;
use kartik\widgets\Select2;
use mootensai\components\JsBlock;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model Proveedor */
/* @var $form ActiveForm */

//\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
//    'viewParams' => [
//        'class' => 'Pedido', 
//        'relID' => 'pedido', 
//        'value' => \yii\helpers\Json::encode($model->pedidos),
//        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
//    ]
//]);
JsBlock::widget(['viewFile' => '_script', 'pos'=> View::POS_END, 
    'viewParams' => [
        'class' => 'ProveedorProducto', 
        'relID' => 'proveedor-producto', 
        'value' => Json::encode($model->proveedorProductos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="proveedor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'nit')->textInput(['maxlength' => true, 'placeholder' => 'Nit']) ?>

    <?= $form->field($model, 'contacto')->textInput(['maxlength' => true, 'placeholder' => 'Contacto']) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true, 'placeholder' => 'Ciudad']) ?>
    
    <?= $form->field($model, 'correo')->textInput(['maxlength' => true, 'placeholder' => 'Correo']) ?>
    
    <?= $form->field($model, 'idioma')->widget(Select2::classname(), [
        'data' => ['es'=>'ES','en'=>'EN'],
        'options' => ['placeholder' => Yii::t('app', 'Seleccione idioma')],
    ]); ?>
    <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
        'data' => ['0'=>'Nacional','1'=>'Internacional'],
        'options' => ['placeholder' => Yii::t('app', 'Seleccione tipo')],
    ]); ?>
    
    <?= $form->field($model, 'tiempo_produccion')->textInput(['placeholder' => 'Tiempo Produccion']) ?>
		
    <?= $form->field($model, 'tiempo_transporte')->textInput(['placeholder' => 'Tiempo Transporte']) ?>
    
    <?= $form->field($model, 'frecuencia_pedido')->textInput(['placeholder' => 'Frecuencia Pedido']) ?>
     
    <?= $form->field($model, 'factor_pedido')->textInput(['maxlength' => true, 'placeholder' => 'Factor Reposición']) ?>
    
    <?= $form->field($model, 'bo_aceptados')->textInput(['placeholder' => 'BO aceptados']) ?>
    
    <?= $form->field($model, 'columna_codigo')->textInput(['maxlength' => true, 'placeholder' => 'Columna Codigo']) ?> 
		 
    <?= $form->field($model, 'columna_cantidad')->textInput(['maxlength' => true, 'placeholder' => 'Columna Cantidad']) ?> 
		 
    <?= $form->field($model, 'columna_precio')->textInput(['maxlength' => true, 'placeholder' => 'Columna Precio']) ?> 
    
    <?= $form->field($model, 'columna_pedido_id')->textInput(['maxlength' => true, 'placeholder' => 'Columna Pedido']) ?>

    <?php
//    $forms = [
//        [
//            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'Pedido')),
//            'content' => $this->render('_formPedido', [
//                'row' => \yii\helpers\ArrayHelper::toArray($model->pedidos),
//            ]),
//        ],
//        [
//            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'ProveedorProducto')),
//            'content' => $this->render('_formProveedorProducto', [
//                'row' => \yii\helpers\ArrayHelper::toArray($model->proveedorProductos),
//            ]),
//        ],
//    ];
//    echo kartik\tabs\TabsX::widget([
//        'items' => $forms,
//        'position' => kartik\tabs\TabsX::POS_ABOVE,
//        'encodeLabels' => false,
//        'pluginOptions' => [
//            'bordered' => true,
//            'sideways' => true,
//            'enableCache' => false,
//        ],
//    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
