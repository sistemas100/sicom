<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proveedor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Proveedor').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'nit',
        'contacto',
        'telefono',
        'direccion',
        'ciudad',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerPedido->totalCount){
    $gridColumnPedido = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'codigo',
                'fecha',
        'estado',
        'observacion',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPedido,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Pedido')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPedido
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerProducto->totalCount){
    $gridColumnProducto = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        'precio1',
        'precio2',
        'precio4',
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
                'nombre_proveedor',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProducto,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Producto')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnProducto
    ]);
}
?>
    </div>
</div>
