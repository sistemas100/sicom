<?php

use app\models\Liquidacion;
use app\models\LiquidacionSearch;
use app\models\ListItem;
use app\models\Pedido;
use app\models\PedidoLog;
use app\models\PedidoProductoLog;
use app\models\PedidoSearch;
use app\models\Proveedor;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $model Proveedor */
/* @var $searchPedido PedidoSearch */
/* @var $searchLiquidacion LiquidacionSearch */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proveedor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Compras: ' . $this->title;
?>
<div class="proveedor-view">
    <h1><?= Html::encode('Compras: ' . $this->title) ?>
        <span class="pull-right">
            <?php
            $pendienteCargaDespacho = PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 2, 'pedido.estado' => 2, 'pedido_log_id' => NULL])
                            ->join('RIGHT join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count();
            $pendienteAprobacionDespacho = PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 2, 'pedido.estado' => 2])
                            ->join('join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count();
            $imporarDespacho = (Pedido::find()->where(['proveedor_id' => $model->id, 'estado' => 2])->count() > 0) ? true : false;
            $aprobarDespacho = (/* !$pendienteCargaDespacho && */$pendienteAprobacionDespacho) ? true : false;
            $enviarDespachoRechazado = (PedidoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 2, 'pedido.estado' => 2])
                            ->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 1) ? true : false;
            $enviarDespachoAprobado = (PedidoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 3, 'pedido.estado' => 3])
                            ->join('join', 'pedido', 'pedido_id = pedido.id')->count() == 1) ? true : false;

            $pendienteCargaFactura = PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 3, 'pedido.estado' => 3, 'pedido_log_id' => NULL])
                            ->join('RIGHT join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count();
            $pendienteAprobacionFactura = PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 3, 'pedido.estado' => 3])
                            ->join('join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count();
            $importarFactura = (Pedido::find()->where(['proveedor_id' => $model->id, 'estado' => 3])->count() > 0) ? true : false;
            $aprobarFactura = (!$pendienteCargaFactura && $pendienteAprobacionFactura) ? true : false;
//            $aprobarFactura = (PedidoProductoLog::find()->where(['proveedor_id' => $model->id, 'pedido.estado' => 3, 'pedido_log.estado' => 3])
//                    ->join('join', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 0)?true:false;
            $enviarFacturaRechazada = (PedidoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 3, 'pedido.estado' => 3])
                            ->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 1) ? true : false;
            $enviarFacturaAprobada = (PedidoLog::find()->where(['proveedor_id' => $model->id, 'pedido_log.estado' => 4, 'pedido.estado' => 4])
                            ->join('join', 'pedido', 'pedido_id = pedido.id')->count() == 1) ? true : false;
            $liquidarFactura = (Liquidacion::find()->where(['pedido.proveedor_id' => $model->id, 'liquidacion.estado' => 0])
                            ->join('join', 'pedido_log', 'pedido_log.factura = liquidacion.id')->join('join', 'pedido', 'pedido_id = pedido.id')->count() > 0) ? true : false;
            ?>

        </span>
    </h1>
    <?php if ($imporarDespacho || $aprobarDespacho || $enviarDespachoRechazado || $enviarDespachoAprobado): ?>
        <div class="well well-sm">  
            <h4>Despachos 
                <span class="pull-right"> <?php
                    //Importar despacho
                    echo ($imporarDespacho) ?
                            Html::a('<span class="glyphicon glyphicon-import"></span> Importar despacho',
                                    Url::to(['pedido/import', 'proveedor_id' => $model->id, 'estado' => 2]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Importar despacho')]) . '&nbsp;' : '';
                    //Aprobar despacho
                    echo ($aprobarDespacho) ?
                            Html::a('<span class="glyphicon glyphicon-ok"></span> Aprobar despacho',
                                    Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 2]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Aprobar despacho')]) . '&nbsp;' : '';
                    //enviar correo despacho rechazado
                    echo ($enviarDespachoRechazado) ?
                            Html::a('<span class="glyphicon glyphicon-envelope"></span>',
                                    Url::to(['pedido/approve', 'estado' => 2, 'estadoPrevio' => 2, 'correo' => $model->correo, 'proveedor_id' => $model->id]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Reenviar correo despacho rechazado')]) . '&nbsp;' : '';
                    //enviar correo despacho aprobado
                    echo ($enviarDespachoAprobado) ?
                            Html::a('<span class="glyphicon glyphicon-envelope"></span>',
                                    Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 2]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Reenviar correo despacho aprobado')]) . '&nbsp;' : '';
                    ?>
            </h4>
        </div>
        <?php
    endif;
    if ($importarFactura || $aprobarFactura || $enviarFacturaRechazada || $enviarFacturaAprobada || $liquidarFactura):
        ?>
        <div class="well well-sm">  
            <h4>Facturas 
                <span class="pull-right"> <?php
                    //Importar factura
                    echo ($importarFactura) ?
                            Html::a('<span class="glyphicon glyphicon-import"></span> Importar factura',
                                    Url::to(['pedido/import', 'proveedor_id' => $model->id, 'estado' => 3]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Importar factura')]) . '&nbsp;' : '';
                    //Aprobar factura
                    echo ($aprobarFactura) ?
                            Html::a('<span class="glyphicon glyphicon-ok"></span> Aprobar factura',
                                    Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 3]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Aprobar factura')]) . '&nbsp;' : '';
                    //Reenviar correo factura rechazada
                    echo ($enviarFacturaRechazada) ?
                            Html::a('<span class="glyphicon glyphicon-envelope"></span>',
                                    Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 2]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Reenviar correo factura rechazada')]) . '&nbsp;' : '';
                    //Reenviar correo factura aprobada
                    echo ($enviarFacturaAprobada) ?
                            Html::a('<span class="glyphicon glyphicon-envelope"></span>',
                                    Url::to(['pedido/approve', 'proveedor_id' => $model->id, 'estado' => 2]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Reenviar correo factura aprobada')]) . '&nbsp;' : '';
                    //Liquidar factura
                    echo ($liquidarFactura) ?
                            Html::a('<span class="glyphicon glyphicon-saved"></span> Liquidar factura',
                                    Url::to(['liquidacion/index', 'proveedor_id' => $model->id]),
                                    ['class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Liquidar factura')]) . '&nbsp;' : '';
                    ?>
            </h4>
        </div>
    <?php endif; ?>
    <div class="row">
        <?php
        //   if ($providerPedido->totalCount) {
        $estados = ArrayHelper::map(ListItem::find()->where(['list' => 'estado_pedido'])->asArray()->all(), 'code', 'value');
        $gridColumnPedido = [
            ['attribute' => 'id', 'visible' => false],
            'codigo_pedido',
//            'codigo',
//            'fecha',
            [
                'attribute' => 'codigo',
                'label' => Yii::t('app', 'Fecha'),
            ],
            [
                'attribute' => 'estado',
                'label' => Yii::t('app', 'Estado'),
                'value' => function($model) use($estados) {
                    return $estados[$model->estado];
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => $estados,
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado', 'id' => 'grid-pedido-search-estado']
            ],
            'observacion',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {importC} {approve} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $url = Url::to(['pedido/view', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                    },
                    'update' => function ($url, $model) {
                        $url = Url::to(['pedido/update', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                    },
                    'importC' => function ($url, $model) {
                        $pedidoLog = $model->getLastLog($model->estado);
                        $url = Url::to(['pedido/import', 'id' => $pedidoLog->id]);
                        return Html::a('<span class="glyphicon glyphicon-import"></span>', $url, ['title' => Yii::t('yii', 'Importar cotización')]);
                    },
                    'approve' => function ($url, $model) {
                        $url = Url::to(['pedido/update', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Aprobar cotización')]);
                    },
                    'delete' => function ($url, $model) {
                        $url = Url::to(['pedido/delete', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                        ]);
                    },
                ],
                'visibleButtons' => [
//                        'view' => function ($model, $key, $index) {
//                            return $model->estado > 0?true:false;
//                         },
                    'update' => function ($model, $key, $index) {
                        return $model->estado == 0 ? true : false;
                    },
                    'importC' => function ($model, $key, $index) {
                        return $model->estado == 1 ? true : false;
                    },
                    'approve' => function ($model, $key, $index) {
                        $pedidoLog = $model->getLastLog($model->estado);
                        return $model->estado == 1 && $pedidoLog->pedidoProductoLogs ? true : false;
                    },
//                        'delete' => function ($model, $key, $index) {
//                            return $model->productos ? false : true;
//                        },
                ],
            ],
        ];
        echo Gridview::widget([
            'dataProvider' => $providerPedido,
            'filterModel' => $searchPedido,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido']],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '<span class="glyphicon glyphicon-shopping-cart"></span> ' . Html::encode(Yii::t('app', 'Pedidos')),
            ],
            // your toolbar can include the additional full export menu
            'toolbar' => [
                [
                    'content' =>
                    Html::a('<span class="glyphicon glyphicon-import"></span>',
                            Url::to(['pedido/import-pedido', 'proveedor_id' => $model->id]),
                            ['class' => 'btn btn-success', 'title' => Yii::t('yii', 'Importar Pedido ')])
                ],
                ['content' =>
                    Html::a('<span class="glyphicon glyphicon-plus"></span>',
                            Url::to(['pedido/create', 'proveedor_id' => $model->id]),
                            ['class' => 'btn btn-success', 'title' => Yii::t('yii', 'Nuevo Pedido ')])
                ],
                ['content' =>
                    Html::a('<span class="glyphicon glyphicon-eye-close"></span>',
                            Url::to(['reporte-productos-desactivados', 'proveedor_id' => $model->id]),
                            ['class' => 'btn btn-warning', 'title' => Yii::t('yii', 'Productos desactivados')])
                ],
                '{export}',
                ExportMenu::widget([
                    'dataProvider' => $providerPedido,
                    'columns' => $gridColumnPedido,
                    'target' => ExportMenu::TARGET_BLANK,
                    'fontAwesome' => true,
                    'dropdownOptions' => [
                        'label' => 'Full',
                        'class' => 'btn btn-default',
                        'itemsBefore' => [
                            '<li class="dropdown-header">Export All Data</li>',
                        ],
                    ],
                ]),
            ],
            //   'export' => false,
            'columns' => $gridColumnPedido
        ]);
        //     }
        ?>
    </div>

    <div class="row">
        <?php
        if ($providerLiquidacion->totalCount) {
            $gridColumnLiquidacion = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'label' => 'Pedidos',
                    'value' => function($model) {
                        return Liquidacion::getStrPedidos($model->id);
                    },
                ],
                [
                    'attribute' => 'valor_factura',
                    'value' => function($model) {
                        return number_format($model->valor_factura, 2);
                    },
                    'hAlign' => 'right'
                ],
                [
                    'attribute' => 'total_gastos',
                    'value' => function($model) {
                        return '$' . number_format($model->total_gastos);
                    },
                    'hAlign' => 'right'
                ],
                [
                    'attribute' => 'impuestos',
                    'value' => function($model) {
                        return '$' . number_format($model->impuestos);
                    },
                    'hAlign' => 'right'
                ],
                [
                    'attribute' => 'total_importacion',
                    'value' => function($model) {
                        return '$' . number_format($model->total_importacion);
                    },
                    'hAlign' => 'right'
                ],
                [
                    'attribute' => 'factor_importacion',
                    'hAlign' => 'right'
                ],
                [
                    'attribute' => 'factor_liquidacion',
                    'hAlign' => 'right'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {report} {delete} ',
                    'buttons' => ['view' => function ($url, $model) {
                            $url = Url::to(['liquidacion/view', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                        },
                        'update' => function ($url, $model) {
                            $url = Url::to(['liquidacion/update', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                        },
                        'report' => function ($url, $model) {
                            $url = Url::to(['liquidacion/report', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-list-alt"></span>', $url, ['title' => Yii::t('yii', 'Report')]);
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['delete', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'data-method' => 'post',
                            ]);
                        },
                    ],
                    'visibleButtons' => [
                        'delete' => false
                    ],
                ],
            ];
            echo Gridview::widget([
                'dataProvider' => $providerLiquidacion,
                'filterModel' => $searchLiquidacion,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido']],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<span class="glyphicon glyphicon-saved"></span> ' . Html::encode(Yii::t('app', 'Liquidaciones')),
                ],
                'export' => false,
                'columns' => $gridColumnLiquidacion
            ]);
        }
        ?>
    </div>
</div>
