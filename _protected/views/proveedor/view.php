<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proveedor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-view">
    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
       
            <?= Html::a('<span class="glyphicon glyphicon-import"></span> Importar productos proveedor', ['import-productos-proveedor', 'proveedor_id' => $model->id], ['class' => 'btn btn-success','title' => Yii::t('yii', 'Importar productos proveedor ')])?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </span>
    </h1>
    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
       // 'nombre',
        'nit',
        'contacto',
        'telefono',
        'direccion',
        'ciudad',
        'correo',
        'idioma',
        ['attribute' => 'tipo', 'value' => $model->tipo?'Internacional':'Nacional'],
        'tiempo_produccion',
        'tiempo_transporte',
        'frecuencia_pedido',
        'factor_pedido',
        'bo_aceptados',
        'columna_codigo',
        'columna_cantidad',
        'columna_precio',
        'columna_pedido_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
//if($providerPedido->totalCount){
//    $gridColumnPedido = [
//        ['class' => 'yii\grid\SerialColumn'],
//            ['attribute' => 'id', 'visible' => false],
//            'codigo',
//                        'fecha',
//            'estado',
//            'observacion',
//    ];
//    echo Gridview::widget([
//        'dataProvider' => $providerPedido,
//        'pjax' => true,
//        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-pedido']],
//        'panel' => [
//            'type' => GridView::TYPE_PRIMARY,
//            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Pedido')),
//        ],
//        'export' => false,
//        'columns' => $gridColumnPedido
//    ]);
//}
?>
    </div>
    
    <div class="row">
<?php
if($providerProveedorProducto->totalCount){
    $gridColumnProveedorProducto = [
        //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'codigo',
                'label' => Yii::t('app', 'Producto'),
                'value' => function ($model) {
                return $model->producto->codigo;}
            ],
            [
                'attribute' => 'estado',
                'label' => Yii::t('app', 'Estado'),
                'value' => function($model){
                     return $model->producto->estado===1?'Activo':'Inactivo';
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0 => 'Inactivo', 1 => 'Activo'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Estado']
            ],
                    
            [
               // 'attribute' => 'promedio_manual',
                'label' => Yii::t('app', 'Promedio'),
                'value' => function ($model) {
                return round($model->producto->promedio_manual);}
            ],
            'referencia_proveedor',
            'referencia_proveedor2',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProveedorProducto,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proveedor-producto']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Productos proveedor')),
        ],
        'export' => false,
        'columns' => $gridColumnProveedorProducto
    ]);
}
?>
    </div>
</div>
