<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\DetailView;

$this->registerJs(
        "$('#report').tableHeadFixer({'left' : 1});
        $('.precios').tooltip({html:true});
      //  $('#report').tableExport({formats: ['xlsx', 'csv', 'txt']});
        var btnDLtoExcel = $('#btn_exportar');
           btnDLtoExcel.on('click', function () {
               $('#report').excelexportjs({
                   containerid: 'report'
                   , datatype: 'table'
               });
                return false;
           });",
        View::POS_READY
);

$this->registerJsFile(
        '@web/js/tableHeadFixer.js', ['depends' => [JqueryAsset::className()]]
);

$this->registerJsFile(
        '@web/js/excelexportjs.js', ['depends' => [JqueryAsset::className()]]
);

$this->title = $model->codigo_pedido . ' (' . $model->proveedor->nombre . ')';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<h1><?= Yii::t('app', 'Pedido') . ' ' . Html::encode($this->title) ?>
    <span class="pull-right">
        <?php
        echo Html::a('<span class="glyphicon glyphicon-export"></span> Exportar', '', ['id' => 'btn_exportar', 'class' => 'btn btn-primary', 'title' => Yii::t('yii', 'Exportar')]);
        echo ' ' . Html::a(Yii::t('app', 'Atras'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']);
        ?>
    </span>
</h1>

<?php
$thead = '<table id="report" class="bg-white table-striped table-condensed table-hover table-bordered" >
  <thead>
    <tr>
        <th style= "text-align:center">Tipo Info</th>
        <th style= "text-align:center">Codigo</th>
        <th style= "text-align:center">Marca</th>
        <th style= "text-align:center">Referencia</th>
        <th style= "text-align:center">Ref proveedor</th>
        <th style= "text-align:center">Nombre</th>';
foreach ($fechas as $fecha) {
    $thead .= '<th style= "text-align:center"><nobr>' . $fecha . '</nobr></th>';
}
$thead .= '<th style= "text-align:center">Pr pedido</th>
      <th style= "text-align:center">Pr ventas</th>
      <th style= "text-align:center">Pr visual</th>
      <th style= "text-align:center">Meses</th>
      <th style= "text-align:center">Saldo</th>';

$thead .= '<th style= "text-align:center">UFCOMPRA</th>
        <th style= "text-align:center">UFVTA</th>
        <th style= "text-align:center">UPCOMPRA</th>
        ';


foreach ($bos as $bo) {
    $thead .= '<th style= "text-align:center">' . $bo . '</th>';
}
$thead .= '
      
      <th style= "text-align:center">FAC</th>
      <th style= "text-align:center">Precio1</th>
      <th style= "text-align:center">Precio2</th>
      <th style= "text-align:center">Precio4</th>
      <th style= "text-align:center">Competencia</th>
      <th style= "text-align:center">Estado</th>
      <th style= "text-align:center">Repetido</th>
      <th style= "text-align:center">Relacionado</th>
      <th style= "text-align:center">Cant P</th>';
$count = 0;
$countPLogs = count($model->pedidoLogs);
foreach ($model->pedidoLogs as $pedidoLog) {
    switch ($pedidoLog->estado) {
        case 0:
            break;
        case 1:
            $count++;
            $thead .= '<th style= "text-align:center" colspan="4">Cotizaci&oacute;n</th>';
            break;
        case 2:
            $thead .= '<th style= "text-align:center" colspan="4">Despacho</th>';
            $count++;
            break;
        case 3:
            $thead .= '<th style= "text-align:center" colspan="4">Factura</th>';
            $count++;
            break;
        default:
            break;
    }
}

for ($index = 0; $index < $count; $index++) {
    $thead .= '<th>cant</th><th>precio</th><th>cant</th><th>obs</th>';
}
$thead .= '</tr></thead>';
$table = '<tbody>';
$bottom = '';
$total_precio4 = 0;
$totales_precio = [];
$prdRela = [];
$prdRa = [];
$totalidadPrds = [];
$totalsP = [];
foreach ($productos as $producto) {
    $totalidadPrds[$producto["codigo"]] = $producto["codigo"];
    if (in_array($producto["codigo"], array("D94471", "D02110"))) {
//        echo "<pre>";
//        print_R($producto);
//        echo "</pre>";
    }
    if (strpos($producto['tipo'], 'PRODUCTO RELACIONADO') !== false) {
        $totalsP[$producto["codigo"] . "-CANT"] = @$totalsP[$producto["codigo"] . "-CANT"] + $producto["cantidad"];
        $totalsP[$producto["codigo"] . "-ESTD"] = @$totalsP[$producto["codigo"] . "-ESTD"] + $producto["estado_producto"];
        $prdRela[$producto['tipo']] = $producto['codigo'];
    }
    if ($producto['tipo'] == "PRODUCTO") {
        $prdRa[$producto["codigo"]] = $producto["codigo"];
    }
}
//echo "<pre>";
//print_R($totalsP);
//echo "</pre>";

foreach ($productos as $producto) {
    $control = 1;
    if ($producto["tipo"] != "PRODUCTO") {
        $xs = explode(":", $producto["tipo"]);
        if (!isset($prdRa[trim($xs[1])])) {
            $control = 0;
        }
    }

    $producto_id = $producto['producto_id'];
    $countLogs = $countPLogs;
    $total_precio4 += $producto['precio4'];
    $sxP = explode("-", $producto['producto_id']);
    $temp = '<tr>
      <td style= "text-align:left">' . $producto['tipo'] . '</td>
      <th scope="row">' . Html::a($producto['codigo'], ['producto/update', 'id' => $sxP[0]], ['target' => '_blank']) . '</th>
      <td style= "text-align:left">' . $producto['nombre_proveedor'] . '</td>
      <td style= "text-align:left">' . $producto['referencia'] . '</td>
      <td style= "text-align:left">' . $producto['referencia_proveedor'] . '</td>
      <td>' . $producto['nombre'] . '</td>';
    $sum_pedidos = 0;
    $sum_ventas = 0;
    $fac = ($producto['upcompra'] != 0) ? $producto['precio1'] != 0 ? (round(($producto['precio1'] / $producto['upcompra']), 2)) : (round(($producto['precio2'] / $producto['upcompra']), 2)) : '0';
    //ventas y pedidos por mes
    foreach ($fechas as $i => $fecha) {
        $pedidos = $producto[$fecha]['pedidos'] ? $producto[$fecha]['pedidos'] : 0;
        $ventas = $producto[$fecha]['ventas'] ? $producto[$fecha]['ventas'] : 0;
        $temp .= '<td style= "text-align:center">' . $ventas . '</td>';
        if ($i < 12) {
            $sum_ventas += $ventas;
            $sum_pedidos += $pedidos;
        }
    }
    //backorders
    $totalBo = 0;
    $tempBo = '';
    foreach ($bos as $bo) {
        $vbo = isset($producto[$bo]) ? $producto[$bo] : 0;
        $totalBo += $vbo;
        $tempBo .= '<td style= "text-align:right">' . $vbo . '</td>';
    }
    $promedio_ventas = $producto["promedio_manual"] ? $producto["promedio_manual"] : round(($sum_ventas / 12));
    $dias_inventario = $promedio_ventas != 0 ? (($producto['existencia'] + $totalBo) / $promedio_ventas) * 30 : 0;


    $relacionadOMostrarPre = explode(",", $producto['relacionado']);

    $relacionadOMostrar = "";
    foreach ($relacionadOMostrarPre as $rM) {
        if (!isset($prdRela["PRODUCTO RELACIONADO de: " . str_replace(" ", "", $rM)])) {
//            if (isset($prdRela["PRODUCTO RELACIONADO de: " . $producto['codigo']]) && $prdRela["PRODUCTO RELACIONADO de: " . $producto['codigo']] == str_replace(" ", "", $rM)) {
//            }
//            if ($producto["tipo"] == "PRODUCTO") {
//                if ($totalsP[str_replace(" "    , "", $rM) . "-ESTD"] != 0 || $totalsP[str_replace(" ", "", $rM) . "-CANT"] != 0) {
                if ($totalsP[str_replace(" "    , "", $rM) . "-ESTD"] != 0 ) {
                    $relacionadOMostrar .= $rM . ",";
                }
//            }
        }
    }
    $relacionadOMostrar = substr($relacionadOMostrar, 0, -1);

    $temp .= '<td style= "text-align:right">' . round(($sum_pedidos / 12), 1) . '</td>';
    $temp .= '<td style= "text-align:right">' . round(($sum_ventas / 12), 1) . '</td>
      <td style= "text-align:right">' . $producto['promedio_manual'] . '</td>
      <td style= "text-align:right" id = meses-' . $producto_id . '>' . round(($dias_inventario / 30), 1) . '</td>
      <td style= "text-align:right" id = existencia-' . $producto_id . '>' . $producto['existencia'] . '</td>
      <td>' . gmdate("Y-m-d", ($producto['ufcompra'] - 25569) * 86400) . '</td>
      <td>' . gmdate("Y-m-d", ($producto['ufvta'] - 25569) * 86400) . '</td>
      <td style= "text-align:right">' . $producto['upcompra'] . '</td>'
            . $tempBo
            . '<td style= "text-align:right"' . ($fac > 1.5 ? '>' : ' class = "bg-pink">') . $fac . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio1'], 0) . '</td>
      <td style= "text-align:right">' . '$' . number_format($producto['precio2'], 0) . '</td>
      <td style= "text-align:right">' . $producto['precio4'] . '</td>
      <td style= "text-align:right" class = "precios" title="' . @$precios[$producto['producto_id']]['todos'] . '">' . @$precios[$producto['producto_id']]['menor'] . '</td>
      <td>' . ( $producto['estado_producto'] ? 'Activo' : 'Inactivo' ) . '</td>
      <td>' . $producto['repetido'] . '</td>
      <td>' . $relacionadOMostrar . '</td>
      <td style= "text-align:right"' . ( $producto['cantidadP'] ? '>' : ' class = "bg-yellow">') . $producto['cantidadP'] . '</td>';
    foreach ($model->pedidoLogs as $pedidoLog) {
        if ($pedidoLog->estado > 1 && $pedidoLog->estado <= 4) {
            $flag = false;
            $totales_precio[$pedidoLog->id] = 0;
            $countProd = count($pedidoLog->pedidoProductoLogs);
            foreach ($pedidoLog->pedidoProductoLogs as $pedidoProductoLog) {
                if ($pedidoProductoLog->producto_id == $producto_id && $pedidoLog->estado) {
                    $totales_precio[$pedidoLog->id] += $pedidoProductoLog->precio;
                    $temp .= '<td style= "text-align:right"' . ($pedidoProductoLog->cantidad && $pedidoProductoLog->cantidad == $producto['cantidadP'] ? '>' : ' class = "bg-red">');
                    $temp .= $pedidoProductoLog->cantidad . '</td>';
                    $temp .= '<td style= "text-align:right"' . ($producto['precio4'] != $pedidoProductoLog->precio && $countProd > 0 && $pedidoLog->estado == 1 ? ' class = "bg-blue">' : '>');
                    $temp .= $pedidoProductoLog->precio . '</td>';
                    $temp .= '<td style= "text-align:right">' . $pedidoProductoLog->nueva_cantidad . '</td>';
                    $temp .= '<td>' . $pedidoProductoLog->observacion . '</td>';
                }
            }
            $countLogs--;
        }
    }
    $temp .= '</tr>';

    if ($producto["tipo"] != "PRODUCTO") {
        if ($producto['estado_producto'] != 0 && $producto["existencia"] != 0) {
            if ($control == 1) {
                $table .= $temp;
            }
        } else if ($totalsP[$producto["codigo"] . "-CANT"] != 0 && $totalsP[$producto["codigo"] . "-ESTD"] != 0) {
            if ($control == 1) {
                $table .= $temp;
            }
        }
    } else {
        if ($control == 1) {
            $table .= $temp;
        }
    }
//        $table .= $temp;
}
$table .= $bottom . '</tbody>
</table>';
$footer = '<tfoot>
    <tr>
      <th colspan="41">Totales</th>
      <th>' . $total_precio4 . '</th>
      <th colspan="3"></th>';
foreach ($totales_precio as $value) {
    $footer .= '<th>' . $value . '</th>
                <th colspan="4"></th>';
}
$footer .= '</tr>
        </tfoot>';
echo '<div class="report">' . $thead . $table . '</div>';
?>
<br>
<br>
<br>
<br>
<div class="row">
    <?php
    $gridColumn = [
        'codigo',
        'observacion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
    ?>
</div>