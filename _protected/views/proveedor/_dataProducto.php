<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->productos,
        'key' => function($model){
            return ['id' => $model->id, 'created_by' => $model->created_by];
        }
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        'precio1',
        'precio2',
        'precio4',
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
        'nombre_proveedor',
        [
                'attribute' => 'distribuidor.nombre',
                'label' => Yii::t('app', 'Distribuidor')
            ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'producto'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
