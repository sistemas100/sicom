<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

?>
<div class="producto-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->nombre) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        'precio1',
        'precio2',
        'precio4',
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
        [
            'attribute' => 'proveedor.nombre',
            'label' => Yii::t('app', 'Proveedor'),
        ],
        'nombre_proveedor',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>