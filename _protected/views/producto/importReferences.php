<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="producto-form">

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ;
        $model->fecha = new \yii\db\Expression('NOW()');?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <button>Importar Referencias</button>
    <br>
    <br>

    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
        <?= Html::activeHiddenInput($model, 'fecha') ?>
    </div>
<?php ActiveForm::end() ?>
</div>
