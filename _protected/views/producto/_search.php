<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-producto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true, 'placeholder' => 'Codigo']) ?>

    <?= $form->field($model, 'referencia')->textInput(['maxlength' => true, 'placeholder' => 'Referencia']) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'precio1')->textInput(['maxlength' => true, 'placeholder' => 'Precio1']) ?>

    <?php /* echo $form->field($model, 'precio2')->textInput(['maxlength' => true, 'placeholder' => 'Precio2']) */ ?>

    <?php /* echo $form->field($model, 'precio4')->textInput(['maxlength' => true, 'placeholder' => 'Precio4']) */ ?>

    <?php /* echo $form->field($model, 'existencia')->textInput(['placeholder' => 'Existencia']) */ ?>

    <?php /* echo $form->field($model, 'ctoprm')->textInput(['maxlength' => true, 'placeholder' => 'Ctoprm']) */ ?>

    <?php /* echo $form->field($model, 'upcompra')->textInput(['maxlength' => true, 'placeholder' => 'Upcompra']) */ ?>

    <?php /* echo $form->field($model, 'ufcompra')->textInput(['maxlength' => true, 'placeholder' => 'Ufcompra']) */ ?>

    <?php /* echo $form->field($model, 'ufvta')->textInput(['maxlength' => true, 'placeholder' => 'Ufvta']) */ ?>

    <?php /* echo $form->field($model, 'proveedor_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
        'options' => ['placeholder' => Yii::t('app', 'Choose Proveedor')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'nombre_proveedor')->textInput(['maxlength' => true, 'placeholder' => 'Nombre Proveedor']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
