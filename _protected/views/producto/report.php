<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoDistribuidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

$this->title = Yii::t('app', 'Competencia');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
?>
<h1><?= Html::encode($this->title) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <span class="pull-right">
        <div onclick='exporteExcel("ExportarTabla")' class='btn btn-success'>Exportar</div>
    </span>
</h1>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>
    function consultar() {
        window.location = window.location.origin + window.location.pathname + '?codigo=' + $('#codigo').val();
    }
    
    function exporteExcel(idTabla)
        {
            var tab_text = '<table border="1px" style="font-size:20px" ">';
            var textRange;
            var j = 0;
            var tab = document.getElementById(idTabla); // id of table
            var lines = tab.rows.length;

            // the first headline of the table
            if (lines > 0) {
                tab_text = tab_text + '<tr bgcolor="#DFDFDF">' + tab.rows[0].innerHTML + '</tr>';
            }

            // table data lines, loop starting from 1
            for (j = 1; j < lines; j++) {
                tab_text = tab_text + "<tr>" + tab.rows[j].innerHTML + "</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");             //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, "");                 // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, "");    // reomves input params
            // console.log(tab_text); // aktivate so see the result (press F12 in browser)

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            // if Internet Explorer
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "ExporteCuartil.xls");
            } else // other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text).replace(/[!'()]/g, escape).replace(/\*/g, "%2A").replace(/%(?:7C|60|5E)/g, unescape));

            return (sa);
        }

</script>

<div class="producto-distribuidor-index" style="overflow-x: hidden">
    <div>
        <?php
        $pre = "";
        if (isset($_GET["codigo"])) {
            $pre = $_GET["codigo"];
        }
        ?>
        <input id="codigo" value="<?php echo $pre ?>">
        <div class="btn btn-success" onclick="consultar();">Consultar</div>
    </div>
    <br/>

    <?php
    // Segundo buscador
    $items = array();
    $marcas = array();
    $preMarcas = array();
    if (isset($otros)) {
        foreach ($otros as $p) {
            $items[$p["codigo"]] = array("COD" => $p["codigo"], "REF" => $p["referencia"], "NOMBRE" => $p["nombre"], "MARC" => $p["Marca"], "PRECIO" => $p["precio"]);
            $preMarcas[$p["NombreR"]] = $p["NombreR"];
            $marcas[$p["codigo"]][$p["NombreR"]][] = array("REF" => $p["RefR"], "NOM" => $p["MarcaR"], "PRE" => $p["PrecioR"], "ACT"=>$p["Fecha"]);
        }
    }
//    echo "<pre>";
//    print_R($marcas);
//    echo "</pre>";
//    die;
    ?>
    <div style="overflow-x: auto">
        <table class="kv-grid-table table table-bordered table-striped kv-table-wrap" id='ExportarTabla'>
            <tr>
                <th>Código</th>
                <th>Referencia</th>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Precio</th>
                <?php
                foreach ($preMarcas as $m => $MARCA) {
                    ?>
                    <td><?php echo $m ?></td>
                    <?php
                }
                ?>
            </tr>
            <?php
            foreach ($items as $p => $X) {
                ?>
                <tr>
                    <td><?php echo $X["COD"]; ?></td>
                    <td><?php echo $X["REF"]; ?></td>
                    <td><?php echo $X["NOMBRE"]; ?></td>
                    <td><?php echo $X["MARC"]; ?></td>
                    <td><?php echo $X["PRECIO"]; ?></td>
                    <?php
                    foreach ($preMarcas as $MARCA) {
                        if (isset($marcas[$X["COD"]][$MARCA])) {
                            ?>
                            <td>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>REFERENCIA</th>
                                        <th>MARCA</th>
                                        <th>PRECIO</th>
                                        <th>FECHA <br/>ACTUALIZADO</th>
                                    </tr>
                                    <?php
                                    foreach ($marcas[$X["COD"]][$MARCA] as $mar) {
                                        ?>
                                        <tr>
                                            <td><?php echo $mar["REF"] ?></td>
                                            <td><?php echo $mar["NOM"] ?></td>
                                            <td><?php echo $mar["PRE"] ?></td>
                                            <td><?php echo substr($mar["ACT"],0,10) ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td></td>
                            <?php
                        }
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

</div>
