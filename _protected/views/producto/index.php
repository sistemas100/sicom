<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Producto');
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="producto-index" style="overflow-x: hidden">

    <h1><?= Html::encode($this->title) ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
        <span class="pull-right">
            <?php //echo Html::a(Yii::t('app', 'Create Producto'), ['create'], ['class' => 'btn btn-success'])  ?>
            <?php //echo Html::a(Yii::t('app', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
            <?=
            Html::a('<i class="fa glyphicon glyphicon-upload"></i> ' . Yii::t('app', 'Importar Relacionados'), ['producto/import-relacionados'], ['class' => 'btn btn-success',
                'title' => Yii::t('app', 'Importar Relacionados')])
            ?>
            <?=
            Html::a('<i class="fa glyphicon glyphicon-upload"></i> ' . Yii::t('app', 'Importar Consolidado'), ['consolidado-ventas/import'], ['class' => 'btn btn-success',
                'title' => Yii::t('app', 'Importar Consolidado')])
            ?>
            <?=
            Html::a('<i class="fa glyphicon glyphicon-upload"></i> ' . Yii::t('app', 'Import Megaexplorer'), ['producto/import'], ['class' => 'btn btn-success',
                'title' => Yii::t('app', 'Import Megaexplorer')])
            ?>
            <?=
            Html::a('<i class="fa glyphicon glyphicon-upload"></i> ' . Yii::t('app', 'Import References'), ['producto/import-references'], ['class' => 'btn btn-success',
                'title' => Yii::t('app', 'Import References')])
            ?>
        </span>
    </h1>

    <div class="search-form" style="display:none">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        [
            'attribute' => 'precio1',
            'value' => function($model) {
                return '$' . number_format($model->precio1, 0);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'precio2',
            'value' => function($model) {
                return '$' . number_format($model->precio2, 0);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'precio4',
            'value' => function($model) {
                return '$' . number_format($model->precio4, 0);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'existencia',
            'hAlign' => 'right'
        ],
       
        [
            'attribute' => 'ctoprm',
            'value' => function($model) {
                return '$' . number_format($model->ctoprm, 0);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'upcompra',
            'value' => function($model) {
                return '$' . number_format($model->upcompra, 0);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'ufcompra',
            'value' => function($model) {
                $UNIX_DATE = ($model->ufcompra - 25569) * 86400;
                return gmdate("Y-m-d", $UNIX_DATE);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'ufvta',
            'value' => function($model) {
                $UNIX_DATE = ($model->ufvta - 25569) * 86400;
                return gmdate("Y-m-d", $UNIX_DATE);
            },
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'duracion',
            'label' => 'Duración',
            'value' => function($model) {
                return ($model->ufvta - $model->ufcompra);
            },
            'hAlign' => 'right'
        ],
//        [
//                'attribute' => 'proveedor_id',
//                'label' => Yii::t('app', 'Proveedor'),
//                'value' => function($model){
//                    return isset($model->proveedor->nombre)?$model->proveedor->nombre:'';
//                },
//                'filterType' => GridView::FILTER_SELECT2,
//                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->asArray()->all(), 'id', 'nombre'),
//                'filterWidgetOptions' => [
//                    'pluginOptions' => ['allowClear' => true],
//                ],
//                'filterInputOptions' => ['placeholder' => 'Proveedor', 'id' => 'grid-producto-search-proveedor_id']
//            ],
        'nombre_proveedor',
        [
            'attribute' => 'promedio_manual',
            'label' => Yii::t('app', 'Promedio'),
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'tipo',
            'label' => Yii::t('app', 'Tipo'),
        ],
        [
            'attribute' => 'bo',
            'label' => Yii::t('app', 'BO'),
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'meses',
            'label' => Yii::t('app', 'Meses'),
            'hAlign' => 'right'
        ],
        [
            'attribute' => 'bajo_stock',
            'label' => Yii::t('app', 'Bajo stock'),
            'value' => function($model) {
                return $model->bajo_stock === '1' ? 'Si ' : ($model->bajo_stock === '0'?'No':'');
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [0 => 'No', 1 => 'Si'],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => '']
        ],
        [
            'attribute' => 'estado',
            'value' => function($model) {
                return $model->estado === 1 ? 'Activo' : 'Inactivo';
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [0 => 'Inactivo', 1 => 'Activo'],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Estado']
        ],
//        [
//            'attribute' => 'tiempo_produccion',
//            'hAlign' => 'right'
//        ],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ];
    ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-producto']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-th"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]),
        ],
    ]);
    ?>

</div>
