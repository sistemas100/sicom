<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producto'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Producto').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        'precio1',
        'precio2',
        'precio4',
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
        [
                'attribute' => 'proveedor.nombre',
                'label' => Yii::t('app', 'Proveedor')
            ],
        'nombre_proveedor',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerReferencia->totalCount){
    $gridColumnReferencia = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'referencia',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerReferencia,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('app', 'Referencia')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnReferencia
    ]);
}
?>
    </div>
</div>
