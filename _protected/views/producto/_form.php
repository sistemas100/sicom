<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,  
    'viewParams' => [ 
        'class' => 'ProveedorProducto',  
        'relID' => 'proveedor-producto',  
        'value' => \yii\helpers\Json::encode($model->proveedorProductos), 
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0 
    ] 
]);

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Referencia', 
        'relID' => 'referencia', 
        'value' => \yii\helpers\Json::encode($model->referencias),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

?>

<div class="producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true, 'placeholder' => 'Codigo']) ?>

    <?= $form->field($model, 'referencia')->textInput(['maxlength' => true, 'placeholder' => 'Referencia']) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'precio1')->textInput(['maxlength' => true, 'placeholder' => 'Precio1']) ?>

    <?= $form->field($model, 'precio2')->textInput(['maxlength' => true, 'placeholder' => 'Precio2']) ?>

    <?= $form->field($model, 'precio4')->textInput(['maxlength' => true, 'placeholder' => 'Precio4']) ?>

    <?= $form->field($model, 'existencia')->textInput(['placeholder' => 'Existencia']) ?>

    <?= $form->field($model, 'ctoprm')->textInput(['maxlength' => true, 'placeholder' => 'Ctoprm']) ?>

    <?= $form->field($model, 'upcompra')->textInput(['maxlength' => true, 'placeholder' => 'Upcompra']) ?>

    <?= $form->field($model, 'ufcompra')->textInput(['maxlength' => true, 'placeholder' => 'Ufcompra']) ?>

    <?= $form->field($model, 'ufvta')->textInput(['maxlength' => true, 'placeholder' => 'Ufvta']) ?>
    
    <?php 
//         $form->field($model, 'proveedor_id')->widget(\kartik\widgets\Select2::classname(), [
//        'data' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
//        'options' => ['placeholder' => Yii::t('app', 'Choose Proveedor')],
//        'pluginOptions' => [
//            'allowClear' => true
//        ],
//    ]); ?>

    <?= $form->field($model, 'nombre_proveedor')->textInput(['maxlength' => true, 'placeholder' => 'Nombre Proveedor']) ?>
    
    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true, 'placeholder' => 'Tipo']) ?>
    
    <?= $form->field($model, 'promedio_manual')->textInput(['maxlength' => true, 'placeholder' => 'Promedio Manual']) ?> 

     <?= $form->field($model, 'estado')->widget(\kartik\widgets\Select2::classname(), [
        'data' => [1=>'Activo',0=>'Inactivo'],
        'options' => ['placeholder' => Yii::t('app', 'Seleccione Estado')],
    ]); ?>
    <?= $form->field($model, 'tiempo_produccion')->textInput(['maxlength' => true, 'placeholder' => 'Tiempo Produccion']) ?>
    <?php
    $forms = [
        [ 
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'ProveedorProducto')), 
            'content' => $this->render('_formProveedorProducto', [ 
                'row' => \yii\helpers\ArrayHelper::toArray($model->proveedorProductos), 
            ]), 
        ], 
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('app', 'Referencia')),
            'content' => $this->render('_formReferencia', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->referencias),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
