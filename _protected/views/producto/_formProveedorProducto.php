<div class="form-group" id="add-proveedor-producto">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ProveedorProducto',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'proveedor_id' => [
            'label' => 'Proveedor',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
                'options' => ['placeholder' => Yii::t('app', 'Choose Proveedor')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
//        'estado' => [
//            'type' => TabularForm::INPUT_WIDGET,
//            'widgetClass' => \kartik\widgets\Select2::className(),
//            'options' => [
//                'data' => [1=>'Activo',0=>'Inactivo'],
//                'options' => ['placeholder' => Yii::t('app', 'Seleccione Estado')],
//            ],
//        ],
        
        'referencia_proveedor' => ['type' => TabularForm::INPUT_TEXT],
        'referencia_proveedor2' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('app', 'Delete'), 'onClick' => 'delRowProveedorProducto(' . $key . '); return false;', 'id' => 'proveedor-producto-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('app', 'Add Proveedor Producto'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowProveedorProducto()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

