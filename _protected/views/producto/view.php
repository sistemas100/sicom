<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Producto'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Producto').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('app', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('app', 'Will open the generated PDF file in a new window')
                ]
            )?>
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'referencia',
        'nombre',
        [
            'attribute' => 'precio1', 
            'value' => '$' . number_format($model->precio1,0),
            'hAlign'=>'right'
        ],
        [
            'attribute' => 'precio2', 
            'value' => '$' . number_format($model->precio2,0)
        ],
        [
            'attribute' => 'precio4', 
            'value' => '$' . number_format($model->precio4,0)
        ],
        'existencia',
        'ctoprm',
        'upcompra',
        'ufcompra',
        'ufvta',
//        [
//            'attribute' => 'proveedor.nombre',
//            'label' => Yii::t('app', 'Proveedor'),
//        ],
        'nombre_proveedor',
        'tipo',
        'promedio_manual', 
        [
            'attribute' => 'estado', 
            'value' => $model->estado === 1?'Activo':'Inactivo'
        ],
        'tiempo_produccion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
        
<?php
if($providerProveedorProducto->totalCount){ 
    $gridColumnProveedorProducto = [ 
            [ 
                'attribute' => 'proveedor.nombre', 
                'label' => Yii::t('app', 'Proveedor'),
            ], 
//                        'estado', 
            'referencia_proveedor', 
            'referencia_proveedor2', 
    ]; 
    echo Gridview::widget([ 
        'dataProvider' => $providerProveedorProducto, 
        'pjax' => true, 
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proveedor-producto']], 
        'panel' => [ 
            'type' => GridView::TYPE_PRIMARY, 
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Proveedor Producto')), 
        ], 
        'columns' => $gridColumnProveedorProducto 
    ]); 
 } 
 ?> 
    </div> 

    <div class="row"> 
 <?php 

if($providerReferencia->totalCount){
    $gridColumnReferencia = [
            ['attribute' => 'id', 'visible' => false],
            'referencia',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerReferencia,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-referencia']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Referencias')),
        ],
        'columns' => $gridColumnReferencia
    ]);
}
?>
    </div>
</div>
