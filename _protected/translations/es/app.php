<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    ' your account could not be activated, please contact us!' => ' su cuenta no pude ser activada, por favor contacte al administrador',
    'Add Producto' => 'Agregar producto',
    'Add Producto Distribuidor' => 'Agregar Producto Distribuidor',
    'Add Referencia' => 'Agregar Referencia',
    'Advance Search' => 'Busqueda Avanzada',
    'Are you sure you want to delete this item?' => 'Está seguro que quiere eliminar este registro?',
    'Change password ( if you want )' => 'Cambie la contraseña(si lo desea)',
    'Choose Distribuidor' => 'Selecione Distribuidor',
    'Choose Fecha' => 'Seleccione Fecha',
    'Choose Producto' => 'Seleccione Producto',
    'Choose Proveedor' => 'Seleccione Proveedor',
    'Ciudad' => 'Ciudad',
    'Codigo' => 'Código',
    'Columna Codigo' => 'Columna Codigo',
    'Columna Marca' => 'Columna Marca',
    'Columna Nombre' => 'Columna Nombre',
    'Columna Precio' => 'Columna Precio',
    'Columna Referencia' => 'Columna Referencia',
    'Competencia' => 'Competencia',
    'Contacto' => 'Contacto',
    'Create Distribuidor' => 'Nuevo Distribuidor',
    'Create Producto' => 'Nuevo Producto',
    'Create Producto Distribuidor' => 'Nuevo Producto Distribuidor',
    'Create Proveedor' => 'Nuevo Proveedor',
    'Create Pedido' => 'Nuevo Pedido',
    'Create Referencia' => 'Nueva Referencia',
    'Create password' => 'Contraseña',
    'Create username' => 'Usuario',
    'Create your password' => 'contraseña',
    'Create your username' => 'Usuario',
    'Ctoprm' => 'Ctoprm',
    'Data is already expired' => 'Los datos ya han caducado',
    'Descuento' => 'Descuento',
    'Direccion' => 'Dirección',
    'Distribuidor' => 'Distribuidor',
    'Distribuidor ID' => 'Distribuidor ID',
    'Distribuidores' => 'Distribuidores',
    'Email' => 'Email',
    'Enter e-mail' => 'Email',
    'Enter new password' => 'Ingrese Nueva Contraseña',
    'Enter the subject' => '',
    'Enter verification code' => 'Ingrese código de verificación',
    'Enter your e-mail' => 'Ingrese su email',
    'Enter your name' => 'Ingrese su nombre',
    'Enter your password' => 'Ingrese su contraseña',
    'Enter your username' => 'Ingrese su usuario',
    'Existencia' => 'Existencia',
    'Fecha' => 'Fecha',
    'ID' => 'ID',
    'Import' => 'Importar',
    'Import Megaexplorer' => 'Importar Megaexplorer',
    'Import References' => 'Importar Referencias',
    'It\'s impossible to have that username.' => 'No es posible tener ese nombre de usuario',
    'Marca' => 'Marca',
    'Nit' => 'NIT',
    'Nombre' => 'Nombre',
    'Nombre Proveedor' => 'Nombre Proveedor',
    'PDF' => 'PDF',
    'Please try to log in.' => 'Intente Iniciar Sesión',
    'Precio' => 'Precio',
    'Precio1' => 'Precio1',
    'Precio2' => 'Precio2',
    'Precio4' => 'Precio4',
    'Precios' => 'Precios',
    'Producto' => 'Producto',
    'Producto Distribuidor' => 'Producto Distribuidor',
    'Producto ID' => 'Producto ID',
    'Producto id' => 'Producto ID',
    'Productos' => 'Productos',
    'Proveedor' => 'Proveedor',
    'Proveedor ID' => 'Proveedor ID',
    'Proveedores' => 'Proveedores',
    'Referencia' => 'Referencia',
    'Referencias' => 'Referencias',
    'Reset' => 'Reset',
    'Search' => 'Buscar',
    'Telefono' => 'Teléfono',
    'The requested page does not exist.' => 'La página solicitada no existe',
    'There was some error while deleting user role.' => 'Hubo un error al eliminar el rol',
    'There was some error while saving user role.' => 'Hubo un error al guardar el rol',
    'There was some error while sending email.' => 'Hubo un error al enviar el correo',
    'This email address has already been taken.' => 'El correo ingresado ya existe',
    'This username has already been taken.' => 'El nombre de usuario ingresado ya existe',
    'To be able to log in, you need to confirm your registration. 
                Please check your email, we have sent you a message.' => 'Debes confirmar tu registro.
                                                        Por favor reisa tu correo, tehemos enviado un mensaje',
    'Ufcompra' => 'Ufcompra',
    'Ufvta' => 'Ufvta',
    'Upcompra' => 'Upcompra',
    'Update {modelClass}: ' => 'Actualizar {modelClass}:',
    'We could not delete this user.' => 'No se pudo eliminar el usuario',
    'Will open the generated PDF file in a new window' => 'El archivo PDF se abrira en una nueva ventana',
    'You have successfuly deleted user and his role.' => 'El usuario su rol se han eliminado.',
    'Archived' => '@@Archivado@@',
    'Are you sure you want to delete this article?' => '@@Confirma que deseas borrar el artículo@@',
    'Articles' => '@@Noticias@@',
    'Author' => '@@Autor@@',
    'Category' => '@@Categoria@@',
    'Content' => '@@Contenido@@',
    'Create Article' => '@@Nueva Noticia@@',
    'Draft' => '@@Borrador@@',
    'Economy' => '@@Economía@@',
    'Home' => '@@Inicio@@',
    'Menu' => '@@Menú@@',
    'My Company' => '@@Mi empresa@@',
    'New pwd ( if you want to change it )' => '@@Nueva contraseña ( si quieres cambiarla )@@',
    'Published' => '@@Publicado@@',
    'Published on' => '@@Publicado el@@',
    'Read more' => '@@Leer más@@',
    'Register' => '@@Registro@@',
    'Society' => '@@Sociedad@@',
    'Sport' => '@@Deporte@@',
    'Summary' => '@@Sumario@@',
    'The best news available' => '@@Las últimas noticias@@',
    'There was an error sending email.' => '@@Ha ocurrido un error enviando el correo.@@',
    'Title' => '@@Título@@',
    'To be able to log in, you need to confirm your registration. Please check your email, we have sent you a message.' => '@@Para poder entrar en el sistema necesitas confirmar tu registro. Comprueba tu correo, te hemos enviado un mensaje@@',
    'We haven\'t created any articles yet.' => '@@No hay artículos aún.@@',
    'You are not allowed to access this page.' => '@@No estás autorizado a ver esta página@@',
    'articles' => '@@artículos@@',
    'news' => '@@noticias@@',
    'your account could not be activated, please contact us!' => '@@No hemos podido activa tu cuenta. Por favor contacta con nosotros.@@',
    'A link to reset password will be sent to your email.' => 'Se enviará un enlace para cambiar tu contraseña a tu correo electrónico.',
    'About' => 'Quiénes somos',
    'Account activation token cannot be blank.' => 'El token de activación de cuenta no puede estar vacío.',
    'Are you sure you want to delete this user?' => 'Confirma que deseas borrar el usuario',
    'Back' => 'Atrás',
    'Cancel' => 'Cancelar',
    'Check your email for further instructions.' => 'Comprueba tu correo y sigue las instrucciones.',
    'Contact' => 'Contacto',
    'Create' => 'Crear',
    'Create User' => 'Nuevo usuario',
    'Created At' => 'Creado',
    'Delete' => 'Borrar',
    'Hello' => 'Hola',
    'If you forgot your password you can' => 'Si has olvidado tu contraseña, puedes ',
    'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.' => 'Si tienes cualquier pregunta, rellena el siguiente formulario para ponerte en contacto con nosotros.',
    'Login' => 'Iniciar sesión',
    'Logout' => 'Desconectar',
    'Name' => 'Nombre',
    'New password was saved.' => 'Se ha guardado la nueva contraseña.',
    'Password' => 'Contraseña',
    'Password reset token cannot be blank.' => 'El token de contraseña no puede estar vacío.',
    'Please choose your new password:' => 'Por favor, elige una nueva contraseña:',
    'Please contact us if you think this is a server error. Thank you.' => 'Por favor, contacta con nosotros si crees que es un error del servidor. Gracias.',
    'Please fill out the following fields to login:' => 'Por favor, rellena los siguiente campos para iniciar sesión:',
    'Please fill out the following fields to signup:' => 'Rellena los siguientes campos para registrarte:',
    'Please fill out your email.' => 'Por favor, introduce tu correo electrónico.',
    'Remember me' => 'Recordarme',
    'Request password reset' => 'Solicitar cambio de contraseña',
    'Reset password' => 'Pedir nueva contraseña',
    'Role' => 'Rol',
    'Save' => 'Guardar',
    'Send' => 'Enviar',
    'Signup' => 'Registro',
    'Sorry, we are unable to reset password for email provided.' => 'Lo siento, no se ha podido cambiar la contraseña para ese correo electrónico.',
    'Status' => 'Estado',
    'Subject' => 'Asunto',
    'Submit' => 'Enviar',
    'Success! You can now log in.' => '¡Cuenta activada! Ahora puedes iniciar sesión con tu usuario y contraseña.',
    'Text' => 'Texto',
    'Thank you' => 'Gracias',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Gracias por contactarnos. Te responderemos lo antes posible.',
    'The above error occurred while the Web server was processing your request.' => 'Ha ocurrido un error mientras el servidor procesaba tu petición.',
    'Update' => 'Editar',
    'Update User' => 'Editar usuario',
    'Updated At' => 'Modificado',
    'Username' => 'Nombre de usuario',
    'Users' => 'Usuarios',
    'Verification Code' => 'Verificación',
    'We couldn\'t send you account activation email, please contact us.' => 'No ha sido posible enviarte el correo de activación. Por favor, contacta con nosotros.',
    'We couldn\'t sign you up, please contact us.' => 'El registro no se ha podido completar, por favor contacta con nosotros.',
    'We will send you an email with account activation link.' => 'Te enviaremos un enlace a tu correo para activar tu cuenta.',
    'Wrong account activation token.' => 'Token erróneo o inválido.',
    'Wrong password reset token.' => 'Token erróneo o inválido.',
    'You have to activate your account first. Please check your email.' => 'Debes activar tu cuenta, por favor revisa tu correo.',
    'for joining us!' => 'por participar en nuestro sitio.',
    'reset it' => 'cambiarla',
    'Export' => 'Exportar',
    'Accept' => 'Aceptar',
    'Resend' => 'Reenviar',
    'Reject' => 'Rechazar',
];
