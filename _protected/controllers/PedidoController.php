<?php

namespace app\controllers;

use Akeneo\Component\SpreadsheetParser\SpreadsheetParser;
use app\models\FormatoCorreo;
use app\models\Liquidacion;
use app\models\Pedido;
use app\models\PedidoLog;
use app\models\PedidoProducto;
use app\models\PedidoProductoLog;
use app\models\PedidoSearch;
use app\models\Producto;
use app\models\Proveedor;
use app\models\ProveedorProducto;
use app\models\UploadForm;
use kartik\mpdf\Pdf;
use PHPExcel;
use PHPExcel_IOFactory;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * PedidoController implements the CRUD actions for Pedido model.
 */
class PedidoController extends AppController {

    /**
     * Lists all Pedido models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PedidoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pedido model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        return $this->render('view', Proveedor::getInfoProductos($model));
    }

    /**
     * Creates a new Pedido model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($proveedor_id) {
        $model = new Pedido(['proveedor_id' => $proveedor_id, 'estado' => 0, 'created_at' => date("Y-m-01 H:i:s")]);
        $post = [];
        If (Yii::$app->request->isPost) {
            //filtramos los productos seleccionados
            $post = Yii::$app->request->post();
            $productos = isset($post['PedidoProducto']) ? $post['PedidoProducto'] : '';
            $promedios = [];
            $productosSel = [];
            foreach ($productos as $key => $producto) {
                if (isset($producto['promedio_manual']) && isset($producto['estado_producto'])) {
                    $promedios[] = [$key, $producto['promedio_manual'], $producto['estado_producto'], new Expression('NOW()'), Yii::$app->user->id];
                }
                if (isset($producto['estado'])) {
                    $productosSel[] = $producto;
                }
            }
            $post['PedidoProducto'] = $productosSel;
            if (!$productosSel) {
                Yii::$app->getSession()->setFlash('error', 'No se seleccion&oacute; ning&uacute;n producto');
            }
            $model->codigo_pedido = Proveedor::getCodigoPedido($proveedor_id);
        }
        if (isset($post['PedidoProducto']) && $post['PedidoProducto'] && $model->loadAll($post, ['nombreEstado']) && $model->saveAll(['nombreEstado'])) {
            //Guardamos los promedios de los productos
            Producto::batchUpdate(Producto::tableName(), ['id', 'promedio_manual', 'estado', 'updated_at', 'updated_by'], $promedios, ['promedio_manual', 'estado', 'updated_at', 'updated_by'])->execute();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', Proveedor::getInfoProductos($model));
        }
    }

    /**
     * Updates an existing Pedido model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        ini_set('max_input_vars', 3000);

        $model = $this->findModel($id);
        $estadoPrevio = $model->estado;
        If (Yii::$app->request->isPost) {
            // Filtramos los productos seleccionados,Actualizamos el borrador
            $post = Yii::$app->request->post();
            $productos = isset($post['PedidoProducto']) ? $post['PedidoProducto'] : [];
            $promedios = [];
            foreach ($productos as $key => $producto) {
                if (isset($producto['promedio_manual'])) {
                    $promedios[] = [$key, $producto['promedio_manual'], new Expression('NOW()'), Yii::$app->user->id];
                    unset($producto['promedio_manual']);
                }
            }
            if ($promedios) {
                //Guardamos los promedios de los productos
                Producto::batchUpdate(Producto::tableName(), ['id', 'promedio_manual', 'updated_at', 'updated_by'], $promedios, ['promedio_manual', 'updated_at', 'updated_by'])->execute();
            }
            if (isset(Yii::$app->request->post()['save']) && $model->estado == 0) {
                $productosSel = [];
                foreach ($productos as $producto) {
                    if (isset($producto['estado'])) {
                        $productosSel[] = $producto;
                    }
                }
                $post['PedidoProducto'] = $productosSel;
                if (!$productosSel) {
                    Yii::$app->getSession()->setFlash('error', 'No se seleccion&oacute; ning&uacute;n producto');
                }
                if (isset($post['PedidoProducto']) && $post['PedidoProducto'] && $model->loadAll($post, ['nombreEstado']) && $model->saveAll(['nombreEstado'])) {

                    Yii::$app->getSession()->setFlash('success', 'Se guardaron los cambios');
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }

            If (isset(Yii::$app->request->post()['send']) || isset(Yii::$app->request->post()['accept'])) {
                $model->estado = $model->estado + 1;
            }
            
            $modelPedidoLog = new PedidoLog(['pedido_id' => $model->id, 'estado' => $model->estado]);
//            echo "<pre>";
//            print_R($_POST);
//            print_R(Yii::$app->request->isPost);
//            print_R($model->attributes);
//            print_R($modelPedidoLog->attributes);
//            echo "</pre>";
//            die;
            if ($modelPedidoLog->save() && $model->save()) {
                if (isset(Yii::$app->request->post()['PedidoProductoLog'])) {
                    $arrPedidoProdLog = [];
                    foreach (Yii::$app->request->post()['PedidoProductoLog'] as $key => $producto) {
                        $arrPedidoProdLog[] = [$producto['pedido_log_id'], $key,
                            isset($producto['nueva_cantidad']) ? $producto['nueva_cantidad'] : '',
                            isset($producto['observacion']) ? $producto['observacion'] : '',
                            new Expression('NOW()'), Yii::$app->user->id];
                    }
                    //guardamos las observaciones en el log
                    Producto::batchUpdate(PedidoProductoLog::tableName(), ['pedido_log_id', 'producto_id', 'nueva_cantidad', 'observacion', 'updated_at', 'updated_by'], $arrPedidoProdLog, ['nueva_cantidad', 'observacion', 'updated_at', 'updated_by'])->execute();
                }
                //eliminamos los productos por pedido e insertamos los productos
                $arrPedidoProd = [];
                if (isset(Yii::$app->request->post()['PedidoProducto']) && $estadoPrevio == 0) {
                    foreach (Yii::$app->request->post()['PedidoProducto'] as $key => $producto) {
                        if (isset($producto['estado'])) {
                            $arrPedidoProd[] = [$key, $id, 1, $producto['cantidad'], new Expression('NOW()'), Yii::$app->user->id];
                        }
                    }
                    if ($arrPedidoProd) {
                        PedidoProducto::deleteAll('pedido_id = :id', ['id' => $id]);
                        Producto::batchUpdate(PedidoProducto::tableName(), ['producto_id', 'pedido_id', 'estado', 'cantidad', 'updated_at', 'updated_by'], $arrPedidoProd, ['estado', 'cantidad', 'updated_at', 'updated_by'])->execute();
                    }
                } elseif (isset(Yii::$app->request->post()['PedidoProducto'])) {
                    foreach (Yii::$app->request->post()['PedidoProducto'] as $key => $producto) {
                        if (isset($producto['estado'])) {
                            $arrPedidoProd[] = [$key, $id, 1, new Expression('NOW()'), Yii::$app->user->id];
                        }
                    }
                    if ($arrPedidoProd) {
                        PedidoProducto::updateAll(['estado' => 0], 'pedido_id = :id', ['id' => $id]);
                        Producto::batchUpdate(PedidoProducto::tableName(), ['producto_id', 'pedido_id', 'estado', 'updated_at', 'updated_by'], $arrPedidoProd, ['estado', 'updated_at', 'updated_by'])->execute();
                    }
                }
                //Enviamos correo si se envia, reenvia o acepta cotizacion
                If (isset(Yii::$app->request->post()['send']) || isset(Yii::$app->request->post()['resend']) || isset(Yii::$app->request->post()['accept'])) {
                    //  $pedidos[] = $model->getLastlog($model->estado)->pedidoProductoLogs;
                    $this->actionEnviarCorreo($model->estado, $estadoPrevio, $model->proveedor->correo, $model->proveedor->idioma, $model->proveedor_id, $model->id);
                }
            }
            return $this->redirect(['proveedor/purchases', 'id' => $model->proveedor_id]);
        } else {
            return $this->render('update', Proveedor::getInfoProductos($model));
        }
    }

    /**
     * Approves an imported despacho in its respective pedidos.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionApprove($proveedor_id, $estado) {
        if (Yii::$app->request->isPost) {
            $estadoPrevio = $estado;
            $estado = isset(Yii::$app->request->post()['accept']) ? $estado + 1 : $estado;
            $pedidos = [];
            foreach (Yii::$app->request->post() as $idPedido => $pedido) {
                if (is_array($pedido)) {
                    $modelPedido = Pedido::findOne($idPedido);
                    $modelPedido->estado = $estado;
                    $despacho = '';
                    $factura = '';
                    $modelLiquidacion = new UploadForm();
                    if ($estado !== $estadoPrevio && $estado === 3) {
                        $despacho = PedidoLog::find()->select('despacho')->orderBy('despacho DESC')->one()['despacho'];
                        $despacho = $despacho ? $despacho + 1 : 1;
                    } elseif ($estado !== $estadoPrevio && $estado === 4) {
                        $factura = PedidoLog::find()->select('factura')->orderBy('factura DESC')->one()['factura'];
                        $factura = $factura ? $factura + 1 : 1;
                        $modelLiquidacion = new Liquidacion(['id' => $factura, 'estado' => 0]);
                    }
                    $modelPedidoLog = new PedidoLog(['pedido_id' => $idPedido, 'estado' => $estado, 'despacho' => $despacho, 'factura' => $factura]);
                    if ($modelPedidoLog->save() && $modelPedido->save() && $modelLiquidacion->save()) {
                        if (isset($pedido['PedidoProductoLog'])) {
                            $arrPedidoProdLog = [];
                            $arrPedidoProdLogFac = [];
                            $valor_factura = 0;
                            foreach ($pedido['PedidoProductoLog'] as $key => $producto) {
                                $precio = isset($producto['precio']) ? $producto['precio'] : 0;
                                $cantidad = isset($producto['cantidad']) ? $producto['cantidad'] : 0;
                                $valor_factura += ($precio * $cantidad);
                                if (isset($producto['pedido_log_id'])) {
                                    $arrPedidoProdLog[] = [
                                        isset($producto['pedido_log_id']) ? $producto['pedido_log_id'] : '',
                                        $key,
                                        isset($producto['nueva_cantidad']) ? $producto['nueva_cantidad'] : '',
                                        isset($producto['observacion']) ? $producto['observacion'] : '',
                                        isset($producto['estado']) ? $producto['estado'] : '0',
                                        new Expression('NOW()'), Yii::$app->user->id
                                    ];
                                    if ($modelLiquidacion) {
                                        $arrPedidoProdLogFac[] = [
                                            $modelPedidoLog->id,
                                            $key,
                                            isset($producto['nueva_cantidad']) ? $producto['nueva_cantidad'] : '',
                                            $precio,
                                            $cantidad,
                                            isset($producto['observacion']) ? $producto['observacion'] : '',
                                            isset($producto['estado']) ? $producto['estado'] : '0',
                                            new Expression('NOW()'), Yii::$app->user->id
                                        ];
                                    }
                                }
                            }
                            $pedidos[] = $arrPedidoProdLog;
                            //guardamos las observaciones en el log
                            Producto::batchUpdate(PedidoProductoLog::tableName(), ['pedido_log_id', 'producto_id', 'nueva_cantidad', 'observacion', 'estado', 'updated_at', 'updated_by'], $arrPedidoProdLog, ['nueva_cantidad', 'observacion', 'estado', 'updated_at', 'updated_by'])->execute();

                            if ($modelLiquidacion->className() === Liquidacion::className()) {
                                Producto::batchUpdate(PedidoProductoLog::tableName(), ['pedido_log_id', 'producto_id', 'nueva_cantidad', 'precio', 'cantidad', 'observacion', 'estado', 'updated_at', 'updated_by'], $arrPedidoProdLogFac, ['nueva_cantidad', 'precio', 'cantidad', 'observacion', 'estado', 'updated_at', 'updated_by'])->execute();
                                $modelLiquidacion->valor_factura = $valor_factura;
                                $modelLiquidacion->save();
                            }
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'No se guardaron los cambios. Por favor intente nuevamente');
                    }
                }
            }
            $proveedor = Proveedor::findOne($proveedor_id);
            $correo = $proveedor->correo;
            $idioma = $proveedor->idioma;
            if ($correo) {
                $this->actionEnviarCorreo($estado, $estadoPrevio, $correo, $idioma, $proveedor_id);
            } else {
                Yii::$app->getSession()->setFlash('error', 'El proveedor no tiene correo configurado. El correo no fue enviado');
            }

            return $this->redirect(['proveedor/purchases', 'id' => $proveedor_id]);
        } else {
            //Obtenemos los modelos de los pedidos que tienen despachos por aprobar

            $models = Pedido::find()->select('pedido.*')
                    ->join('join', PedidoLog::tableName(), 'pedido_id = pedido.id')
                    ->join('join', PedidoProductoLog::tableName(), 'pedido_log_id = pedido_log.id')
                    ->where(['proveedor_id' => $proveedor_id, 'pedido.estado' => $estado, 'pedido_log.estado' => $estado])
                    ->groupBy('pedido.id')
                    ->all();

//            echo "<pre>";
//            print_R($models);
//            echo "</pre>";
//            die;
            return $this->render('approve', [
                        'models' => $models,
            ]);
        }
    }

    /**
     * Send email.
     * If action is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEnviarCorreo($estado, $estadoPrevio, $correo, $idioma, $proveedor_id, $pedido_id = null) {
        $idioma = $idioma ? $idioma : 'es';
        $contenido = FormatoCorreo::findOne(['estado' => $estado, 'estado_previo' => $estadoPrevio, 'idioma' => $idioma]);
        $pedidos = [];
        if ($pedido_id) {
            $model = $this->findModel($pedido_id);
            // Si el pedido pasa de borrador a enviado se deben tomar el pedido para crear el archivo, sino, se debe tomar el log del pedido
            $pedidos = $estadoPrevio === 0 ? $model : $model->getLastlog($model->estado);
        } else {
            $models = Pedido::find()->select('pedido.*')
                    ->join('join', PedidoLog::tableName(), 'pedido_id = pedido.id')
                    ->join('join', PedidoProductoLog::tableName(), 'pedido_log_id = pedido_log.id')
                    ->where(['proveedor_id' => $proveedor_id, 'pedido.estado' => $estado, 'pedido_log.estado' => $estado])
                    ->groupBy('pedido.id')
                    ->all();
            foreach ($models as $model) {
                $pedidos[] = Pedido::getPenultimoLog($model->id, $estado);
            }
        }
        if ($contenido) {
            if (Yii::$app->mailer->compose()
                            ->setFrom('ireypar@importacionesreypar.com')
                            ->setTo($correo)
                            ->setSubject($contenido->asunto)
                            ->setTextBody($contenido->cuerpo_texto)
//         ->setHtmlBody('<b>HTML content</b>')
                            ->attach($this->actionGenerarArchivo($pedidos, $estadoPrevio))
                            ->send() == 1) {
                Yii::$app->getSession()->setFlash('success', 'Se envió el correo a: ' . $correo);
            } else {
                Yii::$app->getSession()->setFlash('error', 'No se pudo enviar el correo');
            }
        } else {
            Yii::$app->getSession()->setFlash('error', 'No hay un formato de correo para el estado del pedido. No se envió el correo');
        }

        return;
    }

    public function actionGenerarArchivo($pedidos, $estadoPrevio) {

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("REYPAR - SICOM")
                ->setLastModifiedBy("REYPAR - SICOM")
                ->setTitle("Pedido")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Result file");
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Código')
                ->setCellValue('B1', 'Referencia')
                ->setCellValue('C1', 'Cantidad')
                ->setCellValue('D1', 'Precio')
                ->setCellValue('E1', 'Observiacion')
                ->setCellValue('F1', 'Nueva Cantidad');
        $baseRow = 2;
        $pedido_id = '';
        if ($estadoPrevio === 0) {
            $pedido_id .= $pedidos->codigo_pedido;
            foreach ($pedidos->pedidoProductos as $producto) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $baseRow, $producto->producto->codigo)
                        ->setCellValue('B' . $baseRow, $producto->producto->referencia)
                        ->setCellValue('C' . $baseRow, $producto->cantidad)
                        ->setCellValue('D' . $baseRow, $producto->precio_und)
                        ->setCellValue('E' . $baseRow, $producto->observacion);
                $baseRow++;
            }
        } else {
            foreach ($pedidos as $pedidoLog) {
                $pedido_id .= @$pedidoLog->pedido_id . '_';
                if (isset($pedidoLog->pedidoProductoLogs)) {

                    foreach ($pedidoLog->pedidoProductoLogs as $producto) {
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A' . $baseRow, $producto->producto->codigo)
                                ->setCellValue('B' . $baseRow, $producto->producto->referencia)
                                ->setCellValue('C' . $baseRow, $producto->cantidad)
                                ->setCellValue('D' . $baseRow, $producto->precio)
                                ->setCellValue('E' . $baseRow, $producto->observacion)
                                ->setCellValue('F' . $baseRow, $producto->nueva_cantidad);
                        $baseRow++;
                    }
                }
            }
        }
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="export.xlsx"');
//        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('uploads/' . $pedido_id . '.xlsx');
        return 'uploads/' . $pedido_id . '.xlsx';
    }

    /**
     * Validates an existing Pedido model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionValidate($id) {
        $model = $this->findModel($id);
        If (Yii::$app->request->isPost) {
            If (isset(Yii::$app->request->post()['validate']) && $model->estado == 1) {
                $model->estado = 2; //validado
            } else {
                $model->estado = 3; //confirmado
            }
        }
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pedido model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $proveedor_id = $this->findModel($id)->proveedor_id;
        $this->findModel($id)->deleteWithRelated();
        return $this->redirect(['proveedor/purchases', 'id' => $proveedor_id]);
    }

    /**
     * 
     * Export Pedido information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerPedidoLog = new ArrayDataProvider([
            'allModels' => $model->pedidoLogs,
        ]);
        $providerPedidoProducto = new ArrayDataProvider([
            'allModels' => $model->pedidoProductos,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerPedidoLog' => $providerPedidoLog,
            'providerPedidoProducto' => $providerPedidoProducto,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => Yii::$app->name],
            'methods' => [
                'SetHeader' => [Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Finds the Pedido model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pedido the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pedido::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid
     * for PedidoLog
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddPedidoLog() {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PedidoLog');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPedidoLog', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid
     * for PedidoProducto
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddPedidoProducto() {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PedidoProducto');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPedidoProducto', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Returns the list of products for tyepeahead.
     * @param string $q
     * @param integer $id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionProductList($q = null, $id = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, nombre AS text')
                    ->from('producto')
                    ->where(['like', 'nombre', $q])
                    ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Producto::find($id)->nombre];
        }
        return $out;
    }

    /**
     * Imports a excel file.
     * @param integer $proveedor_id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionImportPedido($proveedor_id) {
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = '-';
            if ($modelUpload->upload()) {
                $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
                $workbook = SpreadsheetParser::open($inputFiles);
                $worsheets = $workbook->getWorksheets();
                $fecha = new Expression('NOW()');
                $usuario_id = Yii::$app->user->id;
                $error = 0;
                $result = '';
                foreach ($worsheets as $sheet) {
                    $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
                    foreach ($workbook->createRowIterator($myWorksheetIndex) as $row) {
                        $producto_id = isset($row[0]) && $row[0] ?
                                @Producto::find()->where(['codigo' => $row[0], ProveedorProducto::tableName() . '.proveedor_id' => $proveedor_id, Producto::tableName() . '.estado' => 1])->join('join', ProveedorProducto::tableName(), 'producto_id = id')->one()['id'] : '';
                        $cantidad = isset($row[1]) && is_numeric($row[1]) ? $row[1] : '';
                        if ($producto_id && $cantidad) {
                            $rows[] = [$producto_id, $cantidad];
                        } else {
                            $error++;
                            $result .= $error === 1 ? '' : ($row[0] . ':' . $row[1] . ', ');
                        }
                    }
                }
                $result = substr($result, 0, -2);
                if ($error <= 1 && count($rows) > 0) {
                    $codigo_pedido = Proveedor::getCodigoPedido($proveedor_id);
                    $pedido = new Pedido(['codigo_pedido' => $codigo_pedido, 'proveedor_id' => $proveedor_id, 'estado' => 0, 'created_at' => $fecha, 'created_by' => $fecha, 'created_by' => $usuario_id, 'updated_by' => $usuario_id]);
                    $pedido->save();
                    foreach ($rows as $producto) {
                        $productos[] = [$pedido->id, $producto[0], $producto[1], 1, $fecha, $fecha, $usuario_id, $usuario_id];
                    }
                    try {
                        Yii::$app->db->createCommand()->batchInsert(PedidoProducto::tableName(), ['pedido_id', 'producto_id', 'cantidad', 'estado', 'created_at', 'updated_at', 'created_by', 'updated_by'], $productos)->execute();
//                        Yii::$app->db->createCommand()->batchInsert(PedidoProductoLog::tableName(), ['pedido_id', 'producto_id', 'cantidad', 'created_at', 'updated_at', 'created_by', 'updated_by'], $productos)->execute();
                    } catch (Exception $e) {
                        $result = 'No se pudo importar el archivo. Por favor verifique que no hayan productos repetidos en el archivo en intente nuevamente';
                    }
                } else {
                    $result = 'No se pudo importar el archivo. Por favor verifique que todos los productos estén activos y asignados al proveedor seleccionado y que tengan las cantidades correctas (' . $result . ')';
                }
                Yii::$app->getSession()->setFlash($result ? 'error' : 'success', $result ? $result : 'El pedido fue importado correctamente');
                $url = $result ? Url::to(['pedido/import-pedido', 'proveedor_id' => $proveedor_id]) : Url::to(['proveedor/purchases', 'id' => $proveedor_id]);
                return $this->redirect($url);
            }
        }
        return $this->render('import_pedido', ['model' => $modelUpload]);
    }

    /**
     * Imports a excel file.
     * @param integer $id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionImport($id = null, $proveedor_id = null, $estado = null) {
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = '-';
            if ($modelUpload->upload()) {
                if ($id) {
                    $modelPedidoLog = PedidoLog::findOne(['id' => $id]);
                    $modelProveedor = $modelPedidoLog->pedido->proveedor;
                    $result = $this->loadFile($modelUpload, $modelPedidoLog, null, $estado);
                    Yii::$app->getSession()->setFlash($result == "Proceso terminado" ? 'success' : 'error', $result);
                    return $this->redirect(['update', 'id' => $modelPedidoLog->pedido_id]);
                } elseif ($proveedor_id) {
                    $modelProveedor = Proveedor::findOne(['id' => $proveedor_id]);
                    $result = $this->loadFile($modelUpload, null, $modelProveedor, $estado);
                    Yii::$app->getSession()->setFlash($result == "Proceso terminado" ? 'success' : 'error', $result);
                    return $this->redirect(['proveedor/purchases', 'id' => $proveedor_id]);
                }
            }
        }
        if ($estado === 2 || $estado === 3) {
            return $this->redirect(['proveedor/purchases', 'id' => $proveedor_id]);
        }
        return $this->render('import', ['model' => $modelUpload]);
    }

    /**
     * 
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function loadFile($modelUpload, $modelPedidoLog, $modelProveedor, $estado) {

        $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
        $workbook = SpreadsheetParser::open($inputFiles);
        $worsheets = $workbook->getWorksheets();
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az',);
        $proveedor = $modelProveedor ? $modelProveedor : $modelPedidoLog->pedido->proveedor;
        $columna_codigo = array_search(strtolower($proveedor->columna_codigo), $alphabet);
        $columna_cantidad = array_search(strtolower($proveedor->columna_cantidad), $alphabet);
        $columna_precio = array_search(strtolower($proveedor->columna_precio), $alphabet);
        $columna_pedido_id = array_search(strtolower($proveedor->columna_pedido_id), $alphabet);
        $created_by = $updated_by = Yii::$app->user->id;
        $created_at = $updated_at = new Expression('NOW()');
        $rows = [];
        $arrPedidoLogId = [];
        $pedido_proveedor = '';
        $pedido_proveedor_ant = '';
        $arrProducto_id = [];
        $productosNoEncontrados = [];
        $productosExtra = [];
        $arrPedidoProductoProductoId = [];
        if ($modelPedidoLog) {
            $model = $this->findModel($modelPedidoLog->pedido_id);
            foreach ($model->pedidoProductos as $pedidoProducto) {
                $arrPedidoProductoProductoId[] = $pedidoProducto->producto_id;
            }
        }
        foreach ($worsheets as $sheet) {
            $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
            foreach ($workbook->createRowIterator($myWorksheetIndex) as $row) {
                $pedido_proveedor = isset($row[$columna_pedido_id]) && $row[$columna_pedido_id] ? $row[$columna_pedido_id] : '';
                if ($modelPedidoLog) {
                    $pedidoLog_id = $modelPedidoLog->id;
                } else {
                    if ($pedido_proveedor) {
                        if ($pedido_proveedor != $pedido_proveedor_ant) {
                            //traemos el ultimo log en estado 2 y 3 de cada pedido
                            $pedidoLog_id = @PedidoLog::find()->where(['pedido.codigo' => $pedido_proveedor, 'pedido_log.estado' => $estado, 'pedido.proveedor_id' => $proveedor->id])->join('JOIN', 'pedido', 'pedido_id = pedido.id')->orderBy(['id' => SORT_DESC])->one()['id'];
                            //traemos los productos de la ultima cotizacion del pedido
                            $arrPedidoProductoProductoId = PedidoProductoLog::find()->select('producto_id')->where(['pedido.codigo' => $pedido_proveedor, 'pedido_log.estado' => 1, 'pedido.proveedor_id' => $proveedor->id])->join('JOIN', 'pedido_log', 'pedido_log_id = pedido_log.id')->join('JOIN', 'pedido', 'pedido_id = pedido.id')->orderBy(['pedido_log.id' => SORT_DESC])->column();
                            $pedido_proveedor_ant = $pedido_proveedor;
                        }
                    } else {
                        $pedidoLog_id = '';
                        $arrPedidoProductoProductoId = [];
                    }
                }

                $arrPedidoLogId[] = $pedidoLog_id;

//                print_R($row);
//                echo $row[$columna_codigo];
//
//                die;
                $d = @Producto::find()->where(['codigo' => @$row[$columna_codigo], ProveedorProducto::tableName() . '.proveedor_id' => $proveedor->id])->join('join', ProveedorProducto::tableName(), 'producto_id = id')->one()['id'];
                $producto_id = isset($row[$columna_codigo]) && $row[$columna_codigo] ? @$d : '';
                $cantidad = isset($row[$columna_cantidad]) ? $row[$columna_cantidad] : '';
                $precio = isset($row[$columna_precio]) ? $row[$columna_precio] : '';
                if (is_numeric($cantidad) && is_numeric($precio)) {
                    if ($producto_id && array_search($producto_id, $arrPedidoProductoProductoId) === false) {
                        $productosExtra[] = $row[$columna_codigo];
                    }
                    if ($pedidoLog_id && $producto_id) {
                        $arrProducto_id[] = $producto_id;
                        $rows[] = [$pedidoLog_id, $producto_id, $cantidad, $precio, $updated_at, $updated_by, $created_at, $created_by];
                    } elseif (isset($row[$columna_codigo]) && $row[$columna_codigo] != '' && !$producto_id) {
                        //agregamos los productos no encontrados en el sistema para informar
                        $productosNoEncontrados[] = $row[$columna_codigo];
                    }
                }
            }
        }

        //Verificamos si los productos estan en el pedido inicial y no están en el archivo importado y los agregamos al array para ser insertados con valores en cero
        if ($modelPedidoLog) {
            $model = $this->findModel($modelPedidoLog->pedido_id);
            foreach ($model->pedidoProductos as $pedidoProducto) {
                if (array_search($pedidoProducto->producto_id, $arrProducto_id) === false) {
                    $rows[] = [$modelPedidoLog->id, $pedidoProducto->producto_id, 0, 0, $updated_at, $updated_by, $created_at, $created_by];
                }
            }
        }
        if (!empty($rows)) {
            PedidoProductoLog::deleteAll(['IN', 'pedido_log_id', $arrPedidoLogId]);
            // inserta productos en la tabla
            try {
                Yii::$app->db->createCommand()->batchInsert(PedidoProductoLog::tableName(), ['pedido_log_id', 'producto_id', 'cantidad', 'precio', 'updated_at', 'updated_by', 'created_at', 'created_by'], $rows)->execute();
            } catch (Exception $e) {

                return 'No se pudo importar el archivo. Por favor verifique que no hayan productos repetidos en el archivo en intente nuevamente';
            }
            if ($pedido_proveedor !== '' && $modelPedidoLog) {
                Pedido::updateAll(['codigo' => $pedido_proveedor], ['id' => $modelPedidoLog->pedido_id]);
            }
            if ($pedido_proveedor !== '') {
                if (isset($modelPedidoLog->estado)) {
                    $estado = $modelPedidoLog->estado + 1;
                    $sql = "UPDATE pedido SET estado = $estado WHERE proveedor_id = " . $proveedor->id . " and codigo = '" . $pedido_proveedor . "'";
                    $qyerK = Yii::$app->db->createCommand($sql)->execute();

                    $modelPedidoExtra = Pedido::find()->where(['proveedor_id' => $proveedor->id, 'codigo' => $pedido_proveedor])->one()['id'];
                    $sql = "UPDATE pedido_log SET estado = $estado WHERE pedido_id = '$modelPedidoExtra'";
                    $qyerK = Yii::$app->db->createCommand($sql)->execute();
                }
            }
        } else {
            $modelPedidoExtra = @Pedido::find()->where(['proveedor_id' => $proveedor->id, 'estado' => 2])->one()['codigo'];
            if ($modelPedidoExtra != "") {
                $sql = "UPDATE pedido SET estado = 3 WHERE proveedor_id = " . $proveedor->id . " and codigo = '" . $modelPedidoExtra . "'";
                $qyerK = Yii::$app->db->createCommand($sql)->execute();
            }

            $modelPedidoExtra = @Pedido::find()->where(['proveedor_id' => $proveedor->id, 'estado' => 2])->one()['id'];
            if ($modelPedidoExtra != "") {
                $sql = "UPDATE pedido_log SET estado = $estado WHERE pedido_id = '$modelPedidoExtra'";
                $qyerK = Yii::$app->db->createCommand($sql)->execute();
            }
        }

//        if(isset($pedido_proveedor) && $pedido_proveedor !== ''  ){
//            Pedido::updateAll(['codigo' => $pedido_proveedor], ['id' => $modelPedidoLog->pedido_id]);
//        }
//        echo "<pre>";
//        echo "Vamos";
//        print_R($rows);
//        print_R($pedido_proveedor);
//        print_R($modelPedidoLog);
//        echo "<pre/>";
//        die;
        $return = '';
        if ($productosNoEncontrados) {
            $return .= 'Los siguientes productos no fueron encontrados en el sistema o no están realcionados con el proveedor, por lo tanto no fueron cargados: ';
            foreach ($productosNoEncontrados as $value) {
                $return .= $value . ', ';
            }
            $return = substr($return, 0, -2) . '. ';
        }
//        $productosExtra = array_filter($productosExtra);
//        if ($productosExtra) {
//            $return .= 'Los siguientes productos no están en el pedido inicial: ';
//            foreach ($productosExtra as $value) {
//                $return .= $value . ', ';
//            }
//            $return = substr($return, 0, -2) . '. ';
//        }
        //   $deletedFile = unlink($inputFiles);
        return $return !== '' ? $return : "Proceso terminado";
    }

}
