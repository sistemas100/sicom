<?php

namespace app\controllers;

use Akeneo\Component\SpreadsheetParser\SpreadsheetParser;
use app\models\LiquidacionSearch;
use app\models\Pedido;
use app\models\PedidoSearch;
use app\models\Producto;
use app\models\Proveedor;
use app\models\ProveedorProducto;
use app\models\ProveedorProductoSearch;
use app\models\ProveedorSearch;
use app\models\UploadForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProveedorController implements the CRUD actions for Proveedor model.
 */
class ProveedorController extends AppController{

    /**
     * Lists all Proveedor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProveedorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    

    /**
     * Displays a single Proveedor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new ProveedorProductoSearch(['proveedor_id'=>$id]);
        $model = $this->findModel($id);
        $providerPedido = new ArrayDataProvider([
            'allModels' => $model->pedidos,
        ]);
        $providerProveedorProducto = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerPedido' => $providerPedido,
            'searchModel' => $searchModel,
            'providerProveedorProducto' => $providerProveedorProducto,
        ]);
    }
    
    /**
     * Displays a single Proveedor purchases.
     * @param integer $id
     * @return mixed
     */
    public function actionPurchases($id)
    {
        $searchPedido = new PedidoSearch(['proveedor_id'=>$id]);
        $providerPedido =  $searchPedido->search(Yii::$app->request->queryParams);
        $searchLiquidacion = new LiquidacionSearch(['proveedor_id'=>$id]);
        $providerLiquidacion =  $searchLiquidacion->search(Yii::$app->request->queryParams);
  
        return $this->render('purchases', [
            'model' => $this->findModel($id),
            'searchPedido' => $searchPedido,
            'providerPedido' => $providerPedido,
            'searchLiquidacion' => $searchLiquidacion,
            'providerLiquidacion' => $providerLiquidacion,
        ]);
    }

    /**
     * Creates a new Proveedor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proveedor();

        if ($model->loadAll(Yii::$app->request->post(),['pedidos','proveedorProductos']) && $model->saveAll(['pedidos','proveedorProductos'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proveedor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post(),['pedidos','proveedorProductos']) && $model->saveAll(['pedidos','proveedorProductos'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proveedor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Proveedor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proveedor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Pedido
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPedido()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Pedido');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPedido', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for ProveedorProducto
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddProveedorProducto()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ProveedorProducto');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProveedorProducto', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
     * Imports a excel file.
     * @param integer $proveedor_id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    
    public function actionImportProductosProveedor($proveedor_id) {
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = '-';
            if ($modelUpload->upload()) {
                $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
                $workbook = SpreadsheetParser::open($inputFiles);
                $worsheets = $workbook->getWorksheets();
                $fecha  = new Expression('NOW()');
                $usuario_id  = Yii::$app->user->id;
                $error = 0;
                $result = '';
                $fallidos = '';
                $skip = 1;
                foreach ($worsheets as $sheet) {
                    $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
                    foreach ($workbook->createRowIterator($myWorksheetIndex) as $row) {
                        if($skip){
                            $skip--;
                        }else{
                            $producto_id = isset($row[0]) && $row[0] ?
                            Producto::find()->where(['codigo' => $row[0]])->one()['id'] : '';
                            $ref_proveedor = isset($row[1])?$row[1]:'';
                            $ref_proveedor2 = isset($row[2])?$row[2]:'';
                            $promedio = isset($row[3])?$row[3]:'';
                            $estado = isset($row[4])?$row[4]:'';
                            if ($producto_id && is_numeric($estado)) {
                                $productos[] = [$producto_id,$proveedor_id,$ref_proveedor,$ref_proveedor2,$fecha,$fecha,$usuario_id,$usuario_id];
                                $productosEstado[] = [$producto_id,$promedio,$estado,$fecha,$usuario_id];
                            }else{
                                $fallidos .= $row[0].', ';
                                $error++;
                            }   
                        }
                    }
                }
                if($error < 1 && count($productos)>0){
                    ProveedorProducto::deleteAll(['proveedor_id'=>$proveedor_id]);
                    try {
                        Yii::$app->db->createCommand()->batchInsert(ProveedorProducto::tableName(), ['producto_id','proveedor_id','referencia_proveedor','referencia_proveedor2','created_at','updated_at', 'created_by','updated_by'], $productos)->execute();
                    } catch (Exception $e) {
                        $result =  'No se pudo importar el archivo. Por favor verifique que no hayan productos repetidos en el archivo en intente nuevamente';
                    }
                    Producto::batchUpdate(Producto::tableName(), ['id', 'promedio_manual','estado', 'updated_at', 'updated_by'], $productosEstado, ['promedio_manual','estado', 'updated_at', 'updated_by'])->execute();
                }else{
                    $fallidos = substr($fallidos, 0, -2);
                    $result =  'No se pudo importar el archivo. Los siguientes productos están presentando inconvenientes: '.$fallidos.'. Verifique el estado en el archivo y que estén creados en la aplicación';
                }
                Yii::$app->getSession()->setFlash($result? 'error':'success', $result?$result:'La información fue importada correctamente');
                $url = $result? Url::to(['proveedor/import-productos-proveedor', 'proveedor_id' => $proveedor_id]):Url::to(['proveedor/view', 'id' => $proveedor_id]);
                return $this->redirect($url);
            }
        }
        return $this->render('import', ['model' => $modelUpload]);
    }
    
    
    /**
     * Generates report of disabled productos.
     * @author Juan Velez <juanfv@gmail.com>
     * @return mixed
     */
    public function actionReporteProductosDesactivados($proveedor_id) {
        $model = new Pedido(['proveedor_id' => $proveedor_id, 'estado' => null, 'created_at' => date("Y-m-01 H:i:s")]);
        return $this->render('reporte_productos_desactivados', Proveedor::getInfoProductos($model));
    }
}
