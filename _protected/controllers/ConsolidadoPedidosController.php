<?php

namespace app\controllers;

use Yii;
use app\models\ConsolidadoPedidos;
use app\models\ConsolidadoPedidosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConsolidadoPedidosController implements the CRUD actions for ConsolidadoPedidos model.
 */
class ConsolidadoPedidosController extends AppController{


    /**
     * Lists all ConsolidadoPedidos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConsolidadoPedidosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConsolidadoPedidos model.
     * @param string $producto_codigo
     * @param string $fecha
     * @return mixed
     */
    public function actionView($producto_codigo, $fecha)
    {
        $model = $this->findModel($producto_codigo, $fecha);
        return $this->render('view', [
            'model' => $this->findModel($producto_codigo, $fecha),
        ]);
    }

    /**
     * Creates a new ConsolidadoPedidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ConsolidadoPedidos();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'producto_codigo' => $model->producto_codigo, 'fecha' => $model->fecha]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ConsolidadoPedidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $producto_codigo
     * @param string $fecha
     * @return mixed
     */
    public function actionUpdate($producto_codigo, $fecha)
    {
        $model = $this->findModel($producto_codigo, $fecha);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'producto_codigo' => $model->producto_codigo, 'fecha' => $model->fecha]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ConsolidadoPedidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $producto_codigo
     * @param string $fecha
     * @return mixed
     */
    public function actionDelete($producto_codigo, $fecha)
    {
        $this->findModel($producto_codigo, $fecha)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the ConsolidadoPedidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $producto_codigo
     * @param string $fecha
     * @return ConsolidadoPedidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($producto_codigo, $fecha)
    {
        if (($model = ConsolidadoPedidos::findOne(['producto_codigo' => $producto_codigo, 'fecha' => $fecha])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
     /**
     * Imports a excel file.
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionImport() {
        $modelUpload = new \app\models\UploadForm();
        
        ini_set('max_execution_time', 0);
        if (Yii::$app->request->isPost) {
            $modelUpload->file = \yii\web\UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = Yii::$app->request->post('UploadForm')['fecha'];
            $modelUpload->tipo = 'na';
            if ($modelUpload->upload()) {
                $result = $this->loadFile($modelUpload);
                Yii::$app->getSession()->setFlash('success', $result);
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('import', ['model' => $modelUpload]);
    }
    
    
    /**
     * 
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function loadFile($modelUpload) {
        $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
        $workbook = \Akeneo\Component\SpreadsheetParser\SpreadsheetParser::open($inputFiles);
        $worsheets = $workbook->getWorksheets();
        $columna_codigo  = 0;
        $columna_cantidad  = 2;
        $fecha = $modelUpload->fecha;
        $created_by = Yii::$app->user->id;
        $updated_by = Yii::$app->user->id;
        $created_at = new \yii\db\Expression('NOW()');
        $updated_at = new \yii\db\Expression('NOW()');
        
        $arrProductos = array();
        foreach ($worsheets as $sheet) {
            $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
            foreach ($workbook->createRowIterator($myWorksheetIndex) as $rowIndex =>$row) {
                $codigo = trim(isset($row[$columna_codigo])&& (is_string($row[$columna_codigo])||is_numeric($row[$columna_codigo])) ?$row[$columna_codigo]:'');
                $cantidad = trim(isset($row[$columna_cantidad])&& is_numeric($row[$columna_cantidad])?$row[$columna_cantidad]:'');
                $i = floor($rowIndex/1000);
                if($codigo != '' && is_numeric($cantidad)){
                    $arrProductos[$i][] = [$codigo,$fecha,$cantidad,$updated_at,$updated_by,$created_at,$created_by];
                }
            }
        }

        if(!empty($arrProductos)){
            foreach ($arrProductos as $productos) {
                \app\models\Producto::batchUpdate('consolidado_pedidos', 
                        ['producto_codigo','fecha','cantidad','updated_at','updated_by','created_at','created_by'],
                        $productos,
                        ['cantidad','updated_at','updated_by'])->execute();
            }
        }
                
     //   $deletedFile = unlink($inputFiles);
        return  "Proceso terminado";
//        return  "Registros borrados: $deletedRows, Registros actualizados: $updatedRows";
    }
}
