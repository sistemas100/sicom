<?php

namespace app\controllers;

use app\models\Liquidacion;
use app\models\LiquidacionSearch;
use app\models\PedidoLog;
use app\models\PedidoProductoLog;
use app\models\Producto;
use app\models\Proveedor;
use Yii;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * LiquidacionController implements the CRUD actions for Liquidacion model.
 */
class LiquidacionController extends AppController{

    /**
     * Lists all Liquidacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Liquidacion model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $productos = [];
            $produto_id = '';
            $keyAnt = '';
            $fechas = [];
            for ($index = 6; $index >= 0; $index--) {
                $fecha = date("Y-n", strtotime("-$index months", strtotime(date("Y-m-01", $model->estado?strtotime($model->updated_at):time()))));
                $fechas[] = $fecha;
            }
            $proveedor_id = PedidoLog::find()->where(['factura'=>$id])->with('pedido')->one()->pedido->proveedor_id;
            $infoProductos = Proveedor::getInfoProductosQ($proveedor_id,null,null,$id);
                foreach ($infoProductos as $key => $producto) {
                        if ($produto_id !== $producto['producto_id']) {
                            $productos[$key] = $producto;
                            $keyAnt = $key;
                            foreach ($fechas as $fecha) {
                                $productos[$key][$fecha] = $producto['fecha'] == $fecha ? ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']] : ['pedidos' => 0, 'ventas' => 0];
                            }
                        } else {
                            foreach ($fechas as $fecha) {
                                if ($producto['fecha'] == $fecha) {
                                    $productos[$keyAnt][$fecha] = ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']];
                                }
                            }
                        }
                        $produto_id = $producto['producto_id'];
                }
            $precios = Producto::getPreciosCompetencia($proveedor_id);
            return $this->render('view', [
                'model' => $model,
                'productos' => $productos,
                'fechas' => $fechas,
                'precios' => $precios,
            ]);
    }

    /**
     * Creates a new Liquidacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Liquidacion();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Liquidacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
        /**
     * Updates an existing Liquidacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReport($id)
    {     
        $model = $this->findModel($id);
        if (isset(Yii::$app->request->post()['PedidoProducto'])) {
            $arrPedidoProdLog = [];
            foreach (Yii::$app->request->post()['PedidoProducto'] as $key => $producto) {
                $arrPedidoProdLog[] = [$producto['pedido_log_id'],
                    $producto['producto_id'],
                    isset($producto['observacion'])?$producto['observacion']:'',
                    new Expression('NOW()'), Yii::$app->user->id];
            }
            //guardamos las observaciones en el log
            Producto::batchUpdate(PedidoProductoLog::tableName(),
                    ['pedido_log_id', 'producto_id', 'observacion', 'updated_at', 'updated_by'],
                    $arrPedidoProdLog,
                    ['observacion', 'updated_at', 'updated_by'])->execute();
             return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $productos = [];
            $produto_id = '';
            $keyAnt = '';
            $fechas = [];
            for ($index = 6; $index >= 0; $index--) {
                $fecha = date("Y-n", strtotime("-$index months", strtotime(date("Y-m-01", $model->estado?strtotime($model->updated_at):time()))));
                $fechas[] = $fecha;
            }
            $proveedor_id = PedidoLog::find()->where(['factura'=>$id])->with('pedido')->one()->pedido->proveedor_id;
            $infoProductos = Proveedor::getInfoProductosQ($proveedor_id,null,null,$id);
                foreach ($infoProductos as $key => $producto) {
                        if ($produto_id !== $producto['producto_id']) {
                            $productos[$key] = $producto;
                            $keyAnt = $key;
                            foreach ($fechas as $fecha) {
                                $productos[$key][$fecha] = $producto['fecha'] == $fecha ? ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']] : ['pedidos' => 0, 'ventas' => 0];
                            }
                        } else {
                            foreach ($fechas as $fecha) {
                                if ($producto['fecha'] == $fecha) {
                                    $productos[$keyAnt][$fecha] = ['pedidos' => $producto['pedidos'], 'ventas' => $producto['ventas']];
                                }
                            }
                        }
                        $produto_id = $producto['producto_id'];
                }
            $precios = Producto::getPreciosCompetencia($proveedor_id);
            return $this->render('report', [
                'model' => $model,
                'productos' => $productos,
                'fechas' => $fechas,
                'precios' => $precios,
            ]);
        }
    }

    /**
     * Deletes an existing Liquidacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Liquidacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Liquidacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Liquidacion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for LiquidacionItem
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddLiquidacionItem()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('LiquidacionItem');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formLiquidacionItem', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
