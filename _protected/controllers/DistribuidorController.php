<?php

namespace app\controllers;

use Yii;
use app\models\Distribuidor;
use app\models\DistribuidorSearch;
use app\models\ProductoDistribuidor;
use app\models\Producto;
use yii\web\NotFoundHttpException;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * DistribuidorController implements the CRUD actions for Distribuidor model.
 */
class DistribuidorController extends AppController {

    /**
     * Lists all Distribuidor models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DistribuidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distribuidor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $searchModel = new \app\models\ProductoDistribuidorSearch();
        $dataProvider = $searchModel->search(['ProductoDistribuidorSearch' => ['distribuidor_id' => $id]]);
        $model = $this->findModel($id);
//        $providerProducto = new \yii\data\ArrayDataProvider([
//            'allModels' => $model->productosDistribuidor,
//        ]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'providerProducto' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Distribuidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Distribuidor();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Distribuidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post(), ['productosDistribuidor']) && $model->saveAll(['productosDistribuidor'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Imports a excel file.
     * @param integer $id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionImport($id) {
        $modelUpload = new UploadForm();

        ini_set('max_execution_time', 0);

        if (Yii::$app->request->isPost) {


            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = Yii::$app->request->post('UploadForm')['fecha'];
            $modelUpload->tipo = 'na';

            if ($modelUpload->upload()) {
//                $model = Distribuidor::findOne($_GET["id"]);
                if (($this->findModel($_GET["id"]))) {
                    $model = $this->findModel($_GET["id"]);
                    $result = $this->loadFile($modelUpload, $model);
                    Yii::$app->getSession()->setFlash('success', $result);
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    echo "<pre>";
                    echo "Distribuidor no encontrado";
                    echo "</pre>";
                    die;
                }
            }
        }

        return $this->render('import', ['model' => $modelUpload]);
    }

    /**
     * 
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function loadFile($modelUpload, $model) {
        $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
        $workbook = \Akeneo\Component\SpreadsheetParser\SpreadsheetParser::open($inputFiles);
        $worsheets = $workbook->getWorksheets();
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az',);
        $columna_nombre = array_search(strtolower($model->columna_nombre), $alphabet);
        $columna_referencia = array_search(strtolower($model->columna_referencia), $alphabet);
        $columna_precio = array_search(strtolower($model->columna_precio), $alphabet);
        $columna_marca = array_search(strtolower($model->columna_marca), $alphabet);
        $fecha = $modelUpload->fecha;
        $created_by = Yii::$app->user->id;
        $updated_by = Yii::$app->user->id;
        $created_at = new \yii\db\Expression('NOW()');
        $updated_at = new \yii\db\Expression('NOW()');
        $arrProductos = array();
        $arrDistInfo = array();
        //controla que no se ingresen referencias repetidas
        foreach ($worsheets as $sheet) {
            $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
            foreach ($workbook->createRowIterator($myWorksheetIndex) as $rowIndex => $row) {
                $nombre = trim(isset($row[$columna_nombre]) && (is_string($row[$columna_nombre]) || is_numeric($row[$columna_nombre])) ? $row[$columna_nombre] : '');
                $referencia = trim(isset($row[$columna_referencia]) && (is_string($row[$columna_referencia]) || is_numeric($row[$columna_referencia])) ? $row[$columna_referencia] : '');
                $precio = trim(isset($row[$columna_precio]) && is_numeric($row[$columna_precio]) ? $row[$columna_precio] : '');
                $precio = $model->descuento ? @intval($precio - ($precio * ($model->descuento / 100))) : $precio;
                $marca = trim(isset($row[$columna_marca]) && (is_string($row[$columna_marca]) || is_numeric($row[$columna_marca])) ? $row[$columna_marca] : '');
                $i = floor($rowIndex / 500);
                if ($nombre != '' && $referencia != '' && is_numeric($precio) && array_search($referencia, $arrDistInfo) === FALSE) {
                    $arrProductos[$i][] = [$referencia, $nombre, $precio, $marca, 1, 1, $model->id, $updated_at, $updated_by, $created_at, $created_by];
                    $arrDistInfo[] = $referencia;
                }
            }
        }
        $deletedRows = 0;
        $updatedRows = 0;
        if (!empty($arrProductos)) {
//            ini_set('innodb_lock_wait_timeout', '5000');
            // inserta productos en la tabla
            foreach ($arrProductos as $productos) {
                Producto::batchUpdate('producto_distribuidor',
                        ['referencia', 'nombre', 'precio', 'marca', 'nuevo', 'viejo', 'distribuidor_id', 'updated_at', 'updated_by', 'created_at', 'created_by'],
                        $productos,
                        ['nombre', 'precio', 'viejo', 'updated_at', 'updated_by'])->execute();
            }
            //elimina productos que ya no aparecen
//            $deletedRows = ProductoDistribuidor::deleteAll('nuevo IS NULL AND viejo IS NULL AND distribuidor_id = :id', ['id' => $model->id]);

            // Actualiza el código en los registros nuevos
            Distribuidor::updateAll(['fecha' => $fecha], ['id' => $model->id]);
            $updatedRows = ProductoDistribuidor::updateProductoDistribuidor($model->id);


            ProductoDistribuidor::updateAll(['nuevo' => '', 'viejo' => ''], 'nuevo = 1 OR viejo = 1');
        }

        //   $deletedFile = unlink($inputFiles);
        return "Proceso tenminado";
//        return  "Registros borrados: $deletedRows, Registros actualizados: $updatedRows";
    }

    /**
     * Deletes an existing Distribuidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->deleteWithRelated();
        return $this->redirect(['index']);
    }

    /**
     * 
     * Export Distribuidor information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerProducto = new \yii\data\ArrayDataProvider([
            'allModels' => $model->productos,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerProducto' => $providerProducto,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Finds the Distribuidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Distribuidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Distribuidor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid
     * for Producto
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddProducto() {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Producto');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProducto', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

}
