<?php

namespace app\controllers;

use app\models\ProductoDistribuidor;
use app\models\ProductoDistribuidorSearch;
use kartik\mpdf\Pdf;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * ProductoDistribuidorController implements the CRUD actions for ProductoDistribuidor model.
 */
class ProductoDistribuidorController extends AppController{

    /**
     * Lists all ProductoDistribuidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductoDistribuidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all ProductoDistribuidor models with comparative.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new ProductoDistribuidorSearch();
        $searchModel->distribuidor_id = 7;
       // $searchModel->precio = '>0' ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $distribuidores = ProductoDistribuidor::getDistribuidores();
        $menores = ProductoDistribuidor::getMenores();
        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'distribuidores' => $distribuidores,
            'menores' => $menores,
        ]);
    }

    /**
     * Displays a single ProductoDistribuidor model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductoDistribuidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductoDistribuidor();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ProductoDistribuidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductoDistribuidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * Export ProductoDistribuidor information into PDF format.
     * @param string $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => Yii::$app->name],
            'methods' => [
                'SetHeader' => [Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    
    /**
     * Finds the ProductoDistribuidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ProductoDistribuidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductoDistribuidor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
     * 
     * Export report information into Excel format.
     * @return mixed
     */
     /*
	EXPORT WITH PHPEXCEL
	*/ 
	public function actionExport()
    {
        ini_set('memory_limit', '1000M');
         $arrData = ProductoDistribuidor::find()->select("`producto_distribuidor`.*, distribuidor.`nombre` nomdist")
                        ->join('JOIN', 'distribuidor', '`distribuidor_id` = distribuidor.id')
                        ->asArray()->all();
            
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("REYPAR - SICOM")
                                            ->setLastModifiedBy("REYPAR - SICOM")
                                            ->setTitle("Reporte competencia")
                                            ->setKeywords("office 2007 openxml php")
                                            ->setCategory("Result file");
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Referencia')
                    ->setCellValue('B1', 'Nombre')
                    ->setCellValue('C1', 'Precio')
                    ->setCellValue('D1', 'Marca')
                    ->setCellValue('E1', 'Distribuidor')
                    ->setCellValue('F1', 'Fecha');
  
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()
                    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('bcedb4');
        $objPHPExcel->getActiveSheet()->freezePane('A2');
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        $baseRow=2;
        foreach($arrData as $producto){
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$baseRow, $producto['referencia'])
                        ->setCellValue('B'.$baseRow, $producto['nombre'])
                        ->setCellValue('C'.$baseRow, number_format($producto['precio']))
                        ->setCellValue('D'.$baseRow, $producto['marca'])
                        ->setCellValue('E'.$baseRow, $producto['nomdist'])
                        ->setCellValue('F'.$baseRow, $producto['fecha']);
            $baseRow++;
        }
        
           
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="export.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('php://output');
        exit();
    }   

}
