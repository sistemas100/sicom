<?php

namespace app\controllers;

use Akeneo\Component\SpreadsheetParser\SpreadsheetParser;
use app\models\Distribuidor;
use app\models\Producto;
use app\models\ProductoDistribuidor;
use app\models\ProductoSearch;
use app\models\ProductosRelacionados;
use app\models\Referencia;
use app\models\UploadForm;
use kartik\mpdf\Pdf;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductoController implements the CRUD actions for Producto model.
 */
class ProductoController extends AppController {

    /**
     * Lists all Producto models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Producto model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $providerReferencia = new ArrayDataProvider([
            'allModels' => $model->referencias,
        ]);
        $providerProveedorProducto = new ArrayDataProvider([
            'allModels' => $model->proveedorProductos,
        ]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'providerReferencia' => $providerReferencia,
                    'providerProveedorProducto' => $providerProveedorProducto,
        ]);
    }

    /**
     * Creates a new Producto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Producto();
        if ($model->loadAll(Yii::$app->request->post(), ['pedidoProductos', 'pedidoProductoLogs']) && $model->saveAll(['pedidoProductos', 'pedidoProductoLogs'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Producto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->loadAll(Yii::$app->request->post(), ['pedidoProductos', 'pedidoProductoLogs'])) {
            $datos = Yii::$app->request->post();

//            echo "<pre>";
//            print_R(Yii::$app->request->post());
//            echo "</pre>";
//            die;
            if ($model->save()) {
                if (isset($datos["Referencia"])) {

                    foreach ($datos["Referencia"] as $r) {
                        $sql = "INSERT IGNORE INTO referencia values (NULL, '" . $model->id . "','" . $r["referencia"] . "',2)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
                if (isset($datos["ProveedorProducto"])) {
                    foreach ($datos["ProveedorProducto"] as $r) {
                        $sql = "INSERT IGNORE INTO proveedor_producto values ('" . $model->id . "','" . $r["proveedor_id"] . "',NULL,'" . $r["referencia_proveedor"] . "','" . $r["referencia_proveedor2"] . "', NOW(), NOW(), 3,3)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Lists all Producto models with comparative.
     * @return mixed
     */
    public function actionReport() {
        $searchModel = new ProductoSearch();
//        $searchModel->join = TRUE;
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $distribuidores = Distribuidor::find()->all();
//        $menores = ProductoDistribuidor::getMenores();
//        $parametro = Yii::$app->request->queryParams;
        IF (ISSET($_GET["codigo"])) {
            $codi = explode(" ", $_GET["codigo"]);
            $pr = "";
            foreach ($codi as $c) {
                $pr .= "'" . $c . "',";
            }
            $pr = substr($pr, 0, -1);

            $oTherSearch = $searchModel->searchCm($pr);
        } else {
            $oTherSearch = array();
        }
//        print_R($oTherSearch);
        return $this->render('report', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//                    'distribuidores' => $distribuidores,
//                    'menores' => $menores,
                    'otros' => $oTherSearch
        ]);
    }

    /**
     * Deletes an existing Producto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->deleteWithRelated();
        return $this->redirect(['index']);
    }

    /**
     * 
     * Export Producto information into PDF format.
     * @param string $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);
        $providerReferencia = new ArrayDataProvider([
            'allModels' => $model->referencias,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerReferencia' => $providerReferencia,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * 
     * Export report information into Excel format.
     * @return mixed
     */
    /*
      EXPORT WITH PHPEXCEL
     */
    public function actionExport() {
        ini_set('memory_limit', '1000M');
        $searchModel = new ProductoSearch();
        $searchModel->join = TRUE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination(false);
        $distribuidores = Distribuidor::find()->all();
        $menores = ProductoDistribuidor::getMenores();

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("REYPAR - SICOM")
                ->setLastModifiedBy("REYPAR - SICOM")
                ->setTitle("Reporte competencia")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Result file");
        $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells('A1:E1')
                ->setCellValue('A1', 'REYPAR')
                ->setCellValue('A2', 'Código')
                ->setCellValue('B2', 'Referencia')
                ->setCellValue('C2', 'Nombre')
                ->setCellValue('D2', 'Proveedor')
                ->setCellValue('E2', 'Precio');
        $column = 5;
        foreach ($distribuidores as $distribuidor) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($this->toAlpha($column) . '1', $distribuidor->nombre . '-' . $distribuidor->fecha)
                    ->setCellValue($this->toAlpha($column) . '2', 'Referencia')
                    ->setCellValue($this->toAlpha($column + 1) . '2', 'Precio' . ($distribuidor->descuento ? ' - ' . $distribuidor->descuento . '%' : ''))
                    ->setCellValue($this->toAlpha($column + 2) . '2', 'marca');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells($this->toAlpha($column) . '1:' . $this->toAlpha($column + 2) . '1');
            $column = $column + 3;
        }
        $objPHPExcel->getActiveSheet()->getStyle('A1:ZZ2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:ZZ2')->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('bcedb4');
        $objPHPExcel->getActiveSheet()->freezePane('A3');
        $objPHPExcel->getActiveSheet()->getStyle('A1:ZZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A1:ZZ2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        $baseRow = 3; // line 2
        $mayor = 3; // line 2
        foreach ($dataProvider->getModels() as $producto) {
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $baseRow, $producto->codigo)
                    ->setCellValue('B' . $baseRow, $producto->referencia)
                    ->setCellValue('C' . $baseRow, $producto->nombre);
            $precio = $producto->precio1 == 0 ? number_format($producto->precio2, 0) . '*' : number_format($producto->precio1, 0);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $baseRow, $precio);
            if (in_array($producto->id, $menores['prod'])) {
                $objPHPExcel->getActiveSheet()->getStyle('D' . $baseRow)->getFont()->setBold(true);
            }
            $column = 4;
            foreach ($distribuidores as $distribuidor) {
                $row = $baseRow;
                $arrDistInfo = array();
                foreach ($producto->referencias as $referencia) {
                    foreach ($referencia->proddists as $productoDist) {
                        if ($distribuidor->distribuidor_id == $productoDist->distribuidor_id && (empty($arrDistInfo) || (array_search($producto->referencia, $arrDistInfo) === FALSE ))) {
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($this->toAlpha($column) . $row, $productoDist->referencia);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($this->toAlpha($column + 1) . $row, $productoDist->precio);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($this->toAlpha($column + 2) . $row, $productoDist->marca);
                            if (in_array($productoDist->id, $menores['dist'])) {
                                $objPHPExcel->getActiveSheet()->getStyle($this->toAlpha($column) . $row)->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getStyle($this->toAlpha($column + 1) . $row)->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getStyle($this->toAlpha($column + 2) . $row)->getFont()->setBold(true);
                            }
                            $row++;
                        }
                    }
                }
                $column = $column + 3;
                if ($row > $mayor) {
                    $mayor = $row;
                }
            }
            $baseRow = $mayor;
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="export.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('php://output');
        exit();
    }

    /**
     * Finds the Producto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Producto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Producto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid
     * for Referencia
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddReferencia() {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Referencia');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formReferencia', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Action to load a tabular form grid 
     * for ProveedorProducto 
     * @author Yohanes Candrajaya <moo.tensai@gmail.com> 
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com> 
     * 
     * @return mixed 
     */
    public function actionAddProveedorProducto() {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ProveedorProducto');
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProveedorProducto', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Imports a excel file.
     * @return mixed
     */
    public function actionImport() {
        ini_set('max_execution_time', 0);
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = 'na';
            if ($modelUpload->upload()) {
                $result = $this->loadFile($modelUpload);
                Yii::$app->getSession()->setFlash('success', $result);
                return $this->redirect('index');
            }
        }

        return $this->render('import', ['model' => $modelUpload]);
    }

    public function loadFile($modelUpload) {
        $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
        $workbook = SpreadsheetParser::open($inputFiles);
        $worsheets = $workbook->getWorksheets();
        $created_by = Yii::$app->user->id;
        $created_at = new Expression('NOW()');
        $updated_by = Yii::$app->user->id;
        $updated_at = new Expression('NOW()');
        $arrProductos = array();
        $arrReferencias = array();
        $Skip = 1;
        foreach ($worsheets as $sheet) {
            $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
            foreach ($workbook->createRowIterator($myWorksheetIndex) as $rowIndex => $row) {
                if ($Skip == 0) {
                    $codigo = trim(isset($row[$this->translate('AD')]) ? $row[$this->translate('AD')] : '');
                    $referencia = trim(isset($row[$this->translate('AE')]) ? $row[$this->translate('AE')] : '');
                    $nombre = trim(isset($row[$this->translate('AF')]) ? $row[$this->translate('AF')] : '');
                    $precio1 = trim(isset($row[$this->translate('AG')]) ? $row[$this->translate('AG')] : '');
                    $precio1 = $precio1 - ($precio1 * (15 / 100));
                    $precio2 = trim(isset($row[$this->translate('D')]) ? $row[$this->translate('D')] : '');
                    $precio2 = $precio2 - ($precio2 * (15 / 100));
                    $precio4 = trim(isset($row[$this->translate('B')]) ? $row[$this->translate('B')] : '');
                    $precio4 = $precio4 - ($precio4 * (15 / 100));
                    $proveedor = trim(isset($row[$this->translate('AA')]) ? $row[$this->translate('AA')] : '');
                    $tipo = trim(isset($row[$this->translate('AB')]) ? $row[$this->translate('AB')] : '');
                    $existencia = trim(isset($row[$this->translate('AH')]) ? $row[$this->translate('AH')] : '');
                    $ctoprm = trim(isset($row[$this->translate('T')]) ? $row[$this->translate('T')] : '');
                    $upcompra = trim(isset($row[$this->translate('V')]) ? $row[$this->translate('V')] : '');
                    $ufcompra = trim(isset($row[$this->translate('W')]) ? $row[$this->translate('W')] : '');
                    $ufvta = trim(isset($row[$this->translate('X')]) ? $row[$this->translate('X')] : '');
                    $i = floor($rowIndex / 1000);
                    $arrProductos[$i][] = [$codigo,
                        $referencia,
                        $nombre,
                        $precio1,
                        $precio2,
                        $precio4,
                        $proveedor,
                        $tipo,
                        $existencia,
                        $ctoprm,
                        $upcompra,
                        $ufcompra,
                        $ufvta,
                        1,
                        $created_at,
                        $created_by,
                        $updated_at,
                        $updated_by
                    ];
                } else {
                    $Skip = $Skip - 1;
                }
            }
        }

        $updatedRows = 0;
        if (!empty($arrProductos)) {
            // inserta productos en la tabla
            foreach ($arrProductos as $productos) {
                Producto::batchUpdate('producto', ['codigo',
                    'referencia',
                    'nombre',
                    'precio1',
                    'precio2',
                    'precio4',
                    'nombre_proveedor',
                    'tipo',
                    'existencia',
                    'ctoprm',
                    'upcompra',
                    'ufcompra',
                    'ufvta',
                    'estado',
                    'created_at',
                    'created_by',
                    'updated_at',
                    'updated_by'
                        ], $productos, [
                    'referencia',
                    'nombre',
                    'precio1',
                    'precio2',
                    'precio4',
                    'nombre_proveedor',
                    'tipo',
                    'existencia',
                    'ctoprm',
                    'upcompra',
                    'ufcompra',
                    'ufvta',
                    'updated_at',
                    'updated_by'
                ])->execute();
            }
            //obtiene las referencias que cambiaron y las referencias nuevas
            $arrReferencias = Producto::getReferenciasNuevas();
            $ids = $this->array_column($arrReferencias, 0);

            //inserta las referencias nuevas o acualizadas
            if (!empty($arrReferencias)) {
                Producto::batchUpdate('referencia', ['producto_id', 'referencia', 'tipo'], $arrReferencias, ['tipo'])->execute();

                // Actualiza el código en los registros nuevos
                $updatedRows = ProductoDistribuidor::updateProductoDistribuidor();

                //Actualiza las referencias nuevas con tipo 1. 2:pendiente por actualizar 1:Actualizado
                $updatedRows += Referencia::updateAll(['tipo' => 1], ['in', 'producto_id', $ids]);
            }
        }

        //   $deletedFile = unlink($inputFiles);
        return "Proceso terminado";
//        return  "Registros actualizados: $updatedRows";
    }

    // retorna el valor numerico del string 
    public function translate($string) {
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az',);

        return array_search(strtolower($string), $alphabet);
    }

    // retorna la letra del string 
    public function toAlpha($index) {
        $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'aa', 'ab', 'ac', 'ad', 'ae', 'af', 'ag', 'ah', 'ai', 'aj', 'ak', 'al', 'am', 'an', 'ao', 'ap', 'aq', 'ar', 'as', 'at', 'au', 'av', 'aw', 'ax', 'ay', 'az',
            'ba', 'bb', 'bc', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bk', 'bl', 'bm', 'bn', 'bo', 'bp', 'bq', 'br', 'bs', 'bt', 'bu', 'bv', 'bw', 'bx', 'by', 'bz',
            'ca', 'cb', 'cc', 'cd', 'ce', 'cf', 'cg', 'ch', 'ci', 'cj', 'ck', 'cl', 'cm', 'cn', 'co', 'cp', 'cq', 'cr', 'cs', 'ct', 'cu', 'cv', 'cw', 'cx', 'cy', 'cz',
            'da', 'db', 'dc', 'dd', 'de', 'df', 'dg', 'dh', 'di', 'dj', 'dk', 'dl', 'dm', 'dn', 'do', 'dp', 'dq', 'dr', 'ds', 'dt', 'du', 'dv', 'dw', 'dx', 'dy', 'dz',];

        return $alphabet[$index];
    }

    public function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if (!array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            } else {
                if (!array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if (!is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }

    /**
     * Imports a excel file.
     * @param integer $proveedor_id
     * @return mixed
     * @author Juan Velez <juanfv@gmail.com>
     */
    public function actionImportRelacionados() {
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = '-';
            if ($modelUpload->upload()) {
                $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
                $workbook = SpreadsheetParser::open($inputFiles);
                $worsheets = $workbook->getWorksheets();
                $fecha = new Expression('NOW()');
                $error = 0;
                $result = '';
                $skip = 1;
                foreach ($worsheets as $sheet) {
                    $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
                    foreach ($workbook->createRowIterator($myWorksheetIndex) as $row) {
                        $producto = isset($row[0]) ? $row[0] : '';
                        $relacion = isset($row[1]) ? $row[1] : '';
                        if ($producto && $relacion && !$skip) {
                            $rows[] = [$producto, $relacion];
                        } else {
                            $skip--;
                            $error++;
                        }
                    }
                }
                if ($error <= 1 && count($rows) > 0) {
                    try {
                        ProductosRelacionados::deleteAll();
                        Yii::$app->db->createCommand()->batchInsert(ProductosRelacionados::tableName(), ['producto', 'relacion'], $rows)->execute();
                    } catch (\Exception $e) {
                        $result = 'No se pudo importar el archivo.';
                    }
                } else {
                    $result = 'No se pudo importar el archivo.';
                }
                Yii::$app->getSession()->setFlash($result ? 'error' : 'success', $result ? $result : 'La información fue importada correctamente');
                return $this->redirect('index');
            }
        }
        return $this->render('importRelacionados', ['model' => $modelUpload]);
    }

    /**
     * Imports an excel file.
     * @return mixed
     */
    public function actionImportReferences() {
        ini_set('max_execution_time', 0);
        $modelUpload = new UploadForm();
        if (Yii::$app->request->isPost) {
            $modelUpload->file = UploadedFile::getInstance($modelUpload, 'file');
            $modelUpload->fecha = new Expression('NOW()');
            $modelUpload->tipo = "-";

            if ($modelUpload->upload()) {

                $result = $this->loadFileReferences($modelUpload);
//                echo $result;
//                die;
                Yii::$app->getSession()->setFlash('success', $result);
                return $this->redirect('index');
            } else {
                echo " no Cargado";
                die;
            }
        }

        return $this->render('importReferences', ['model' => $modelUpload]);
    }

    public function loadFileReferences($modelUpload) {
        $inputFiles = 'uploads/' . $modelUpload->file->baseName . '.' . $modelUpload->file->extension;
        $workbook = SpreadsheetParser::open($inputFiles);
        $worsheets = $workbook->getWorksheets();
        $arrReferencias = array();
        $Skip = 1;
        $rowIndex = 0;
        $sheet = $worsheets[0];
        $myWorksheetIndex = $workbook->getWorksheetIndex($sheet);
        $filasAc = 0;
        foreach ($workbook->createRowIterator($myWorksheetIndex) as $row) {
            if ($Skip == 0) {
                $codigo = trim(isset($row[0]) ? $row[0] : '');
                if (isset($row[0]) && $row[0] != "" && $codigo != "") {
                    $producto_id = Producto::findOne(['codigo' => $codigo]);
                    if (isset($producto_id->id)) {
                        for ($index = 1; $index <= 21; $index++) {
                            if (isset($row[$index]) && $row[$index] != '') {
                                $i = floor($rowIndex / 1000);
                                $rowIndex++;
                                $referencia = trim(isset($row[$index]) ? $row[$index] : '');
                                $arrReferencias[$i][] = [$referencia, $producto_id->id, 2]; //2:pendiente por actualizar 1:Actualizado
                            }
                        }
                    }
                }
            } else {
                $Skip = $Skip - 1;
            }
        }
        $updatedRows = 0;
//        echo "<pre>";
//        print_R($arrReferencias);
//        echo "</pre>";
//        die;
        if (!empty($arrReferencias)) {
            foreach ($arrReferencias as $cl => $referencias) {
//                // inserta referencias nuevas en la tabla y actualiza las que cambias
                foreach ($referencias as $ref) {
                    $q1 = Yii::$app->db->createCommand("SELECT * FROM referencia where producto_id = '$ref[1]' AND referencia = '$ref[0]'")->queryAll();
                    if (isset($q1[0])) {
//                        echo "encontró 1";
//                        die;
                    } else {
                        $sql = "INSERT INTO referencia (producto_id, referencia, tipo) values ($ref[1],'$ref[0]',1);";
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                    $filasAc++;
//                    print_R($sql);
//                    die;
//                    Producto::batchUpdate('referencia', ['referencia', 'producto_id', 'tipo'], $ref)->execute();
                }
            }
//            // Actualiza el informe
            $updatedRows += ProductoDistribuidor::updateProductoDistribuidor();
//
//            //Actualiza las referencias nuevas con tipo 1 (2:pendiente por actualizar 1:Actualizado)
            $updatedRows += Referencia::updateAll(['tipo' => '1'], ['tipo' => '2']);
        }
//        $deletedFile = unlink($inputFiles);
        return "Proceso terminado ($filasAc)";
//        return  "Registros actualizados: $updatedRows";
    }

}
