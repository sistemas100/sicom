<?php
namespace app\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;

/**
 * AppController extends Controller and implements the behaviors() method
 * where you can specify the access control ( AC filter + RBAC ) for your controllers and their actions.
 */
class AppController extends Controller
{
    /**
     * Returns a list of behaviors that this component should behave as.
     * Here we use RBAC in combination with AccessControl filter.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'controllers' => ['user','formato-correo','consolidado-pedidos','consolidado-ventas','distribuidor','liquidacion','producto-distribuidor','referencia'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete','import','pdf','export','report', 'add-liquidacion-item'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'controllers' => ['pedido'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf', 'add-pedido-log', 'add-pedido-producto', 'product-list', 'import', 'approve', 'import-pedido'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'controllers' => ['producto'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf', 'import', 'export', 'import-references', 'import-relacionados', 'report', 'add-proveedor-producto', 'add-referencia'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'controllers' => ['proveedor'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'purchases', 'add-pedido', 'add-proveedor-producto','import-productos-proveedor','reporte-productos-desactivados'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'controllers' => ['producto','producto-distribuidor'],
                        'actions' => ['index', 'view', 'pdf', 'report', 'export'],
                        'allow' => true,
                        'roles' => ['mercadeo'],
                    ],

                ], // rules

            ], // access

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ], // verbs

        ]; // return

    } // behaviors

} // AppController
